---
title: 'Just plugged this mystery mahogany Explorer in'
date: Fri, 10 Jan 2020 18:24:11 +0000
author: rj
draft: false
tags: ['Explorer', 'guitar', 'mahogany', 'mystery', 'natural', 'vintage', 'vintage guitar']
---

omigosh omigosh omigosh! I just plugged this mystery mahogany Explorer in.

I was afraid to - didn’t know if it was going to be a huge letdown. The sound: it’s gorgeous. I think on this home stereo no less and not even a guitar amp it may have catapulted to my favorite of all time. The sound is articulate (not muddy) and has super beef to it if you chug on the low E string.... oh my gosh tone heaven. The tone control has huge adjustment. The damn thing sounds like my spanking tele. It’s got the tele spank sparkle to it - on a frickin’ Gibson scale 24.75”! What??!

The neck is thick. Real thick. I just measured the 12th fret as 1". The 1st fret / nut area is about 7/8" thick. I pulled the cover from the controls on the back. There isn't any real big identifiers. I'd say they are CTS pots. 500K. The two volume controls say "86 500K 0416" on them. No other info stamped. The tone control simply says "EP0" the rest is covered with solder.