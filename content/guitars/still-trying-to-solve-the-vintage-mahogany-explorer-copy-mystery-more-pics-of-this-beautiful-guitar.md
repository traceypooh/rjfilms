---
title: 'Still trying to solve the vintage Mahogany Explorer copy mystery!! More pics of this beautiful guitar.'
date: Fri, 31 Jan 2020 15:48:52 +0000
author: rj
feature_image: https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0177-Explorer-mahogany-Vermont-mystery-day2-BEST-overall-1024x475.jpg
tags: ['70s', 'Burny', 'Explorer', 'Fernandes', 'guitar', 'mahogany', 'MIJ', 'vintage', 'vintage guitar']
---

I still have a cold trail as to what this beauty is. I've been playing the heck out of it and I really love it.

![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0177-Explorer-mahogany-Vermont-mystery-day2-BEST-overall-1024x475.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0183-Explorer-mahogany-Vermont-mystery-day2-BEST-longview-1024x646.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0181-Explorer-mahogany-Vermont-mystery-day2-BEST-body-1024x658.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0157-Explorer-mahogany-Vermont-mystery-day-BEST-back-1024x685.jpg) ![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0132-Explorer-mahogany-Vermont-mystery-day-curved-End-Headstock-1024x695.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0141-Explorer-mahogany-Vermont-mystery-day-BEST-knobs-grain-1024x685.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0158-Explorer-mahogany-Vermont-mystery-day-BEST-headstock-back-1024x710.jpg)![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/DSC_0163-Explorer-mahogany-Vermont-mystery-day-BEST-side-grain-bck-1024x685.jpg)

Through tons of internet searching I found this guitar looks nearly identical to it. Unfortunately it's listed as 'unknown' maker and country of origin.

The headstock looks exactly the same build. With three pieces of wood seamed the same. It had the same tuner mount holes above each tuner (and has replacement tuners like mine does).

The truss cover is a 3 hole cover, with the truss rod cutout all the way down flush to the nut (like mine).

The Finish looks similar, back cover the same.

Seller lists it was 'refinished in natural' before by someone, but I think it's possible that's not accurate. It could be the headstock was sanded down to wood. And maybe the neck.  It's also a two piece body and the body extends to the 4th fret like mine does (most go to the 3rd fret and not as high).

 [https://reverb.com/item/4330809-unknown-explorer-style-guitar-right-handed-awesome-jackson-bridge-pickup?show\_sold=true](https://reverb.com/item/4330809-unknown-explorer-style-guitar-right-handed-awesome-jackson-bridge-pickup?show_sold=true)

Here's a pic of that ad above, the headstock front and rear shots. The construction look the same.

\[caption id="attachment\_195" align="alignnone" width="640"\]![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/unknown-jackson-pickup-identical-Explorer-3-way-headstock-front-1024x768.jpg) SAMSUNG CAMERA PICTURES\[/caption\] \[caption id="attachment\_196" align="alignnone" width="640"\]![](https://rjfilms.com/guitars/wp-content/uploads/2020/01/unknown-jackson-pickup-identical-Explorer-back-headstock-3-wood-1024x768.jpg) SAMSUNG CAMERA PICTURES\[/caption\]