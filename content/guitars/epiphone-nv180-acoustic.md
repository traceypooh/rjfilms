---
title: 'Epiphone NV180 acoustic'
date: Sun, 16 Dec 2018 19:36:40 +0000
author: rj
feature_image: https://rjfilms.com/guitars/wp-content/uploads/2020/04/epiphone-NV180-I-bought-CL-ad-300x172.png
tags: ['bolt on', 'Epiphone', 'Japan', 'Made in Japan', 'MIJ', 'NGD', 'Nova series', 'noveau', 'NV', 'rare', 'vintage', 'vintage guitar']
---

Bought this one for $60! Local CL philadelphia ad... I was working a week in DC that was mostly sitting around so pounding cragislists ads all up the atlantic seaboard for scoring deals on the drive back north... And I spots this one with my eye. ![](https://rjfilms.com/guitars/wp-content/uploads/2020/04/epiphone-NV180-I-bought-CL-ad-300x172.png) ![](https://rjfilms.com/guitars/wp-content/uploads/2020/04/IMG_0132-Epiphone-NV180-225x300.jpg) Unfortunately, I only have this in my hands for a few moments... and as a new guitar player / buyer I don't realize the action is sky high... and in fact, the neck is pulling away from the body. This will get un tensioned by me, and set aside for a semi serious repair. It's a bolt on neck (unusual) and what seems to happen with many Epiphone acoustic bolt ons like this (the FT140, FT130 etc...) is that the glue in the neck block gives out and then it's a large job to fix it and make it better than factory solid with new wood braces installed under the sound board. Read more about that repair - and one I have on to do list for future here: [Gibson Forum: Repairing Norlin era neck](https://forum.gibson.com/topic/37812-repairing-a-norlin-era-ft-guitar-with-broken-neck/)