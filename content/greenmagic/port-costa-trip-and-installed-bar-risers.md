---
title: 'Port Costa trip and installed bar risers!'
date: Sun, 01 Jun 2014 23:08:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day1.jpg
---

Installed Mileage 47,662. Shorter trip with the cafe 78 CB400 and Hayabusa up to Port Costa and warehouse Cafe for second visit. My arms in a lot of pain still but that’s a short fun destination.  
I was wanting badly to install new 1″ bar risers but wanted to do it in the middle of a trip for most direct comparison so now was the chance.  
[![zx6e bar risers instlall day1](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day1.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day1.jpg)  
I wanted to see if these would fit with stock cables – especially the brake line – would clear windscreen and maybe avoid tank ding finally, and be all I was hoping for better riding position as some forum peeps say they’re great.

We got to the biker bar, and two of us split a beer. I like that place. I brought the right sized Allen key and using my bike tool kit cheater extension was able to swap out the stock bolts, raise the bars and bolt in the new risers in just minutes![![zx6e bar risers instlall day2](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day2.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day2.jpg)  
[![zx6e bar risers instlall day3](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day3.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-instlall-day3.jpg)[![zx6e bar risers install day4](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-install-day4.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/06/zx6e-bar-risers-install-day4.jpg)  
Well… they are fantastic!! It’s almost like transforming it into a new bike! I can’t really over state how amazing the change is!! It’s taking a ton of pressure of my wrists. And, as a totally unexpected bonus and one that seems almost counter intuitive, it also changes my seat to knee to footpeg geometry. It makes my legs more spaced out as I’m more upright. It doesn’t seem possible but it really is. I guess it is the fact I’m more upright which puts my hip in a different position which crunches my legs up less.  
All I can say is wow!!  
It’s putting this seating a lot closer to the feel of a new Ducati Multistrada. And that’s great.

My body was pained from yesterday so it was hard to fully appreciate them, but it’s a whole lot better and I hope transformed my bike from a 100 miler to a 200-250 miler machine for me.