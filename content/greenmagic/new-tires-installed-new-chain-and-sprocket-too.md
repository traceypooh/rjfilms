---
title: 'New tires installed! New chain and sprocket too!'
date: Fri, 12 Feb 2016 19:13:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_4197-427x320.jpg
tags: ['repair']
---

Mileage: 62470  
Just finished installing my 3rd set of tires. This time I got to use a friend’s pro tire machine. It was awesome! Both done in under 30 minutes. I never used a machine before it was pretty easy.  
Airing up to set the bead was pretty simple too – a first for GM wheels which have been super hard to set in the past.

Next day I struggled trying to get the chain master link rivet style to install without the proper tools. Lots of swearing and help from friends… finally used a small socket and a C clamp that had to be borrowed as I just broke my own one trying to do the same thing.  
Pressed in the pins through the plate, and then used a very tapered punch with a small anvil behind the chain and punched in / deformed the master link pins.  
Was a huge relief to have that done!

Now I can go ride GM without being terrified the chain would snap and take me out or go through the cases and blow my engine. Been a nerve wracking few months here.

The SunStar aluminum rear 46 tooth sprocket was so much lighter than the all steel JT 48 tooth sprocket I put in. Incidentally, this is the first time I’ve ever ridden a ZX6E with the stock sprocket setup 16/48 teeth. GM came with a 46T on the rear.

Beautiful new JT gold X Ring chain. Hoping this setup lasts a long time!

Rode out today and my steering was completely messed up. Something was definitely wrong. Well, after hwy at 70 mph and 102 turns in the mountains I got to the destination and by this point knew my front tire was real low.  
How low can you go? 6 psi front, 20 psi rear. Not good. I had no way to fill the air up last night – racing to get it done with borrowed tools.

“how fast can you change your tires with the machine?” (Me): “Uh… like, maybe 20 minutes (I had no idea – never used a machine!).

It’s all good today though. Looking forward to return trip with proper inflated tires!

![IMG_4197](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_4197-427x320.jpg)

![IMG_4196](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_4196-1000x750.jpg)

Here’s the chain a few months on – and the hand bashed in master link:  
[![IMG_5671](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/IMG_5671-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/IMG_5671.jpg)