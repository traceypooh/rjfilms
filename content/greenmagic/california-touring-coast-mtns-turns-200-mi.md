---
title: 'California touring. Coast mtns turns 200 mi'
date: Fri, 28 Mar 2014 19:21:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image5-1024x764.jpg
tags: ['Carmel', 'Gold Coast', 'Highway 1', 'Monterrey', 'Touring']
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image5-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image5.jpg)  
Drove 202 miles and hit 101 on the 1 for those who like numbers.  
That’s the longest I’ve driven my E in a day, and the fastest I’ve gone. Weather about 60. Mostly sunny. Great coastal weather with the frequent fog and gloom out there by the sea otherwise.

Biked to work 3x this week and Friday we got done at noon! So I headed for the coast since I was already 35 miles away in the peninsula.  
Took 92 all the way to Half Moon Bay and went slightly up the 1 to a little surf spot by Mavericks and ate whatever food I brought in my tank bag.  
Back on the 1 south – I passed a full police escort making traffic a mess escorting a bicyclist riding Amaerica for a charity. But that’s cool, good cause.  
Took a left off 1 to Creek cutoff road and followed to Tunitas Creek road. Very cute single lane road winding over small pass and farmland. Will a bunch of recent rain CA up here is very green and pretty now.  
Took Tunitas back to the 1 just cause, and looped back on Tunitas the whole way to Skyline. Tunitas Creek road is a tiny windy redwood grooved mysterious road. Empty of cars. Met a nice Canadian biker girl (bicyclist) riding for 2 weeks in America and camping at night.

Skyline south to Alice’s / 4 corners / Woodside and then at least the locally famous and acclaimed superstar road 84 east to the sea. What a road! Haven’t driven it n years and first time in my life on a bike. Wow! Turns after turns and more turns. They never seem to end. Perfect pavement great turns makes you feel heroic out there! Rt 1 to Stinson beach or 84 as favorite? Hmmmm… Lets pass that question for now, they are both my favorites!

I then turned again onto Rt 1 south with Pigeon Pt lighthouse as my destination goal. Made quite a few beach and photo stops along the way.  
Really pretty and rugged coastline. Impressive empty surf. At Pigeon Pt I walked to end and saw grey whales spouting and their backs coming out of the water while stopped there which was cool.  
I guess they are migrating back north after escaping the winter chilly north waters for southern birthing.  
Back on 1 north towards 84 I wound out bike to 11K, man that motor pulls over 7k, it just screams at 9k +  
Hit indicated 101 mph but the front was a little unstable. Perhaps I also have a little head shake after 95 mph but I never notice it otherwise. At about 95 mph the front end becomes somewhat unstable. Literally the front end, as in the main fairings start vibrating and flapping about. I’m missing several mount tabs (broken off) from the main nose fairings where it bolts in below, and one small fairing piece apparently I’m missing under the headlight. So my horn flaps about like crazy after 50-60 mph too.

Zipped up and down 84 to 4 corners (84 & Skyline) again, the roads are just too good to do once or twice. And back to the O town.  
Really looking forward to putting 1″ bar risers on the bike I think it would be a huge improvement over stock for me, but this bike is sweet!  
First tank of gas: 47.5 mpg. 2nd tank 42 mpg.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image6-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image6.jpg)  
This is Skyline.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image8-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image8.jpg)  
Skyline north of Woodside. Redwoods.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image7-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image7.jpg)  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image17.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image17.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image21.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image21.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image19-e1396214458463.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image19.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image18-e1396214484424.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image18.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image16.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image16.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image25.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image25.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image23.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image23.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image5-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image5.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image13.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image13.jpg)[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image11-e1396214439744.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image11-e1396214439744.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image24-e1446229973146.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image24-e1446229973146.jpg)

[![Green Magic kawasaki zx6e ](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image20-e1446230118714.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image20-e1446230118714.jpg)