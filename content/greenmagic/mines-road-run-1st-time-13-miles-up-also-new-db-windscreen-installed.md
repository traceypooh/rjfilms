---
title: 'Mines Road run, 1st time. 13 miles up. Also new DB windscreen installed!'
date: Sun, 29 Sep 2013 19:31:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Mines-road-wrong-turn-view-photo-150x150.jpg
---

Also a spectacular wrong turn, maybe best one I’ve done past Mines Road to a park that dead ends after ascent and descent over a peak ridgeline run with gorgeous views!

[![Mines road wrong turn view photo](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Mines-road-wrong-turn-view-photo-150x150.jpg)](https://rjfilms.com//blog/greenmagic/mines-road-wrong-turn-view-photo/)

[![Ford photo](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Ford-photo-150x150.jpg)](https://rjfilms.com//blog/greenmagic/ford-photo/)

[![lead sledphoto](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/lead-sledphoto-150x150.jpg)](https://rjfilms.com//blog/greenmagic/lead-sledphoto/)

  

[![Noble race carphoto](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Noble-race-carphoto-150x150.jpg)](https://rjfilms.com//blog/greenmagic/noble-race-carphoto/)

[![lunch livermorephoto](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/lunch-livermorephoto-150x150.jpg)](https://rjfilms.com//blog/greenmagic/lunch-livermorephoto/)

[![GMM-mines-overlook](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/GMM-mines-overlook-150x150.jpg)](https://rjfilms.com//blog/greenmagic/gmm-mines-overlook/)

  

[![GM mines road overlook spine](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/GM-mines-road-overlook-spine-150x150.jpg)](https://rjfilms.com//blog/greenmagic/gm-mines-road-overlook-spine/)

[![MinesRd1trip-photo 1](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-1-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-1/)

[![Mines Rd bikers photo](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Mines-Rd-bikers-photo-150x150.jpg)](https://rjfilms.com//blog/greenmagic/mines-rd-bikers-photo/)

  

[![Mines rd bikers 2](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/Mines-rd-bikers-2-150x150.jpg)](https://rjfilms.com//blog/greenmagic/mines-rd-bikers-2/)

[![MinesRd1trip-photo 3-1](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-3-1-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-3-1/)

[![MinesRd1trip-photo 1-1](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-1-1-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-1-1/)

  

[![MinesRd-Single-lane-rd](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd-Single-lane-rd-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd-single-lane-rd/)

[![MinesRd1trip-photo 4-1](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-4-1-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-4-1/)

[![MinesRd1trip-photo 3](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-3-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-3/)

  

[![MinesRd1trip-photo 2](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-2-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-2/)

[![MinesRd1trip-photo 4](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-4-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-4/)

[![MinesRd1trip-photo 5](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-5-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-5/)

  

[![MinesRd1trip-photo 5-1](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/MinesRd1trip-photo-5-1-150x150.jpg)](https://rjfilms.com//blog/greenmagic/minesrd1trip-photo-5-1/)

[![GM pretty view mines road turn off](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/GM-pretty-view-mines-road-turn-off-150x150.jpg)](https://rjfilms.com//blog/greenmagic/mines-road-run-13-miles-up-first-time-also-new-db-windscreen-installed/gm-pretty-view-mines-road-turn-off/)

  

We also drove into downtown Livermore to get a lunch, and headed to a rumored ‘cute old town’ downtown and stumbled onto a Nostalgic Car show that was packed. Pretty cool, and great all you can eat Indian buffet we ate out on patio where the show car winners happened to strut by

In morning I was also alarmed to have gas pouring out leaks onto the ground on start up first and second try. This happened once before I let the bike sit for 4 weeks. This time about 2 weeks or so. Leaking out of cylinder 1 & 2 with gas dripping off intake on #4 too at least if not them all.

I theorized that maybe my float needles are sticking in the seats, and the carbs overflow and leak. So I let it sit for 20 min, stewing and on my forum researching http:///www.zx6e.net which is an excellent forum.  
I went back to bike and with it still off rocked the throttle back and forth 6-7 times +, hoping it could possibly unstick the float needles. And whaddya know? Started right back up and not a drop leaking. Hmmmm.  Mildly worried first 1/4 of trip and then just solid fun!

Prior to starting bike, I installed a new Double Bubble windscreen to try it out. WOW, what an amazing difference. Gone is the highway (50 mph+) buffeting and roar. There is no cockpit wind blast until slight start about 50 mph is first you notice a little wind now. Gonna make this bike such a better tourer!!