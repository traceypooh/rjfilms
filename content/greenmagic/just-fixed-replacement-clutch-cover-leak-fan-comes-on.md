---
title: 'Just fixed replacement clutch cover leak, fan comes on!'
date: Thu, 03 Sep 2015 17:21:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/09/replaced-clutch-cover-IMG_3047-427x320.jpg
tags: ['repair']
---

Mileage: 58,250  
So I figured out what went ‘sideways’ on the replacement clutch cover install – the damn gasket. I reused the one I had made before for it. And it slid up into the engine area a bit and right at the most sensitive spot there was no gasket at all!  
Hence the instant leak the bike goes upright (let alone runs) and oil is in that area now.

I also topped off the coolant (after the radiator thermostatic temp switch swap), and then started the bike on the center stand. No leaks. I let it warm all the way up to the middle of the temp gauge and the radiator fans came on!!  
That’s a first in a month, so a good end to the day after and idiotic hole punch destroyed my last remnant of gasket paper this morning. I was cutting a new one, and then it was ruined. I was irate. But now I am happy!

[![replaced clutch cover IMG_3047](https://rjfilms.com/greenmagic/wp-content/uploads/2015/09/replaced-clutch-cover-IMG_3047-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/09/replaced-clutch-cover-IMG_3047.jpg)