---
title: 'Just got Paid to make Palomares, Pinehurst, Niles Canyon, 84E to Mines Rd. Run! 100 miles 5 hours.'
date: Wed, 19 Jun 2013 21:51:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/Picture-61-150x150.png
---

Got last minute call to shoot an MGA car purchase for “Dusty Cars” during rush hour, so I plotted beautiful canyon back road burner trip! Left at 4:40 pm back to Oakland 9:50pm. 100 Miles, and two hour shoot on site. Niles Canyon is just gorgeous! Also 84E out of Sunol into S. Livermore was pretty epic up the mountain vista there.

[![Picture 6](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/Picture-61-150x150.png)](https://rjfilms.com//blog/greenmagic/just-got-paid-to-make-the-palomares-pinehurst-niles-canyon-84e-to-mines-rd-run-100-miles-5-hours/picture-6-2/)

[![IMG_1823](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1823-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1823/)

[![IMG_1826](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1826-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1826/)

  

[![IMG_1829](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1829-e1371754579221-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1829/)

[![IMG_1830](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1830-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1830/)

[![IMG_1835](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1835-e1371754532367-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1835/)

  

[![IMG_1837](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1837-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1837/)

[![IMG_1840ps](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1840ps-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1840ps/)

[![IMG_1841](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1841-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1841/)

  

[![IMG_1844](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1844-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1844/)

[![IMG_1843](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1843-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_1843/)

  

MGA car is in upper middle left corner of farm shot. Farmer, George there, was the seller. Motor and trans removed an in creepy cool shed, like “Evil Dead 2” stuff. Cool.

So I did have to strap about 6 lbs of the camera and wireless microphone to get it to the shoot. Shot whole thing hand held. Fun run, but late back and late dinner. Pretty tiring run after editing most of day. But very pretty ride!