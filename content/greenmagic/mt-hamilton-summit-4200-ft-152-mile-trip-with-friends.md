---
title: 'Mt. Hamilton summit. 4200 ft. 152 mile trip with friends'
date: Mon, 26 Jan 2015 00:09:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-Niles-Canyon-zx6e-500x273.png
tags: ['California', 'Mt. Hamilton', 'south bay', 'Touring', 'Touring', 'Zx6e', 'zzr600']
---

Beautiful January day! late start, actually a bit my fault as I had many issues getting my GoPro mounted.  
The ride up there was great. Redwood to Palomares Canyon to Niles Canyon to Calaveras Rd all the way to 680 S near Milpitas. Then short 680 trip and Capitol Xprwy to Quimby and Mt Hamilton and the wild looking observatory. Heard snippets of tour and the whole interior and scope dates to 1887 or so!  
Slipped once on Palomares which was on GoPro, but it doesn’t look too dramatic. Patchy dappled sun / shade and didn’t see the sand in the road and slid the front and rear a little. Was a little spooky.

The ride up the mountain was great, the pavement great, fantastic weather, just a lot of fun out on the road.

Stops: Railroad Cafe in Sunol for lunch, Fuel stop shortly after, and just a few very short stops besides the actual summit.

[![gopro grab zx6e - Niles Canyon zx6e](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-Niles-Canyon-zx6e-500x273.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-Niles-Canyon-zx6e.png)  
Niles Canyon – one of my favorite cruising spots. A beautiful canyon drive.

[![gopro grab zx6e - lunch RR cafe sunol](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-lunch-RR-cafe-sunol-500x282.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-lunch-RR-cafe-sunol.png)  
Lunch stop RR Cafe, Sunol.

[![gopro grab zx6e - cornering Calavares](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-cornering-Calavares-500x280.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-cornering-Calavares.png)  
Calavares Road. Pretty epic turns, farmland, canyon carving. Must do this one again!

[![gopro grab zx6e - 2 riders Calavares](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-2-riders-Calavares-500x273.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-2-riders-Calavares.png)  
Calavares.

[![IMG_1536](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1536-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1536.jpg)Calavares quick stop.

[![IMG_1546](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1546-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1546.jpg)Leaving San Jose up Quimby. Very pretty views backwards.

[![IMG_1555](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1555-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1555.jpg)The T at Quimby at rt 130.

[![IMG_1553](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1553-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1553.jpg) Mt Hamilton in the background.

[![gopro grab zx6e - creepy trees](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-creepy-trees-500x279.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/gopro-grab-zx6e-creepy-trees.png)  
creepy trees encroach me on the right. GoPro grab.

[![IMG_1580](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1580-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1580.jpg)

[![IMG_1579](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1579-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1579.jpg)

[![IMG_1572](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1572-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1572.jpg)

[![IMG_1571](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1571-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1571.jpg)  
[![IMG_1570](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1570-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1570.jpg)

[![IMG_1588](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1588-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1588.jpg)

[![IMG_1567](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1567-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1567.jpg)

[![IMG_1565](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1565-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1565.jpg)

[![IMG_1562](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1562-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1562.jpg)  
Outer Spacey on the gas.  
[![IMG_1559](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1559-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1559.jpg)

[![IMG_1558](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1558-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1558.jpg)

Here’s a few pics at the summit of Mt Hamilton with the fantastic observatory. Beautiful views. Stunning roads. Did you check out those turns?  
[![IMG_1601](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1601-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1601.jpg)

[![IMG_1595](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1595-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1595.jpg)

[![IMG_1613](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1613-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1613.jpg)

[![IMG_1617](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1617-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1617.jpg)

[![IMG_1628](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1628-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1628.jpg)

[![IMG_1644](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1644-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/01/IMG_1644.jpg)  
Loosing light fast. Damn were these roads a blast! I can’t wait to go again with more time for some extra laps. I’m surprised we pushed to the summit. The CB400A rider doesn’t like highways or the dark, both of which we resolved we would be riding in if we went for the summit. We left Oakland close to 11:30am I think (late) and didn’t leave lunch until about 1:30p so we were pushing a 86 mile one way to the summit at that point. But it really wasn’t that cold at all, in fact I was hot from Milpitas until the sun dropped. We rode out the way we came in (Quimby) until that T at rt 130. We passed some nice farms taking 130 to Alum Rock Road, 680 North to surface street Mission Blvd the whole way to 580. 580 pounded my bike and back as always – I Hate that damn road (Hayward to Oakland 580 sucks) – to 13 north. I think we got back about 7pm. A fantastic journey and brand new roads for me starting at Calavares!

(Note: I forgot to shut off my fuel petcock the day before – something I never forget now b/c of what happens next… – get up in the a.m. to fuel puddle next to bike, and stink of fuel. Gas soaking carb #2 intake boot from carbs to the motor. It’s either the float gasket that’s flat there leaking, OR my float needles sticking in the seat, and I have fuel leaking through my carbs – and probably into the motor dammit. Oil did not smell of fuel thankfully. I shut off gas for an hour, then went WOT for 3 minutes to try to clear out fuel \[motor off\]. The bike jammed on the starter – hydro locked up no doubt. I only got it to start by rocking it back and forth in gear many times, and finally got it running, almost bailed on the trip altogether. was concerned I have fuel in oil, but I guess I really wanted to go on this group ride! No leaks all day, and bike ran strong.)