---
title: 'Trip north from Orange County 518 mile day'
date: Sun, 15 Mar 2015 22:45:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8477-400x300.jpg
tags: ['Touring']
---

[![IMG_8477](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8477-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8477.jpg)  
All Packed up and ready to go. 10:38 a.m. Start mileage: 53,290

A morning stop in L.A. on the way up. 11:48am. Just had to do it with Green Magic:  
[![IMG_8484](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8484-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8484.jpg)  
[![IMG_8487](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8487-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8487.jpg)

Went back to downtown, Skid Row to be precise as I cannot find where the f\*ck the stupid 10 West to Santa Monica is. Must be the rarest sign in LA. No access, I drive around in circles like 6 laps and 30 minutes swearing furiously into my helmet. Anyway, after going backwards and then backwards again on the 10E – twice no less to take the 2nd damn exit so I can loop back and make it onto 10 W – geez give me a break – I finally lane split the traffic choked highway 10 to Santa Monica. Then after zigging and zagging get ahead of traffic on the 1 northbound into Malibu. Text and pee break across from Pepperdine – nice location, school. 1:30pm.  
Malibu:  
[![IMG_8495](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8495-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8495.jpg)

[![IMG_8497](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8497-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8497.jpg)  
[![IMG_8500](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8500-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8500.jpg)

[![IMG_8498](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8498-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8498.jpg)

A lot of bikers just congregated at this restaurant. Was jammed. Some tuner kids too. And I guy wheelie-ing down the road one knee on the seat…

[![IMG_8501](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8501-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8501.jpg)Stopped in Santa Barbara – Montecito precisely – and got a take out burrito from Los Arroyos (done that before there). 4:40pm. Was ok, and ate it next to my bike and back onto the 101 very briefly and the lovely 156 pass!

Stagecoach Lane turnoff at the peak of the pass. I’ve always loved this view:  
[![IMG_8506 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8506-copy-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8506-copy.jpg)

[![IMG_8504](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8504-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8504.jpg)

Getting near desperate on fuel. Was very relieved to see Los Alamos coming up, a town I detoured in coming south.  
Notice the Ferrari Dino GTS in the background. I sure did. I was so excited to check it out, as it’s one of my all time favorite ‘cages’, and combined with being so tired, I let the side stand pull it’s usual ‘trick’. It’s happened to us all. I drop the bike at the fuel pump right here. And yeah, GoPro was running so I guess I will have to post that in shame.  
[![IMG_8511](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8511-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8511.jpg)

[![IMG_8513](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8513-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8513.jpg)

[![IMG_8515](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8515-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8515.jpg)  
A quick stop not far out of Los Alamos. Very pretty wine country, and yeah, I peed right there too.

[![IMG_8517](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8517-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8517.jpg)

[![IMG_8516](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8516-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8516.jpg) My favorite rt 101 stop: Pismo Beach again. Wasn’t as dark as the camera suggests it was. I wanted to stay longer and have a proper break, but I’ve made so little mileage today and I am pissed about it all, and the sun is getting ready to set. So rig up the GoPro for the nice section on Rt 101 leaving Pismo and get on my way.

It’s from here on I drop the hammer and don’t let up.  
[![IMG_8521](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8521-e1426636450374-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8521-e1426636450374.jpg) I ride 100 miles straight until my next short break at Wild Horse Road, King City, for my final fuel up. Not off the bike long, but I do clean the damn bug corpses off my helmet visor again. I take time to eat an apple right by the fuel tanks, there's a nice fuel aroma to enjoy with the fruit taste. 150 miles to go still. So i ride another 100 miles straight. Had planned to split it 75 / 75 but I get a little second wind and ride the wave. I also don't want a long final leg either. Mostly cranking on the hwy 80 mph and sometimes 85 keeping up with fastest cars out there. On this trip I start gripping narrow the handlebars and move a thumb over the grip like the fingers to change it up for the body. It helps. I also stretch my leg fully out in front of the fairings a few times which actually helps a lot - the E has a cramped knee position. Final stop just south of San Jose on Silicon Valley lane or something. Walk in paces up and down the end of the street. Shoot out a text and back into the night. I arrive into Oakland just before 11pm. There's some Indian take out food awaiting me! End mileage: 53,808 518 miles about 12 hours total.