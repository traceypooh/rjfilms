---
title: 'Marin. Hwy 1 Stinson. Bolenas. Pt Reyes. Lucas Valley. Nicasio. 3 bridge loop!'
date: Sun, 08 Sep 2013 20:30:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image-300x224.jpg
tags: ['Touring']
---

Fantastic trip. Probably about 150 miles total. Left late at 11am. But sweet highway 1 made up for it off the 101 in Mill Valley. Attacked corners with fresh pavement. Best quality turns I’ve ever done on a bike. Very thrilling. Beautiful drive out of Stinson and past Bolenas. Fresh half dozen oysters for me in Bolenas. Wet fog “rained” on us at Pt Reyes tip. Brrr…

[![image](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image.jpg) [![image](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image2-300x225.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image2.jpg)

[![Magic Man](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image1-e1378765094724-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image1-e1378765094724.jpg)

Magic Man

[![image](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image3-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image3.jpg) [![image](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image4-300x225.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/image4.jpg)