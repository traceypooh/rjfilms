---
title: 'Had to rig up 12V hot to power headlight out. Ride home at dark.'
date: Fri, 13 May 2016 21:15:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5138-427x320.jpg
tags: ['repair']
---

[![IMG_5138](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5138-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5138.jpg)

Headlight still kerfluey. I lost the high beam about 7-8 weeks ago, but the low beam had been working. Now back to the bike, both my headlights are out and rode to work as I wasn’t expecting to work after dark, but I did.  
Put in replacement headlight early in day at work and the new known working bulb did not work on high or low beam. Shit. Not good.  
[![IMG_5108](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5108-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5108.jpg)  
Disassembled the left handgrip. All contacts look very good. No trouble spotted there at all.  
Then I lost the small ball bearing into the bike somewhere. Grrr. Whatever. No biggie.  
Here’s where it gets interesting:  
Mechanic I worked w yesterday (I was living the dream – rocketing around a 1964 jaguar E Type!!!)  
Had some crazy tool. Electrical type. Never seen one before. Not a multimeter. (It was a power probe).  
Anyway he attached one lead directly to my battery post and then could instantly test all the circuits by touching each the top of each fuse WITH ignition off and activate each circuit!  
So he told me my replacement headlight worked fine. We saw it light up.  
Then he probed the 3 way switch handle grip headlight at the solder points individually and could then tell me the white/red lead was low beam one of other was high beam as they each activated the light.  
He said I should just wire a 12V hot wire to either solder point. Actually he wanted me to cut the wire and twist a new lead together. He had done a 13hr day w me and had to go then.  
So I decided to dumpster dive their dumpster and easily found a bunch of old scrap wiring. I cut up a lead and wrapped it around the solder post of the handgrip switch lo beam and reused some electrical tape to try to insulate it.

Used the 12V ACC connection that comes from under the seat that I had rigged up a connection to by the front gauges to a cigarette lighter adaptor that I used for moto camping to power GPS, my iPhone, and Nikon batteries if needed.  
So I unplugged that connector and jammed the lead into the long vertical lead and voila! Low headlight!

Mechanic said for some reason the handle grip switch is NOT getting 12VDC power and that’s my headlight issue.  
So would this be an ignition key / area contact / wire that is suspect?

Biked after dark after driving E Type w the crazy wiring at 80-85mph on highway and she worked great for 40 miles! But only temporary solution and I have to manually plug / unplug wire for headlight as the ACC circuit is always hot even without the key in and bike off.  
[![IMG_5139](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5139-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5139.jpg)