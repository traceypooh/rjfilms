---
title: 'Nissin CBR Honda brake upgrade fitted! DIY How to'
date: Sun, 26 Apr 2015 18:20:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/03/IMG_2754-copy-763x750.jpg
tags: ['brakes', 'DIY', 'nissin', 'repair', 'upgrade. braking', 'Zx6e', 'zzr600']
---

Nissin CBR Honda brake caliper upgrade How to DIY  
![IMG_2754 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2016/03/IMG_2754-copy-763x750.jpg)

![IMG_2228](https://rjfilms.com/greenmagic/wp-content/uploads/2016/03/IMG_2228-500x312.jpg)

![IMG_2391](https://rjfilms.com/greenmagic/wp-content/uploads/2016/03/IMG_2391-332x320.jpg)

I bought a complete set of Honda CBR F4i front brakes for just $20!  
That included everything, the calipers, Master cylinder, all brake lines, pads, and even the lever at the m/c.

I wound up combining them with the stock ZX6E brake lines, master cylinder and brake lever. Was just a little easier that way to install.  
Took some grinding on the fork tubes with a file in two places on each one, plus filing down the Nissin calipers themselves in one spot and removing several nickel thicknesses there, and I also had to deal with the different bolt in sizes.  
I opted to tap out the Nissin caliper holes and use the stock ZX6E brake bolts to their tubes. This way is safer I believe and also I can swap back to a stock setup quickly in a pinch. No shouldered bolts for me.  
I borrowed a friend’s M10 1.25 pitch tap and that did the trick.