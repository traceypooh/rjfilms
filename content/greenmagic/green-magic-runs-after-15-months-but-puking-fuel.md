---
title: 'Green Magic runs... after 15 months... but puking fuel.'
date: Tue, 12 Nov 2019 21:10:00 +0000
author: rj
draft: false
tags: ['repair']
---

I charged the battery for 4 hours. I was then just able to start Green Magic and get it running! Unfortunately the carbs kept running and running an puking pouring fuel everywhere. I kept shutting off the fuel petcock to stop the pouring fuel and kept the motor running, when fuel was getting very lean in the clear gas tube I installed I would open the petcock again to keep motor running, and fuel would pour out of carbs in a second. Very annoying and troubling. Fuel was everywhere. I finally was able to narrow it down to the third carb from the petcock side of bike (right as sitting #3) that was pouring fuel in a huge stream. I guess the valve seat needle must be sticking there and won't unstick with heat or knocking the side of the bowl doesn't work either :(