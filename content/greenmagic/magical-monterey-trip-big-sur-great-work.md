---
title: 'Magical Monterey trip. Big Sur. Great work!'
date: Mon, 17 Aug 2015 04:20:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0102.jpg
tags: ['Concours', "Concourse d'elegance", 'Ford GT40', 'Highway 1', 'Kawasaki', 'Monterrey', 'Ninja', 'Pebble Beach', 'sexiest car of all time', 'Touring', 'Zx6e', 'zzr600']
---

Had an amazing work trip down to Monterey. Working at the Concours d’Elegance, something I have always wanted to see.
Now there were far more Lambos down there than Prius cars!

Client put me in a magical hotel, the best hotel experience I’ve ever had. Monterey Bay Inn. The staff was terrific, and it was such a small boutique hotel on the water there were no common space rooms, so that meant breakfast was brought to your door every morning!

I was surrounded on two sides by beautiful Monterey Bay on the top floor of the Inn. I found out later I got The Honeymoon Suite – literally – my work buddy had been there last year for a wedding. I had a marble bathroom and open tub that looked right out to the ocean and a balcony over the water. It was fantastic every morning and often I’d get back to enjoy it in the late afternoon.

The cars were awesome there. And I got released early one day (we started early most days), and I got changed fast and took Green Magic down the famous Route 1 coastline into the heart of Big Sur.
At the final place I stopped at – just beyond Julia Phiefer beach with the rock formation cutout – I pulled in a great turnout with beautiful southern view. There I saw whales galore! Breaching out of the water, I couldn’t count the numbers I saw. I only had my pocket Canon SD camera with me with silly zoom so here’s the best pics I could muster with it.

We had great meals for the trip, frequenting the Tiki Bar restaurant maybe 3 nights. An easy walk for our crew from two hotels.

I’m hoping to go back next year for this event, it was the best assignment I’ve had this year!

[![IMG_0102](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0102.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0102.jpg)

My beautiful hotel room views out both directions:
[![my hotel view Monterey GMM 4 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/my-hotel-view-Monterey-GMM-4-PM-500x282.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/my-hotel-view-Monterey-GMM-4-PM.png)[![my hotel view Monterey GMM 9 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/my-hotel-view-Monterey-GMM-9-PM-500x281.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/my-hotel-view-Monterey-GMM-9-PM.png)
Riding down Route 1 to Big Sur Saturday.
[![IMG_0104](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0104-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0104.jpg)

[![IMG_0107](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0107-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0107.jpg)

[![IMG_0111](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0111-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0111.jpg)

[![IMG_0120 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0120-copy-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0120-copy.jpg)

[![IMG_0125](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0125-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0125.jpg)

[![IMG_0126](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0126-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0126.jpg)The magical view from Nepenthe restaurant.
[![IMG_0140](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0140-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0140.jpg)

[![IMG_0142](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0142-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0142.jpg)

[![IMG_0150](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0150-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0150.jpg)

[![IMG_0158](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0158-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0158.jpg)

[![IMG_0165](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0165-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0165.jpg)

[![IMG_0167](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0167-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0167.jpg)Julia Pheiffer beach, near where I turned around.

[![IMG_0172](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0172-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0172.jpg)

[![IMG_0182 cropped whale](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0182-cropped-whale-500x277.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0182-cropped-whale.jpg)

[![IMG_0203 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0203-copy-500x288.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0203-copy.jpg)Those are the flukes of the whale’s tail. Also the back of one coming out of the water highest mark above the rocks.

[![IMG_0189](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0189-e1446176002306-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0189-e1446176002306.jpg)
[![IMG_0206](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0206-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0206.jpg)

[![IMG_0207](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0207-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0207.jpg)

[![IMG_0212](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0212-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_0212.jpg)
The Bixby Bridge.

[![IMG_2773](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2773-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2773.jpg)

[![IMG_2770](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2770-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2770.jpg)Riding down to Monterey on day one. Wednesday. Zipped out of work in Belmont asafp, to beat some traffic and make a crew dinner in Monterey. Got there a little early so snapped some pics just north of town. Had to pack a fair bit of gear for 4.5 days and a shirt and tie and fancy shoes for Sunday.

[![IMG_2956](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2956-e1446228350100-358x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2956-e1446228350100.jpg)

[![IMG_2967](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2967-e1446228951829-500x176.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2967-e1446228951829.jpg)

[![IMG_2945](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2945-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2945.jpg)

[![IMG_2952](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2952-e1446228985409-500x191.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2952-e1446228985409.jpg)My favorite car of all time: 1966 Ford GT40. I would own one if I could! Sex on wheels.

[![IMG_2997](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2997-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2997.jpg)

[![IMG_2868](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2868-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2868.jpg)

[![IMG_2875](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2875-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2875.jpg)Hotel and balcony and a breakfast morning. The early breakfast moments each morning right by the water were sublime.

[![IMG_2846](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2846-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_2846.jpg)Grill of the 1937 Delahaye 145 Franay Cabriolet.

[![IMG_3013](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3013-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3013.jpg)
Last full day work there: Sunday show. Yes, that’s an actual ’65 Shelby Cobra Daytona.