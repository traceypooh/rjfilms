---
title: 'Got the Fox Twin Clicker shock just now!'
date: Fri, 29 Aug 2014 16:36:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image4-1024x646.jpg
---

Just got the shock! And here’s some pics. I was so excited…

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image4-1024x646.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image4.jpg)  
The original Fox box was inside. A little time capsule.  

[![Fox box](https://rjfilms.com/greenmagic/wp-content/uploads/2014/08/image-1024x766.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/08/image.jpg)

Fox box

It came with the original receipt, Warranty Registration Card, and the instructions!  
Receipt dated: 4/28/85 sold for $328.88 which included $2.88 for shipping.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image5-1024x704.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image5.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image3-1024x717.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image3.jpg)