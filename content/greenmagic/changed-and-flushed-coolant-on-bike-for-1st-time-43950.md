---
title: 'Changed and flushed coolant on bike for 1st time 43,950'
date: Sat, 15 Jun 2013 17:15:00 +0000
author: rj
draft: false
---

Quite happy about it, the week prior I had discovered no coolant visible under rad. cap, and overflow reservoir was so dirty it looked like none in there as well. So had been freaking out that I’d been driving with very little to no coolant. Never had a leak since August 2012, so I started to also worry about fun things like a blown head gasket.

Anyway, All is well! There was coolant in there, maybe 1/2 to 2/3 of what it should be and looked ok, so out with the old and in with the Prestone Ethylene Glycol! Now I can dare to rev that motor and ride again!

In fact, I’m heading right out after it’s all done!