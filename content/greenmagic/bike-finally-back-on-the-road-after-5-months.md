---
title: 'Bike finally back on the road after 5 months'
date: Sun, 10 Feb 2013 14:07:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image-300x225.jpg
---

Woot! Bike is running again (bad spark plug wires had me stumped for a long time), and finally solved my battery draining in 3 mile issue as well!  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image.jpg)  
On a 20 mile loop yesterday. It was 60 + F here and sunny but we got a later start.  
More trips and pics to come.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image1-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image1.jpg)  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image2-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image2.jpg)

First time in a long time I’ve been actually been able to do this.  
My bike races crazy on the revs about 6-7K in 2nd or 3rd gear. Ugh I hope it’s not…  
(Still getting used to this bike).

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image3-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/image3.jpg)