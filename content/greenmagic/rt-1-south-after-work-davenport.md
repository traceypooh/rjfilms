---
title: 'Rt 1 south after work. Davenport'
date: Fri, 14 Nov 2014 21:07:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/image-400x300.jpg
---

Fun trip south on the 1. Drove into Half Moon Bay since was in the Peninsula anyway. Love the HMb area. Kept driving south past Pigeon Pt lighthouse and got to cute little Davenport.  
Was a getting dark / cold / facing rush hr type of ride back east but it was worth it!  

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/image-400x300.jpg)](https://rjfilms.com//blog/greenmagic/rt-1-south-after-work-davenport/image-97/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/image1-400x300.jpg)](https://rjfilms.com//blog/greenmagic/rt-1-south-after-work-davenport/image-98/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/image2-400x300.jpg)](https://rjfilms.com//blog/greenmagic/rt-1-south-after-work-davenport/image-99/)

  

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/11/image-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/11/image.jpg)