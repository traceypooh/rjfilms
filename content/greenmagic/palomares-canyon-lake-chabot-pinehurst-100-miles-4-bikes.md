---
title: 'Palomares Canyon & Lake Chabot / Pinehurst 100 miles 4 bikes!'
date: Sun, 16 Jun 2013 20:28:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8762-150x150.jpg
tags: ['Touring']
---

Just rode 100 miles with closest friends this past weekend. My best friend flew in and rented Ducati Multistrada, My girlfriend went two up with me on Green Magic – Kawasaki ZX6-E Ninja, Harley 1200 sportster, and ’78 Hondamatic 400A.

[![IMG_8762](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8762-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8762/)

[![IMG_8771](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8771-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8771/)

[![IMG_8775](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8775-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8775/)

  

[![IMG_8777](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8777-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8777/)

[![IMG_8778](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8778-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8778/)

[![IMG_8776](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8776-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8776/)

  

[![IMG_8782](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8782-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8782/)

[![IMG_8812](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8812-e1371677829205-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8812/)

[![IMG_8803](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8803-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8803/)

  

[![IMG_8819](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8819-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8819/)

[![IMG_8814](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8814-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8814/)

[![IMG_8823](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8823-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8823/)

  

[![IMG_8831](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8831-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8831/)

[![IMG_8822](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8822-e1371677924150-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8822/)

[![IMG_8845](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8845-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8845/)

  

[![IMG_8842](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8842-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8842/)

[![IMG_8850webbed](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8850webbed-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8850webbed/)

[![IMG_8851](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_8851-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_8851/)

  

[![5friendsTourJune2013Bikes](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/5friendsTourJune2013Bikes-150x150.jpg)](https://rjfilms.com//blog/greenmagic/palomares-canyon-lake-chabot-pinehurst-100-miles-4-bikes/5friendstourjune2013bikes/)

  

Oakland Lake Merritt to Montclair. Meet friends, check out gear and bikes. Load out.

Skyline past Observatory. Great road! Down Redwood and into Castro Valley. Down to Hayward and Buffalo Bills for lunch. Sneaky side roads that are fast parallel roads to 580 East to cute old town of Sunol, and great old historic Union Pacific train pulled in. Just before Sunol swapped with Ducati rider and my girlfriend and I did an accidental wheelie in 2nd gear (just for a second). (also my first wheelie ever).

On to Niles Canyon Road and then up Palomares past two small vineyards. I hopped on the Harley for the first time here and it was fun and rumbly. It was hot all day 85 degrees and sweating if in the sun and not riding.

Then off to a friends BBQ / birthday party on way back in Moraga. Happy 40th Karthik! Got home late again but a great fun day of 100 miles and everyone was safe and happy and bikes all worked great!