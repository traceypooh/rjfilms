---
title: 'NorCal Morgan Territories yesterday! Gorgeous loop'
date: Sun, 17 Feb 2013 20:33:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1084-300x224.jpg
tags: ['Uncategorized']
---

A riddle for you. Two twins riding two twins and an E.

So anyway… three of us went on this 86 mile loop yesterday out of Oakland, CA. Mid 60s Degree F (15 + degrees C for you other lot). I was hot most of trip.

Perfect riding weather. My bike made the whole trip. Much smoother shifting just after oil change, and my low 2500-2400 rpm stumble uphill is pretty much gone!

Morgan Territories is like escaping to a whole other world out here…

We went the long way round Mt Diablo.

![MotoMorganTerritory1084](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1084-300x224.jpg)

![MotoMorganTerritory1015lore](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1015lore-300x225.jpg)

Start of trip more or less. Redwood / Pinehurst back roads thru the Redwoods out of Oakland towards Moraga

![MotoMorganTerritory1035](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1035-300x224.jpg)![MotoMorganTerritory1038lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1038lores-300x225.jpg)

Windy single track road for 7 miles! Brilliant.

[![MotoMorganTerritory1041lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1041lores1-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1041lores1.jpg) ![MotoMorganTerritory1043lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1043lores-300x225.jpg)

[![MotoMorganTerritory1057lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1057lores-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1057lores.jpg) [![MotoMorganTerritory1060lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1060lores-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1060lores.jpg)![MotoMorganTerritory1056](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory10561-300x224.jpg)

At regional park parking lot. Felt like I was in the wild west on a cool summer day.

![MotoMorganTerritory1066](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1066-300x224.jpg)![MotoMorganTerritory1068](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1068-300x224.jpg)![MotoMorganTerritory1073](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory10731-300x224.jpg)![MotoMorganTerritory1074](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1074-300x224.jpg)[![MotoMorganTerritory1087](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1087-300x224.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1087.jpg)![MotoMorganTerritory1080](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1080-300x224.jpg)![MotoMorganTerritory1089](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1089-300x224.jpg)[![MotoMorganTerritory1081lores](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1081lores-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/MotoMorganTerritory1081lores.jpg)

Other bikes: brand new (250 miles) Harley 1200 sportster customized / murdered. The bike sounds very cool and rumbly. (Other) twin on a twin is a HondaMatic CB400A. Cafe racer mod-ed.

I’m from New England – NH – so I was seriously thinking of all the people in the north and east and the bad weather there. I am grateful to get in the warm ride here. Dream for spring and summer.