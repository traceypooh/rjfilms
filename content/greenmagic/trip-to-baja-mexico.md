---
title: 'Trip to Baja Mexico!'
date: Fri, 13 Mar 2015 20:40:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image8-400x300.jpg
tags: ['Touring']
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image8-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image8.jpg)  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image7-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image7.jpg)  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image9-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image9.jpg)Just got back late last night from a trip south of the border.  
Blazed to Mexico from Santa Ana and through Tijuana (been there). Customs was quick and I was through.  
[![Screen Shot 2015-03-17 at 5.06.44 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.06.44-PM-500x281.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.06.44-PM.png)

Tried to find a cute late lunch spot in Rosarito. No such luck. It’s a dirty pit of a city.  
I did however, manage to find Splash, with an excellent view by the water. It’s at KM 52, “Libre Rosarito Ensenada” in the town of Primo Tapia across from the road Via Lactea.  
A 3pm lunch, it was a nice break because of this view:  
[![DCIM102GOPRO](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0131-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0131.jpg)  
Onward to Ensenada. I took the local road here, because it was actually closest to the cliff and the ocean and prettier, I could also cruise a little slower and enjoy the beautiful views. This point was particularly beautiful. I had to stop and snap some pics.  
[![IMG_8221](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8221-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8221.jpg)

[![IMG_8237](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8237-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8237.jpg)

[![IMG_8235](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8235-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8235.jpg)

[![IMG_8233](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8233-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8233.jpg)

[![IMG_8230](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8230-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8230.jpg)

[![IMG_8227](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8227-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8227.jpg)  
location: Rosarito Beach Municipality  
Baja California, Mexico  
32.148632, -116.893150

After the Libre “Free” local road stopped following the beach, it wound under the highway and inland. So, I took a little detour and drove inland on the regular route 1. I’m glad I did, the farm country here was breathtaking. There was also some medium exciting turns throughout the mountains there that I shot GoPro of. I couldn’t believe how green it all was inland, a lucky time of year for Green Magic Man!  
[![IMG_8248 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8248-copy-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8248-copy.jpg) After rising to a little plateau, I decide to double back, hit those turns harder back to the ocean route, cuz I want to see more ocean.  
[![Screen Shot 2015-03-17 at 5.46.35 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.46.35-PM-500x273.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.46.35-PM.png)

The new “scenic highway” toll road 1D is just that: one of the most beautiful roads I’ve ever driven on between Rosarito and Ensenada. Particularly the section north of Ensenada, Salsipuedes Bay, is unbelievable. The route follows majestic high cliffs over the dazzling ocean cut literally through a few mountain ridge backs and around this stunningly gorgeous bay. I GoPro a lot of this portion and stopped multiple times for pics of the amazing view. It’s a fast highway 2 lanes each direction and mostly empty. Nice fresh pavement.  
[![IMG_8250](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8250-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8250.jpg)

[![IMG_8254](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8254-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8254.jpg)

[![IMG_8253](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8253-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8253.jpg)  
I only thought about doing this trip late the night before riding back from a long day at work. I suddenly had all next week off – at least cleared for the moment – so instead of heading north on the 1, I thought about a totally spontaneous trip to Baja Mexico.  
I looked up a few things in the morning, which delayed my leave until 11am, but I did find info that only the southern beaches Playa El Faro, and Playa Estero were worth seeing in Ensenada.  
As it happened, after the grimy, traffic clogged, hot, and nasty downtown I happened to spot a sign for El Faro. I took the right turn until it Ts. Left to Estero right to El Faro. I went left. This beach apparently has been completely taken over by a posh gated resort. I find out, though, you can visit it independently. Which is what I do. I walked around a bit, and figure it’s gonna be expensive here ($87 is “best” rate for a single I’m told later at desk).  
[![IMG_8260](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8260-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8260.jpg)  
[![IMG_8269](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8269-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8269.jpg)Estero Beach resort views.

Walking to the end of the gated property, I slip through a fence, and I spy with my little eye, a much more modest looking area next door north – El Faro. The vantage point here:  
[![IMG_8262](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8262-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8262.jpg)  
So back I go to the fork.  
I follow the the road until the end to the unsurprisingly named El Faro RV resort and camping.  
I speak to the guy at desk, this place is near empty this day, and he quotes me $48 for a single. This is right on the beach, and the best beach in the area, and I really want to stay here. I say, how about a flat $40? He checks with someone and says “ok”.  
The room is pretty huge, with a front living room complete with “retro” couch. [![IMG_8303](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8303-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8303.jpg)The promising looking bath and shower turn out to be less than stellar the next day.  
I ask at check in “cervesa”? And he says I have to drive back down the road towards town. I stash my bag and zip into town and the second place has the stuff. I grab me a Dos Equis and a premixed canned Capt. Morgan and cola. Nice. And wrap it up in my shirt and zip back to the resort / beach.  
I finally get to enjoy a cold beer by the beach as the sun has just set and I sit peacefully until it goes to darkness. The pic is the view from my beach there.  
[![IMG_8273](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8273-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8273.jpg)

[![IMG_8283](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8283-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8283.jpg)  
[![IMG_1688](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1688-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1688.jpg)  
Back into town after dark, I stop at Del Reys Tacos which is a little bigger than a stall on the side of the road. Decent beef tacos (2) and a cheese enchilada and Fanta Orange all for $4.25. On the short ride into town, Green Magic does not like the local heat and stalls often in protest. The road is a hot dusty mess.  
Back to the beach again and have Mr. Morgan accompany me for the dark walk on the beach. [![IMG_8298](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8298-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8298.jpg)  
I walk around and enjoy the peaceful beach and waves gently crashing. I’m the only one on this beach, there’s one street lamp to light the whole thing up (or really only attempting to). I think to myself: it’s about 10pm, I am here by myself, and it’s Mexico. As calm and beautiful as this beach is right now, and as much as I’d like to walk and stay more, I decide I should probably pack it in. I can hear people saying “you got mugged on a Mexican beach at night alone and you’re surprised??!” “Why were you out there alone?” and me having not much to say in response. Hell, I’m weary anyway, so I hit my room.

I scrounge 3 discarded bricks and make a “ramp” and wheel my bike up into my room, as any good motorcyclist to Baja does. My “living room”. [![IMG_8300](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8300-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8300.jpg)

[![IMG_8302](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8302-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8302.jpg)  
Settled in later, my bed is surprisingly nice and firm, quite comfy.  
All I can hear is the ocean, which is quite something to be said if you’re in Ensenada.

[![IMG_8313](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_83131-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_83131.jpg)  
Next day wake up. My view out the back patio door.  
Have the breakfast of champions: old(er) nacho chips, and a peanut butter power bar I absconded from work last week and broken off hardened bits of an 8 day old peanut butter sandwich, I broke bits off to get around the mold blooms. Yummy!  
[![IMG_8304](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8304-400x234.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8304.jpg)  
I eat this on my back deck type of thing. It comes with the older seating enablement devices.  
[![IMG_8315](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8315-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8315.jpg)  
[![IMG_8319](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8319-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8319.jpg)  
[![IMG_8316](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8316-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8316.jpg)

Walk on the beach, it’s pretty tranquil and lovely. Some other tourists about but calm nonetheless. I walked to each boundary, there’s some sort of military property to the north,[![IMG_8357](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8357-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8357.jpg) and the slightly snootier Estero beach resort and the inlet to the south. It’s low tide but I spy a nice deep spot on the estuary / inlet. [![IMG_8330](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8330-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8330.jpg)

[![IMG_8350](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8350-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8350.jpg)  
The point you see there is where I’m headed next.  
[![IMG_8339](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8339-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8339.jpg)  
Back to room, contacts in, Navajo style under shorts – check. Back to beach I dive headfirst into the water here: [![Screen Shot 2015-03-17 at 5.55.02 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.55.02-PM-449x320.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-5.55.02-PM.png)Nice swim, the water I’d estimate was about 62 degrees, not bad for early March! I’m the only one swimming about.

Back to room, fast shower and pack, I’m pushing check out time. So on to that shower review. [![IMG_8321](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8321-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8321.jpg)Firstly, the water stinks of sulfur. And with the water “pressure” I might be able to generate more moisture with my saliva and lick myself clean like a cat does. Oh well, at least the water heater made the water not freezing, although not warm.

[![IMG_8368](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8368-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8368.jpg)  
The resort road to my room.

[![IMG_8370](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8370-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8370.jpg)  
Packed up and checked out. Final view of lovely El Faro beach.

Exiting the resort road.  
[![IMG_8382](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8382-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8382.jpg)  
[![IMG_8384](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8384-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8384.jpg)  
[![IMG_8383](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8383-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8383.jpg)

After packing up and handing in key, I’ve decided I will head to the far south end of the bay and should be something to the drive and something pretty to see right?  
It actually IS quite nice. Not too far, about 25 miles to get to the farthest point. It’s hot, but if I keep moving I can just avoid sweating in my leathers and keep my bike happy. It does NOT like getting hot on this trip. It’s very prone to stalling idling at lights when hot, no matter where I’ve set the idle adjuster screw.

The road gets higher up in the mountain with some nice fun little turns here and there. Very relaxing ride out to the point. [![IMG_8389](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8389-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8389.jpg)  
[![IMG_8397](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8397-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8397.jpg)  
[![IMG_8407](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8407-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8407.jpg)  
[![IMG_8406](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8406-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8406.jpg)

My hotel man tells me about the blowhole or La Bufadora, and that there’s only 3 like it in the world, one here, one in Hawaii, and one, um, elsewhere, I dunno. I recall quickly reading something about that yesterday before I left.  
Mostly I just pointed it to this, um, point, because I felt I should and there must be something worthwhile to see.  
My hotel guy tells me to go see the blow thingee and eat at Bananas which is good (it’s not really – or at least what I get there later is not good).  
So Bananas, huh? You’ve been there. A super touristy spot. It’s three stories overlooking the water at lands end, feeling like a small version of Cancun’s Señor Frogs. [![IMG_8410](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8410-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8410.jpg)  
Subpar fish tacos. Just fried white fish without much flavor and over priced for Mexico. Whatever, at least I could see the ocean and the bike from my table. [![IMG_1692](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1692-e1427067750354-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1692-e1427067750354.jpg) My table and Green Magic down below.  
After lunch I walk to La Bufador. At first it’s a little underwhelming. But wait for it… I hang around longer than the other people and some pretty spectacular eruptions occur at odd intervals. I’m talking like a 60 foot ejection of seawater, and once all over a crowd of people at the observation area! I GoPro, canon camera it all. It was pretty damn cool. [![IMG_8445](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8445-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8445.jpg)  
[![IMG_8444](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8444-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8444.jpg)

Back on the road to head north. I don’t know it at the time, but I pretty much wind up riding the whole back to The U.S. and on to Santa Ana with very few breaks. In hindsight I probably should have stopped at either of those spots on the way up for an early dinner by the sea. But I didn’t and I guess I was worried about the border crossing.

Salsapuedes Bay rest area overlook on the way back north:  
[![IMG_8455](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8455-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8455.jpg)

[![IMG_8473](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8473-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8473.jpg)

[![IMG_8457](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8457-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8457.jpg)

Speaking of which, super hot in TJ, the signs make little sense near the border area. I lane split probably 300 cars easily all stacked up going nowhere. I probably cut 2 hours off my border crossing. I also made an illegal lane change from some improperly signed route to the border which turned out to head back downtown (Bah, Tijuana!), so I slow down by the rows of jersey barriers and at the near last point dodge the small gap in some barriers and back onto the right 4 lane road (which is oddly empty at the moment), to go right into the border checkpoint.[![Screen Shot 2015-03-17 at 11.45.30 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-11.45.30-PM-439x320.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/Screen-Shot-2015-03-17-at-11.45.30-PM.png)

I zip through pretty quickly, I cut my motor and push it the final 12 car lengths to the border dude. Again, Green Magic is acting up and rebelling against any real heated idling.  
Once through I stop at the 2nd US exit and fill up on cheaper and realer (yeah, that is a word hehe), U.S. premium gas. I’m thinking at this point it’s about 6pm and I should find a spot to eat and take a long break. But I’m not really hungry (really? really), so I push on. Then in my head I can’t figure out where to eat anyway, and I do recall recently finding again the great mexican spot my friend took me to in Santa Ana. So I resolve to power up there and eat when back in town.  
I ride the setting sun out and make a final rest area stop about 50 miles from Santa Ana. It’s been a looonnnggg time in the saddle, really since, the southern bay tip of Ensenada, so I gotta move them muscles and pace around the rest area. Re-insert my ear plugs because I didn’t do a very good job last time and the roar on the freeway has been near deafening and certainly annoying. Jam them in good and now I can enjoy some quiet heavier throttling on the super slab north.

Funny thing was, I arrived back in Santa Ana with my takeout (chicken burrito with extra diabla sauce on the side) the same moment my friend pulled in from work at about 8:45pm. So we sit and enjoy some wine together and get caught up.

More Photo and video links to be posted here soon.

Mileage: start 52,827 to 53,290 end trip  
I think I’m well past my oil change. And the bike seems to use oil a bit too.