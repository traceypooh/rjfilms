---
title: 'Work on the 1978 CB400A top end engine project begins in earnest'
date: Sat, 22 Aug 2015 23:45:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3668-428x320.jpg
tags: ['repair']
---

Finally tore into the engine oil blown gasket issue today on the Honda CB400A motorcycle. Doing it as a favor to help a friend get the bike back in sellable condition.

The lower cylinder to crank gasket blew while I was riding it on the highway.

[![IMG_3668](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3668-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3668.jpg)  
Got the tank off and the valve cover off. Will adjust the valve timing tomorrow.

The oil which was way overfull with oil or fuel leaked into it through the carbs and into the intake and down through the piston rings into the crank. This caused intense pressure and this model doesn’t seem to have a crankcase vent so I guess the valve cover vent serves that purpose. The pressure could not be released fast enough and blew the lower seal out. Evidence abounds that recently oil sprayed out of the upper hose connection to the air box, and oil all over everything where the tube exits the air box to drain by the back wheel. It blew oil all over that whole area – it should not have, should just be steam and hot air coming out as needed.

There are two plans, A & B.  
Plan A:  
\-Replace all the (contaminated) oil.  
\-Re-Torque all the head bolts – which apparently are weak on this model and loosen  
\-Adjust the valve timing while in there anyway.  
Button it all back up and test fire it up to see if the leak goes away.  
I give this about a 1% chance of working tomorrow. But 4 people advised me to try this first.

Plan B:  
Go back into it.  
\-Remove valve cover  
\-Remove the head bolts  
\-Remove the valve train and rocker arms and lobes  
\-Remove the cylinder head.  
\-Removed the cylinders themselves.  
\-Install all new gaskets all around.

[![IMG_0236](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0236-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0236.jpg)

[![IMG_0233](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0233-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0233.jpg)