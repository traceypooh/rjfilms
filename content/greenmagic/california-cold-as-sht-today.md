---
title: 'California cold as sh&t today'
date: Sat, 07 Dec 2013 21:00:00 +0000
author: rj
draft: false
---

Had to drive to work about 100 miles round trip. The morning at 10 am in the sun was really cold – about 40. My rear tire slid early on some very cold tarmac.  
The ride home in the dark was worse. My fingers went totally numb. My thinsulate leather gloves were not up to the task. 40 degrees and not the right gear made for a very un fun ride.  
On the plus side, I had Redwood Rd all to myself, not one motorcyclist or better yet not one car for twenty miles. I also noticed a strange thing riding over the pass in the mountains. Something flickering in my side vision. Eventually I realized I wasn’t imagining it or cold hallucinating it but it was a large bird or even an owl and it was flying right in front of my bike along with me in my headlights for at least well maybe 200 yards or so. I guess it was using my light. Was weird but cool.