---
title: 'Morgan Territories my Girl and Oktoberfest Clayton'
date: Sat, 05 Oct 2013 19:40:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/Panoramic-GMM-morgan-territoriesJ2Kwide-Crop-Sq-GM-150x150.jpg
---

Drove 110 miles today 2up with my Girlfriend on the back. Really fun, it was a hot day in 82 + temps and full sun. On loop around Mt. Diablo, we stopped for drink in Clayton (last call for hydration or anything really) and discovered an ‘event’ there – Oktoberfest! It was awesome fun, glass beer steins everywhere on the street. Split an Oktob brew on pizzeria patio in the shade and a lot of water and headed to Morgan Territories. Beautiful, stopped in the park to stretch / relax.

[![Green Magic Man, ZX6e motorcycle Morgan Territories](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/Panoramic-GMM-morgan-territoriesJ2Kwide-Crop-Sq-GM-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morgan-territories-with-girlfriend-and-oktoberfest-clayton/green-magic-man-zx6e-motorcycle-morgan-territories-4/)

[![IMG_2687](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/IMG_2687-150x150.jpg)](https://rjfilms.com//blog/greenmagic/img_2687/)

[![Oktobo-tent-IMG_2690](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/Oktobo-tent-IMG_2690-150x150.jpg)](https://rjfilms.com//blog/greenmagic/oktobo-tent-img_2690/)

  

[![MorganTerr moto CK 2013_9238](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9238-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9238/)

[![MorganTerr moto CK 2013_9241](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9241-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9241/)

[![MorganTerr moto CK 2013_9220](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9220-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9220/)

  

[![MorganTerr moto CK 2013_9229](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9229-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9229/)

[![MorganTerr moto CK 2013_9226](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9226-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9226/)

[![MorganTerr moto CK 2013_9225](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9225-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9225/)

  

[![MorganTerr moto CK 2013_9230](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9230-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9230/)

[![MorganTerr moto CK 2013_9250](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9250-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9250/)

[![MorganTerr moto CK 2013_9252](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9252-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9252/)

  

[![MorganTerr moto CK 2013_9254](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9254-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9254/)

[![MorganTerr moto CK 2013_9247](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9247-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9247/)

[![Green Magic Man, ZX6e motorcycle Morgan Territories](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/Panoramic-GMM-morgan-territoriesJ2Kwide-Crop-GM-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morgan-territories-with-girlfriend-and-oktoberfest-clayton/green-magic-man-zx6e-motorcycle-morgan-territories-2/)

  

[![Green Magic Man, ZX6e motorcycle Morgan Territories](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/Panoramic-GMM-morgan-territoriesJ2Kwide-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morgan-territories-with-girlfriend-and-oktoberfest-clayton/green-magic-man-zx6e-motorcycle-morgan-territories/)

[![MorganTerr moto CK 2013_9262](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9262-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9262/)

[![MorganTerr moto CK 2013_9266](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9266-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9266/)

  

[![MorganTerr moto CK 2013_9277](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9277-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9277/)

[![MorganTerr moto CK 2013_9273](https://rjfilms.com/greenmagic/wp-content/uploads/2013/10/MorganTerr-moto-CK-2013_9273-150x150.jpg)](https://rjfilms.com//blog/greenmagic/morganterr-moto-ck-2013_9273/)

  

Cute farmland, gorgeous really. I felt my passenger falling asleep several times in and out, that was scary! Had to stop in Castro Valley Peet’s to get her energized. Fun twisties back kept us both alert.

In Morgan territories at the big right turn, eased out clutch, half throttling, and pinned it in 2nd all the way to 14,000 redline! I was kinda nutty, hadn’t been past 10K on Green Magic yet. I tell ya, though, the weight of pillion makes a huge performance difference and forget very aggressive cornering. Bottoms out on bumps and wallows in bumps in turns big time and scrapes center stand, a big crunch there underfoot. Still… so much fun.