---
title: 'Lake Chabot loop with Bailey and Ducati Multistrada'
date: Sun, 16 Jun 2013 20:15:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1813-150x150.jpg
---

[![IMG_1813](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1813-150x150.jpg)](https://rjfilms.com//blog/greenmagic/lake-chabot-loop-with-bailey-and-ducati-multistrada/img_1813/)

[![IMG_1810](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1810-150x150.jpg)](https://rjfilms.com//blog/greenmagic/lake-chabot-loop-with-bailey-and-ducati-multistrada/img_1810/)

[![IMG_1812](https://rjfilms.com/greenmagic/wp-content/uploads/2013/06/IMG_1812-e1371674275479-150x150.jpg)](https://rjfilms.com//blog/greenmagic/lake-chabot-loop-with-bailey-and-ducati-multistrada/img_1812/)

  

After we both changed the coolant on Green Magic, we picked up rental 2013 Ducati Multistrada 1200cc bike for 1 day rental.

Since we got it early on Sat before 5pm, of course we’re going riding!

Very fun and fast for me 44 mile turn turn turn ride with a walk in between at lovely Lake Chabot.