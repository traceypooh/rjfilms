---
title: 'Loving the new exhaust modification!'
date: Sun, 22 Nov 2015 20:04:00 +0000
author: rj
draft: false
---

Rode out to San Pablo Dam road and met a friend. We rode out a scenic way on Castro Ranch Road to Pinole Valley Road and onwards to Crockett and on to Port Costa and a stop at Warehouse Cafe.  
We had a brew and shot some pool.  
My friend has a ’96 CBR 959 RR with a 4 into 1 stubbed exhaust. His exhaust sounds pretty cool, I’ve liked it every time I hear it.  
But you know what? The modified stock dual cans on my ZX6E represent!!  
He mentioned several times that he really liked my exhaust note compared to how it was before and how good it sounded.  
We couldn’t resist a little rev testing side by side at Port Costa before we drove off into the cold night.

A fun night out, and I love this new exhaust!!