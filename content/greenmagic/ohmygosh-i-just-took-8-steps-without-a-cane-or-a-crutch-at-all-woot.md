---
title: 'OhMyGosh!! I just took 8 steps without a cane or a crutch at all!!!!!! Woot!!'
date: Tue, 20 Jun 2017 07:29:00 +0000
author: rj
draft: false
tags: ['Uncategorized']
---

This is meaningless to anyone who has not broken a lower limb… but for me it is 100% thrilling and astonishing!  
I was so far behind all weekend on the Cape for where I need to be by tomorrow for a 9 day grueling job.  
I’ve gambled everything on this – all cascading decisions rest on me being able to walk – a lot – at least with a single crutch or cane. But really the very best is with nothing at all except the boot.  
Yesterday was huge – at end of PT session I was somehow able walk on a single cane. I couldn’t believe it. I had worked so hard Saturday and Sunday over at First Encounter beach.  
I was cursing and so angry I couldn’t walk on a single device at all. I tried so hard. I wanted to slam my crutch into the earth and destroy it.  
I worked my leg so hard my upper calf went into a spasm, and on Monday the PT girl said my leg was completely knotted up and worked it so hard to try to break up the huge muscle knot and tear. It killed – smashing her thumb into it was the most pain of this whole experience since a few days after surgery by far.  
My calf is still a very weak shadow of itself, and after walking the driveway a few times with single cane and taking those 8 steps free (with my boot of course – that’s on for the next 2+ weeks), my calf started the cramping up process again. Not super painful or anything, but I know that’s it’s knotting up in protest from abuse.