---
title: 'Mines Road mile 17, Delvalle Park, Palomares'
date: Tue, 05 Jul 2016 21:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5794-1000x750.jpg
---

Did a great 150 mile joyride today.  
[![IMG_5794](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5794-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5794.jpg)  
I went down Palomares Canyon after the 100+ turns on Redwood and stopped by two wineries for pics. A guy who worked at one that was closed was really nice and walked down to the gate and chatted me up for a bit and told me about their September black and white horror movie tradition there. Hoping to catch one this year.  
[![IMG_5784](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5784-1024x715.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5784.jpg)

[![IMG_5786](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5786-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5786.jpg)  
[![IMG_5783](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5783-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5783.jpg)  
Rode down to Calavares and had intended to do that road, but it turns out it’s closed for the next 18 months M-F for work on the dam. So had to double back and rack my brain where to go ride. I remembered a wrong turn once over a short stunningly beautiful pass and then back down to a park.  
I finally figured it was a wrong turn off Mines Road and headed over that way up 84. Sheesh the commuter hour traffic was awful although I enjoyed bobbing and weaving behind a standard adventure bike.  
Finally got onto Mines around away from the crazies, and was loving it.  
[![IMG_5787](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5787-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5787.jpg)  
Delvalle park was late enough to drive right into and check it out. West Beach area seems nice to return to one day for a BBQ / swim.  
[![IMG_5792](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5792-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5792.jpg)

[![IMG_5797](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5797-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5797.jpg)

[![IMG_5796](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5796-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5796.jpg)  
[![IMG_5800](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5800-1024x707.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5800.jpg)

[![IMG_5794](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5794-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5794.jpg)  
Then I rode back to the Mines Road turnoff and up that beautiful road to mile marker 17.  
[![IMG_5790](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5790-942x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5790.jpg)

[![IMG_5789](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5789-1024x702.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5789.jpg)

mile 17 marker where I turned back around:  
[![IMG_5802](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5802-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5802.jpg)  
I was battling time and fuel. My tank was on ‘E’ and I was 12 miles past switching to reserve when I turned back towards Livermore.  
[![IMG_5804](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5804-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_5804.jpg)  
At one point I was so worried about fuel and making my night flight that I shut off the motor and coasted a couple miles down the pass. I made it to downtown Livermore and filled up.  
Then I got asked to work another day out west… so I wound up not taking that flight after all!  
Was getting cold anyway, and a cold ride back to Oakland awaited me. I picked up Champa Gardens take out for 3 of us and stashed it in my tank bag and brought it up the hill.

Mines Road really charmed me today. I’m going to spend a bunch more time riding that road now.  
I did have one slide on the way out – which was pretty ridiculous as I was putt-putting along going slow to conserve fuel.