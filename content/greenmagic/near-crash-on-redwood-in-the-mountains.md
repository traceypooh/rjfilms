---
title: 'Near crash on Redwood in the Mountains'
date: Thu, 19 Nov 2015 18:29:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3432-427x320.jpg
---

Been very very cold here for Cali.  
High 30s overnight and Low 40s in the morning and the mountain pass I ride has ‘Icy’ signs on it permanently up. So today I ride slow old man pace on the mountain pass.  
I hit a turn I do at 50mph all the time at 35mph on a cold day and for no reason lost the front end. Bike was almost flat horizontal in the turn, I was going down and crashing, and SOMEHOW the front tire held it’s grip and I made it out crash free. In fact, I haven’t related this story anywhere yet, and I’m going to post up what the tire looked like in the slide. It was crazy. (I examined the turn at great length. No ice, no water, no oil, no reason to have a slow barely leaning bike wash out on the front end. Only thing was there were a few lined up ridges in the corner – perhaps it induced a mini speed wobble – although the handle bars didn’t shake, the bike just nearly went down to pavement suddenly.) Can’t explain but it was crazy.

I wish I could understand what happened so I could learn from it and avoid it in the future.

![IMG_3432](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3432-427x320.jpg)

![IMG_3446](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3446-427x320.jpg)

![IMG_3444](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3444-e1454772867447-240x320.jpg)![IMG_3443](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3443-e1454772887628-1000x750.jpg)

![IMG_3457](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3457-427x320.jpg)