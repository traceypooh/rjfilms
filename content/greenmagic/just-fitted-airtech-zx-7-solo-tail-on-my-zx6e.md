---
title: 'Just fitted AirTech ZX-7 solo tail on my ZX6E'
date: Sat, 07 Sep 2013 16:03:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image17-300x196.jpg
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image17-300x196.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image17.jpg)

It’s actually a lot easier than I thought to do, and quick to at least get it on.

NO subframe modification necessary! I was wondering a lot about that. Slipped right on. Optional remove rear undertray and other bits for cleaner look. There are two mount points on the tank for tail, and the integrated seat (mod) goes into normal stock holding positions. The Reserve switch detent on the fairing is Waay off, no chance you’re gonna use reserve switch unless you custom hack a cutout on the AirTech for the E.

Here it is gently wiggled onto the frame, least easy part was just getting the seat into the tabs and clicking in but still easy.  
Oh yeah, you’re going to need to remove / relocate / disconnect, whatever, the keyed seat release. Obviously I recommend you move it under the rear part of tail so you can either key in from there or pull it out to key seat release.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image18-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image18.jpg)

There, that’s a lot better. Only lose back third of wheel arch, as you just need one segment out. Again you could simply cut lower sides off and still have it work, but I’m going to keep it as is and off bike and make a little plastic rectangle or something to shelf canister and tool kit on hidden underneath. There’s still a lot of space especially without tail light or a flatter LED setup.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image19-300x241.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image19.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image20-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image20.jpg)

[![zx6e_zx7_solo tail_GreenMagic_stock fairing removed](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_stock-fairing-removed-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_stock-fairing-removed.jpg)

This is a bone stock CA model setup. Note charcoal canister and (actually stock Katana 600) tool set. You could keep them, though it would not be easy to get to tools. EDIT: You could actually easily fish them out the back, depending on if you fit a brake light etc and how you do it.  
Probably going to get an all in one integrated LED tail light with built in blinkers.

[![zx6e_zx7_solo tail_GreenMagic_brakelight_removed](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_brakelight_removed-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_brakelight_removed.jpg)

Next: Remove the tail light. (4) 12 mm bolts. Disconnect all the wiring. You’ll need another light unless you like the lawless thing or just track your soon to be prettier friend.  
Note: in this pic the keyed seat release has been unbolted. It’s hanging down off frame.

[![zx6e_zx7_solo tail_GreenMagic_coolant](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_coolant-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_coolant.jpg)

From the rear: Coolant reservoir not moved at all, stock position. Also interesting, this setup will allow more airflow to regulator.

[![zx6e_zx7_solo tail_GreenMagic_underside](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_underside-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic_underside.jpg)

So here’s that newer exposed area. The grey frame here is about 10″ wide and has four holes already in that area not being used, so I plan to rig up the new flush mounts in the outer two holes, and the LED in the center. Want it all nice and small and tidy. Will hang plate just under it. It should look pretty clean when done. Also plan to cover the exposed sub frame rails on the sides with probably white or black stickers to make it a little less noticeable.

[![zx6e_zx7_solo tail_GreenMagic](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/zx6e_zx7_solo-tail_GreenMagic.jpg)