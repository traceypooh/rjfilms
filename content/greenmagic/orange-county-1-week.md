---
title: 'Orange County – 1 week'
date: Wed, 11 Mar 2015 23:59:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0042-400x300.jpg
tags: ['Touring']
---

Got to my friend’s place last night Wednesday at 9:45pm. Made it from Oakland in 9 hours which was pretty remarkable I say.  
I commuted for the next 6 of 7 days to Dana Point from Santa Ana.  
I had some long midday breaks and one day early out, so my friend and I met up for some SUP – Stand Up Paddleboarding twice.  
[![DCIM102GOPRO](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0042-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0042.jpg)

[![DCIM102GOPRO](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0071-no-tags-SUP-parking-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0071-no-tags-SUP-parking.jpg)

Friday night we worked until midnight and had a short 7am turnaround, so the company was nice enough to put me up in a gorgeous local hotel: the Laguna Cliffs (Marriot). We had another 5 hour midday break so I went over to the hotel and checked in and lounged about in peace for a few hours. [![IMG_1660](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1660-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1660.jpg)  
[![IMG_1661](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1661-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1661.jpg)

My little back deck was nice to hang out on, with the ocean in the background.[![IMG_1663](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1663-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1663.jpg)  
Been a lot of activity for days and not much sleep so I kind of nodded off on the amazingly comfy bed. Set an alarm, shook myself up, showered and finally got to shave before heading back to the other hotel for the ‘night shift’ 6pm – midnight. They fed us a nice dinner although that does not deter the crew from heckling “What’s for chicken?” most meals. No complaints here, I love not having to worry about meals. And they were chock full of healthy choices being So Cal – I loved it.  
[![IMG_1665](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1665-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1665.jpg)  
6:15 am view off my deck the next morning to go back to work.

Saturday I ride back to Santa Ana, and my friend has and old friend in town, so we grill up a nice sumptuous dinner with wine. I man the filet mignon. I’ve never cooked this before, but I get good instructions from Bob and on an iron skillet very hot on the propane grill I cook the 3 steaks in butter and more butter. I turns out really delicious, medium rare and tender and moist. Bob grilled up a ton of terrific salmon steaks along with buttery corn on the cob. And a potato and cheese things and I threw together a salad. So good, I want to go back and eat that dinner again!  
[![norton reserva malbec exc wine B and S IMG_3499](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/norton-reserva-malbec-exc-wine-B-and-S-IMG_3499-239x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/norton-reserva-malbec-exc-wine-B-and-S-IMG_3499.jpg)

[![filet mignon IMG_3501](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/filet-mignon-IMG_3501-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/filet-mignon-IMG_3501.jpg)

[![filet mignon meal IMG_3500](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/filet-mignon-meal-IMG_3500-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/filet-mignon-meal-IMG_3500.jpg)  
I wind up making some salmon, salad, and chipotle mayo tortilla wraps for lunch in the days to come. Yummy!

On Sunday I had a short easy setup day, so rode over to Baby Beach and met Bob again to go Paddleboarding. Changed there and locked my gear in his car. Off we paddled. This time we paddled over to Doheny beach, met Ron, and all tried surfing the small waves there. Perfect weather and very fun, but kind of hard to balance on these boards as they were not the surfing shaped one, but slender and built for speed. So I fell in a lot.  
We then got cleaned up and rode out to Cook’s Corner with his wife pillion afterwards.  
[![grab ride to cooks corner](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/grab-ride-to-cooks-corner-500x281.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/grab-ride-to-cooks-corner.png)  
Here’s the LA County / Orange County well known biker bar at Cook’s Corner:

[![IMG_8206](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8206-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8206.jpg)

[![IMG_8204](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8204-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8204.jpg)

That blue flamed bike is a real man’s bike. A custom S & S crate motor-ed bike 1300cc. Loud as hell, shakes itself apart with so much vibration, and trust me, that thing hauls ass. I couldn’t believe how fast that thing was with my friend on the throttle and his wife 2Up behind him.  
We got some food there, and when we went to leave the custom huge bike’s headlights were out. After popping the seat, battery and battery box looking for the fusebox and or bad wiring and covering my hands in oil, we decided we would ride back in formation, with my headlight on hi beam and his wife pillion on my bike (for safety). It worked out fine, and he sent the bike in the next day to get worked done anyway.

Another long 5 hour break on Tuesday, so I rode up to Laguna Beach to re-visit it again on a bike. I really like that walk in Heisler Park on the cliffs above the water (near the fancy restaurant, Las Brisas I went to in 2011). It was too bad the weather was cloudy or the pics would have been really nice.  
[![DCIM102GOPRO](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0090-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0090.jpg)

[![DCIM102GOPRO](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0112-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/GOPR0112.jpg)

The job down here and the experience was great. I had a blast all around. The food was quite good at the St Regis in Dana Point all week too. Mellow work days, with plenty of time off but yet netting nice overtime days too!

Last day of work was Wednesday there. I get an email later in the day that a job for next week has been postponed 10 days… so suddenly don’t have a job I have to ride north and fly back east for… So riding back late at night, I start hatching a plan instead of riding north tomorrow… what about the other direction to Mexico?…