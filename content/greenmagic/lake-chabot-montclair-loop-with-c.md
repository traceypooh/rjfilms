---
title: 'Lake Chabot, Montclair loop with C'
date: Sun, 10 Nov 2013 20:38:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/12/GM-lake-chabot-nov-10-2013-photo-300x224.jpg
---

Drove up to T & H house and had late bday celeb. Got new AlpineStars gloves! Drove to Castro Valley and hit Don Julio’s for a food break, that was nice. Walked about Lake Chabot as it was getting colder and darker. [![GM lake chabot nov 10 2013 photo](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/12/GM-lake-chabot-nov-10-2013-photo-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/12/GM-lake-chabot-nov-10-2013-photo.jpg)