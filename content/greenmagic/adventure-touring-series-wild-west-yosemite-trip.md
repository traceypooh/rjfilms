---
title: 'Adventure Touring Series: Wild West Yosemite trip'
date: Fri, 19 Sep 2014 23:59:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7766-copy-1000x750.jpg
tags: ['sport touring', 'Touring', 'yosemite', 'Zx6e', 'zzr600']
---

Just did an epic two day 559 mile trip to **Yosemite.**[![IMG_7766 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7766-copy-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7766-copy.jpg)  
Travel via 132 to Coulterville, the famous 49 highway (of “49′er” fame) to Mariposa and beyond to Oakhurst. Then the unknown and into the original entrance to Yosemite: the stagecoach mountain dirt road and 9 miles of it.  
Planned this route the week before but due to extreme hot temps, a high of 90-95 degrees in Yosemite and over 100 on the way there, and smoke choked Yosemite Valley due to two massive forest fires I postponed until today. And even then, suddenly rain was expected some or all morning in the Bay Area and that’s not a fun start. Don’t have everything waterproof, so waited out some of the rain… and went for it![![gloom rain start photo 2](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/gloom-rain-start-photo-2-300x224.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/gloom-rain-start-photo-2.jpg)  
Rainy view from window greets me at 9am from Berkeley Hills. Not encouraging. But I’m determined to go. [![ninja coffee](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/ninja-coffee-400x205.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/ninja-coffee.jpg)  
At least I started the day out properly. (note coffee mug) lol.  
Departed at 10am. First stop Oakland hills to borrow a replacement Canon camera. Short stop with some excited chatting with my typical riding buddy. But this was to be a solo trip – no one could go and I planned midweek to avoid crazy weekend crowds.[![IMG_7318](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7318-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7318.jpg)  
First stop in the Oakland Hills on the road on Redwood to pee and stopped at side of road what do you think I spy with my little eye? [![IMG_7319](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7319-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7319.jpg)A discarded Pittsburg tools motorcycle tire balance tool. Sweet! Chuck it deeper into woods to retrieve later. That will get good use.  
Side highway to Pleasanton and then get on the ole’ slab. 580 to 132 East, which turned out to be a great guess on my part. Quick stop at Altamount Pass to figure where my exit is.[![IMG_7325](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7325-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7325.jpg)

[![IMG_7323](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7323-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7323.jpg)  
Excellent 132 road after Modesto, just gorgeous scenery, fast sweepers, excellent pavement and no one, NO cars! Score. This area was just like a western movie, so beautiful. Wish I had stopped at a key spot for more pics but kept going.

[![IMG_7332](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7332-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7332.jpg)

[![IMG_7331](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7331-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7331.jpg)  
Stopped at Turlock State Park for lunch (Only meal I brought with me), and find this interesting bridge overpass I just rode over. Only had to share the space with a nice older timer and his old dog and we chatted for a bit. He’s lived nearby for 60 years and did not know where to head for Coulterville – 30 miles away! Had some nice tips on other places to see on another ride.[![IMG_7334](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7334-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7334.jpg)

[![IMG_7347](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7347-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7347.jpg)

[![IMG_7346](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7346-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7346.jpg)  
La Grange:  
Old mining town with some preserved ruins including an old jail. Charming.  
[![IMG_7349](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7349-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7349.jpg)

[![IMG_7357](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7357-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7357.jpg)

[![IMG_7359](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7359-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7359.jpg)

[![IMG_7350](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7350-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7350.jpg)  
[![IMG_7351](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7351-e1411497712164-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7351.jpg)  
[![IMG_7366](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7366-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7366.jpg)

[![IMG_7364](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7364-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7364.jpg)  
132 after this was fantastic. I can’t wait to ride it again. The foothills:  
[![IMG_7368](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7368-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7368.jpg)

[![IMG_7381](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7381-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7381.jpg)

[![IMG_7377](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7377-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7377.jpg)  
[![IMG_7373 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7373-copy-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7373-copy.jpg)

[![IMG_7370](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7370-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7370.jpg)  
132 all the way until it Ts in Coulterville, another historic mining town. It used to have 25 saloons in it’s heyday, but you can still get a coldee at the Hotel Jeffries with it’s original 3 foot thick adobe walls. I didn’t stay long it was quite hot here. Found this cool looking original Chinese grocery out of town center and stopped for some pics. [![IMG_7384](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7384-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7384.jpg)

[![IMG_7403](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7403-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7403.jpg)

[![IMG_7400](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7400-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7400.jpg)

[![IMG_7398 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7398-copy-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7398-copy.jpg)

[![IMG_7391](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7391-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7391.jpg)

[![IMG_7390](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7390-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7390.jpg)

[![IMG_7388](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7388-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7388.jpg)

[![IMG_7385](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7385-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7385.jpg)  
Then you head south of here on route 49 and start the “Little Dragon” which lasts until the summit pass or Bear Valley. Great, beautiful and empty road. Very few cars, a touring bike, a harley and otherwise just breathtaking scenery. The road is “… a chance to see what the country looked like when the miners worked it – tough, demanding, beautiful.” Many mines line this road and it’s part of the “Mother Lode” general area. There was some loose gravel in several turns, but it was loaded with turns and fun fun!  
[![IMG_7420](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7420-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7420.jpg)

[![IMG_7440](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7440-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7440.jpg)

[![IMG_7436](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7436-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7436.jpg)  
[![IMG_7435](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7435-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7435.jpg)Look at these turns!

[![IMG_7425](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7425-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7425.jpg)

[![IMG_7421](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7421-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7421.jpg)  
The peak viewpoint is the high point of the Little Dragon. Gorgeous. And I didnt know it until looking at pics later but this in fact is the very spot Dave1 and Bobl stop to take pictures on in Dave1’s ride report and noted the bridge in background. (I had better weather for pics than they did!)  
[![IMG_7446](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7446-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7446.jpg)

[![IMG_7445](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7445-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7445.jpg)

[![IMG_7453](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7453-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7453.jpg)

Descend down summit. Had done a litle research and knew upcoming Bear Valley and Mt Bullion were semi-ghost towns with ruins from the Gold Rush days. Didn’t find any ruins at Mt Bullion but Bear Valley was great!

[![IMG_7455](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7455-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7455.jpg)

[![IMG_7463](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7463-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7463.jpg)

[![IMG_7481](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7481-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7481.jpg)

[![IMG_7486](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7486-e1411451791489-562x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7486-e1411451791489.jpg)

[![IMG_7475](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7475-e1411451975487-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7475-e1411451975487.jpg)  
I love this: Highway Patrol sign and the Jolly Roger! I wish…  
[![IMG_7478](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7478-e1411500232642-733x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7478-e1411452027454.jpg)

Mt. Bullion, only thing worth photo. LOL! love this sign, anyone watch “Black Adder?” Awesome British Comedy with Rowan Atkinson. “Privet” is slang for ‘loo’ or toilet. He regularly calls Baldrick ‘Privit breath’ in the series… too funny.[![IMG_7490](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7490-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7490.jpg)

Continued on to Mariposa which is cute but touristy so only stopped for 2 pics there and kept moving to Oakhurst.  
[![IMG_7494](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7494-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7494.jpg)  
On 49, I passed the turn off for Chowchilla Mountain Road here to drive around the other side another 30 miles or so of pavement to cut in half the distance of the upcoming dirt mountain road and instead meet up with it through another dirt road out of Fish Camp.  
 At 5pm I stopped for dinner (early) due to the unknown next part coming up: the trip into the mountains on a long barely used dirt road / track over a small pass, I knew I’d need as much light as possible and have to take it very slow to not dump the bike.  “El Cid” mexican restaurant was decent at about mile 220 into the trip in northern part of Oakhurst.  
[![Oakhurst dinner El Cid Mexican](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Oakhurst-dinner-El-Cid-Mexican-e1411500568148-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Oakhurst-dinner-El-Cid-Mexican.jpg)  
Dinner:  
Short ride north on route 41 and into Fish Camp. Nothing there, but a sudden road sign signified this is where I get off.   
[![IMG_7497](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7497-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7497.jpg)Travelled this historic stage coach road through Chowchilla Mountain through the back door gate into Yosemite. This is where all the wagon trains used to go in and out to Yosemite.  Had to drive 9 miles on dirt back woods mountain pass road. Drove about 5.6 miles off of Route 41 on dirt roads that were so narrow a car couldn’t possibly pass and it would be impossible for a car to turn around in, it was that narrow, no million point turns would do it. 

[![IMG_7501 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7501-copy-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7501-copy.jpg) The end of the road.

[![IMG_7505](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7505-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7505.jpg)

[![IMG_7506](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7506-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7506.jpg) Gah! Where the F am I?? No cell service, no GPS satellite, no roads marked! Shit!

[![IMG_7507](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7507-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7507.jpg) Road narrowing, is this spooky or cool…?

[![IMG_7513](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7513-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7513.jpg) I’m a dirty birdie. I felt bad for Green Magic and the dust.

[![IMG_7516](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7516-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7516.jpg) Huge (??) crossing of Chowchilla Mountain Road with “Forest” – (Garmin) but really I guess it’s Summit Road.

[![IMG_7521](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7521-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7521.jpg) 2nd turn option, you go this way young man, East! (Towards Yosemite woods gate).

At last: The Gate – and it’s open and passable!  
[![IMG_7523 copy 2](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7523-copy-2-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7523-copy-2.jpg)

[![IMG_7526](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7526-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7526.jpg) oh, yeah! If the gate is closed a moto can easily sneak around it.

[![IMG_7529](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7529-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7529.jpg)  
I backed into this ditch to pitch camp. It looks worse than it was, I powered right out of it without a wheelspin!

[![IMG_7530](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7530-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7530.jpg)  
Ok, spot my camp!

[![IMG_7532](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7532-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7532.jpg)

[![IMG_7533](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7533-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7533.jpg)

Had to squeeze bike into this thicket and camo it a little with branch and cover reflective plate. “Dispersed” camping is allowed in the Stanislaus forest but not too sure about it all… And I find out later I was in a different national forest altogether just before the National Park Boundary.

[![IMG_7537](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7537-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7537.jpg)

At camp spot the night terrors begin…  
Dark at 8pm and nothing to do so tried to sleep – I was really exhausted anyway.  Lots of strange noises. They start at 9pm and get crazy by 10:30pm really hit a high at 2:30am when a jeep roared by, stopped right by my tent for several minutes, continued, turned at gate and raced back uphill.   
Insane sounds, like a dragon “sneeze/roar” kind of like a “TCH-ROAR!” sound was all i could think of. Super loud and very close by, about 100 feet up the road where I saw an old gated overgrown path that hadn’t been used in years. That was obviously the animal super highway. Swell. (At least I didn’t camp ON that overgrown path – the only damn flat spot in this whole wooded ‘road’). Scariest night camping ever, worrying about bears too all alone miles into woods. I was pretty sure it wasn’t a bear, though not all the other noises I heard. Whatever the hell this thing was it was pissed off, and very active.  
I’ve camped all over – a lot when younger too, let’s see: NH, VT, ME, MA, UT, CA, NV, France, Spain, Norway, and I ain’t NEVER heard anything so f\*&ed up as this. Wait, I did hear this in Norway… I recall this almost instantly. Scared the living hell out of my girlfriend and I at the time. We both imagined it was a roaring dragon doing flybys on our tent in the middle of nowhere. Nerve wracking. Don’t believe me: well take a listen here then. I searched online when back home and found the exact noise. Now crank it up to an ’11’ outside your tent alone.  
CLICK here – I found this same sound I heard all night on the internet: [Awful night terror noise](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Vixen-Scream-Fox.m4a)  
[Mp3 version here](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Vixen-Scream-Fox.mp3)

 I woke up to a a cawing crow at 6:30am and thought “Sweet!” I slept later than I thought I could. Packed up my dusty bike with all my gear, set up the GoPro and had to stash the tank bag to do that.  
Drove through the gate and entered Yosemite National Park for the very first time.  
[![Yosemite entrance sign GoPro grab](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Yosemite-entrance-sign-GoPro-grab-500x281.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Yosemite-entrance-sign-GoPro-grab.jpg)  
This is also an unmanned gate – there’s no one else anywhere nearby – and a ‘free’ ‘backdoor’ entrance to Yosemite if you want to roll that way.  
[![IMG_7538 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7538-copy1-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7538-copy1.jpg)  
Followed dirt track / road for about 3.3 miles and came in the sneaky way right through the Wawona Golf Course!  
[![Wawona Golf Course backdoor Yosemite GopPro grab](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Wawona-Golf-Course-backdoor-Yosemite-GopPro-grab-500x281.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/Wawona-Golf-Course-backdoor-Yosemite-GopPro-grab.jpg)  
First stop: Wawona General store for some breakfast: Black Cherry Almond Clif bar, banana and some trail mix for later.  
[![wawona general store gopro grab](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/wawona-general-store-gopro-grab-500x281.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/wawona-general-store-gopro-grab.png)  
Then zipped off to Glacier Point and found the most stunning view 3000 feet above the below valley. It’s so high up, you look over the edge and your stomach goes into your throat reflexively. Just gorgeous.  
[![IMG_7548 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7548-copy-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7548-copy.jpg)

[![IMG_7563](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7563-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7563.jpg)

This was 3000 feet above the valley floor view:  
[![IMG_7571](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7571-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7571.jpg)

And the drive up there is loaded with fast sweeping in superb shape except for one short length about 2/3 of the way up the 17 mile road.  First lookout I drove into with GoPro there was a deer in the parking lot and not too many people as it was still quite early, happily.  
 [![IMG_7578](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7578-e1411451808727-562x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7578-e1411451808727.jpg)  
[![IMG_7590](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7590-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7590.jpg)After returning down Glacier Point Road I back tracked to Wawona General Store and got a tuna sandwich as I was pretty damn hungry at 11:30am. Then off south on 41 to Mariposa Grove and some of the oldest larger giant sequoia trees.  
[![IMG_7596](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7596-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7596.jpg)  
They have one the “Grizzly Giant” tree that is estimated 2000 years old! It’s a beautiful wide and crazy looking tree. Circumference at the base is 94 feet, diameter 30 feet, 210 feet tall.  
[![IMG_7606](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7606-e1411451834843-562x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7606-e1411451834843.jpg)Also saw the “Tunnel Tree” and hiked about 2.6 miles there.

Put back on all the gear and then headed to the actual Yosemite Valley itself. Back same route 41 North. Then I was man behaving badly. I was breaking speed limits with abandon, (uh, several DY lines for cages), and looped that particular route 41 hairpin turn about 7 times at high revving speeds. I find out later that Park rangers are everywhere ticket everyone left and right for 4-5 mph over the 35mph park limit. Dodged that bullet. Have to be calmer next time I guess.  
[![IMG_7630](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7630-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7630.jpg)  
Finally approaching the world famous valley, it’s about 3pm. A newer forest fire caused a lot of smoky haze in the valley unfortunately. The famous “Tunnel View” vista point was a little underwhelming due to this smoky haze. [![IMG_7636](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7636-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7636.jpg)

[![IMG_7637](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7637-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7637.jpg)  
And indeed coming over the pass into the area you could smell remnants of burnt things.  I had hit a wall on this 2nd day at 2pm and felt over the whole trip, just thoroughly exhausted. My left arm was very sore. But the ride into the Valley Floor and especially the “Mist Trail” to Vernal Falls really perked me up. Very steep hike especially that last bit to get to the very top of Vernal Falls. Bathed in sweat and heart pumping big time up that face. The final ascent has a crazy iron fenced staircase that looks straigh out of Indiana Jones and the Temple of Doom.  
[![IMG_7718](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7718-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7718.jpg)  
[![IMG_7672](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7672-e1411499554228-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7672.jpg)[![IMG_7701](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7701-e1411451235477-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7701-e1411451235477.jpg)  
[![IMG_7735](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7735-e1411451924850-562x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7735-e1411451924850.jpg) Crazy final staircase.  
[![IMG_7661](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7661-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7661.jpg)

I had the top of the falls all to myself at 5pm. Hike back down to the Valley and walk to bike and gear up and head out. I installed my tank bag back (had been cramming it in my saddle bags to use the gas tank GoPro mount in the park, and this turned out to be a mistake. The ride out was incredibly epic, much better than the way in, and the view simply jaw dropping.

[![IMG_7745](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7745-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7745.jpg)

[![IMG_7769](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7769-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7769.jpg)

[![IMG_7766](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7766-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7766.jpg)

[![IMG_7776](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7776-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7776.jpg)  
A nice German guy insisted taking my photo with my bike here!

Indeed, past El Portal and out of the park the gorgeousness just wouldn’t stop: route 140 follows this continuing canyon for miles and miles and the pretty Merced river right next to the road for 30 miles! Did a little hand held GoPro here.  
I stopped just before the El Portal gate at 7pm and unfortunately the Rangers were still installed at the gate booth and stopping all traffic. Not good, my back door mountain entrance meant I had no pass. I stretched out, did a little dirt rermoval off chain, wasted more time, and finally 7:15 figured I need to get the F outta here, I have a 4 hour ride ahead and I am beat tired. I hit the gate and with a big grin the Ranger waves me through and says “Have a great evening!” Wow, I say the same back and head down this wonderous road grinning. I lost the light shortly after and I am very eager to go back this way some morning soon in broad daylight and soak it all in. And pay for a proper pass in, I find out later motorcycles are half price $10 fee (!)  
The bike broke down in livermore / pleasanton border final night at 11:30pm, about 35 miles from home.  
The clutch broke. Taking exit for final rest and stretch, the clutch lever fell right into the handlebar and no clutch. I had to jam shift transmission into 3rd gear, slow down and stop.  I thought my clutch cable snapped. But something else is wrong in clutch itself. The lever arm tube fell out the bottom of the bike and was hanging. I twiddled it around a lot, got it slotted and jammed it up into clutch by hand. I figured I had a clutch pull or two before it was done.  Went in 3rd gear back to highway, pulled in clutch – it worked – and shifter from 3rd to 6th and drove 32 highway miles that way to oakland. Pulled in clutch once more at exit stop sign to go straight to 2nd gear at red light. Then 3rd clutch pull to start and slowly cruised to her apartment and stored bike. Will have to get going on it to fix it. My girlfriend had to pick me up there last night so we could go to Berkeley.  
This was my first trip with a GPS running and it was great and very handy.  
Yosemite was amazing. The whole trip was. Epic pics. Including spot where presidents stand 3000 feet above valley floor looking down on half dome.  
——  
Summary:  
I drove about 237 miles or so the first day to my camping stop off Chowchilla Mtn Road in the woods. Second day between all the park driving and driving back to Oakland I drove about 322 miles, a new day record with Green Magic.  5 mile climb to top of Vernal Falls and back, 2 miles hike in Mariposa Grove.

**Terrors in the night:**  The next morning safe and secure in Berkeley I finally discovered what that roaring was, the night terrors that plagued me in Yosemite and also in the summer of 1996 in Norway camping. We were scared shitless then and I was uber creeped out alone. The power of the internet… I figured I would look it up and was trying to figure out which animals Yosemite and rural Norway have in common. Some crazy sort of elk cry??   
Nope, it’s the “Vixen – Cry” and / or “alarm” cry from none other than the little cuddly (horny or pissed off) red fox. Aww, wish I had known and coulda slept. You would never in a million years think a small animal like that, and even a fox could possibly make such a noise. I’ve heard coyotes howling lots and lots, and know their excited hunting noises, calls, and alarm noises. But this is completely on a different level altogether.