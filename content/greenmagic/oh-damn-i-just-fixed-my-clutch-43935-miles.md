---
title: 'Oh damn! I just fixed my clutch 43,935 miles'
date: Tue, 04 Jun 2013 15:21:00 +0000
author: rj
draft: false
tags: ['repair']
---

Yup, i finally ‘loosened’ clutch cable adjustment instead of where it was when I bought it, or let tightening it which I tried before and was an utter failure. I can rev past 6,000 rpms now!  Hit 8K + doing loop around Lake Merritt just now, not planning to hit 14K anytime soon, especially as I discovered no coolant in the thermostat / cap area. But great development! Thought I was going to have to replace the clutch next.