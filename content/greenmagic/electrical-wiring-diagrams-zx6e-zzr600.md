---
title: 'Electrical wiring diagrams ZX6E ZZR600'
date: Wed, 30 Jun 2010 14:39:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/28.jpg
tags: ['Uncategorized']
---

Here’s some of the wiring diagrams for reference on the road.

[![28](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/28.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/28.jpg)

![05](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/05-573x750.jpg)

![06](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/06-573x750.jpg)  
![15](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/15.jpg)

![14](https://rjfilms.com/greenmagic/wp-content/uploads/2016/06/14-574x750.jpg)