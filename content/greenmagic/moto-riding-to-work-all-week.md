---
title: 'Moto riding to work all week.'
date: Tue, 25 Mar 2014 14:48:00 +0000
author: rj
draft: false
---

Riding to work all week. Had nice unexpected weather break today. Morning partly sunny.  
Parked Green Magic in parking garage and it poured midday. Should be clear for ride home.  
Will make at least one run to the ocean after work or Marin or something.  
Got back to Green Magic last night after working in MA and cape construction.