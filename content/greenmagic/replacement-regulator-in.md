---
title: 'Replacement regulator in!'
date: Thu, 02 May 2013 13:24:00 +0000
author: rj
draft: false
tags: ['repair']
---

At 43697 miles I think charging issue solved. I popped in a used 2003 Kawasaki ZR7S regulator with 13,382 miles on it, for $39 with free shipping.

**It’s the same unit a SH650A-12 model.**

Drove it about 26 miles yesterday after installation and my dash lights and particularly blinkers in dash have never been so bright! Also my horn is about 2x louder than it’s ever been, so I am hoping this solved the problem!