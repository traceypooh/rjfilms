---
title: 'First naked step yesterday. FEAR PT PAIN. Beatings'
date: Fri, 07 Jul 2017 08:08:00 +0000
author: rj
draft: false
---

I took my very first step yesterday with nothing. Nothing on either leg. I was pretty astonished.  
And also did not want to at all. I had only hours earlier been cleared from ‘the boot’ aka aircast.  
So here I am in Physical Therapy. Told to stand. Told to walk.  
AND THEN  
told I will be balancing on one leg for 30 seconds. My therapist-in-training has never made a joke with me. I assume she’s finally now. So I say “on my good leg, right?” And she says, “Sure”, then the other.

No way. No freaking way. I have been a champion ankle / single leg balancer since my prior ankle break 23 years ago. I can do the flamingo with my eyes shut for at least 30 seconds. I can balance without movement eyes open.  
But not today.  
Apparently the such limited walking etc… has weakened my good leg, even though I demand it to do everything for me for the past 6 weeks. I bruise that one too, putting all my weight on it constantly and hopping, twisting and carrying weight all on that leg.

Then after a slightly wobbly open eyed good leg 30 second ‘flamingo’ pose I demanded to do the bad leg.  
So… after resisting and say no way I can even do 3 seconds… I try and try but my good leg will not leave the ground – toe firmly attached to terra firma. My body won’t do it. Every part of my brain and body says DONTDOIT. NO.  
I am even holding onto the table next to me. Finally, I am able to raise the good leg off the ground. For a few seconds. A few wobbly seconds. Everything feels completely wrong. My bad leg is frozen like a vice left outdoors 5 years on a New England salty road.  
It’s turning all kinds of colors in urgent screaming protest. Red with white speckled blotches. The pain ratchets up fast. This is completely impossible to do, my brain knows this.

PT is about fear. And going so far past the personal fear barrier deep into your worst fears. Doing things you know absolutely you should not be doing, and cannot do. You are sure you cannot and should not do a simple thing. You would never ever do it on your own or try it – you wouldn’t even think about it in your head because it hurts your broken body so much to even put that glimmer of thought in your head. “Oh, ok, do two sets of 15 of those then.” It’s that.

My Grandmother told me something like this often: “Your body is your own expert. You know what you should and can do”. (Another piece of excellent advice: When you are sick / can’t eat, “If you can picture yourself eating an item, then you can.”)  
This completely throws your Grandmother’s knowledge and wisdom out the window and stomps all over it.  
The ‘therapy’ I ‘endured’ yesterday was akin to a Nancy Kerrigan beating.  
My leg completely swelled up and all around the ankle. Almost as bad as the original break. Much worse than flaunting Doctor’s and Therapist’s orders doing “Ultimate Adventure” hell for 8 days and rock climbing and sand crawling and gravel scrambling in 115 F heat in Arizona and Utah.