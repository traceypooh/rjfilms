---
title: 'Tilden, 3 bears, pigfarm hill w Bailey & Harley'
date: Wed, 11 Sep 2013 19:45:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/Bailey-Russ-moto-sept-11-2013-inspiration-ptPS-300x219.jpg
---

Friend in from east coast again, and Hunter kindly volunteered his Harley and gear so we could go ride after work. Wednesday mid week and we’re both pretty tired out from work week etc… but fun of course nonetheless. A squirrel almost commits suicide under my front wheel as I am highest speed of day (80) down back of isolated 1st ‘bear’ hill.

[![Bailey  Russ moto sept 11 2013 inspiration ptPS](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/Bailey-Russ-moto-sept-11-2013-inspiration-ptPS-300x219.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/09/Bailey-Russ-moto-sept-11-2013-inspiration-ptPS.jpg)

Inspiration Point lookout