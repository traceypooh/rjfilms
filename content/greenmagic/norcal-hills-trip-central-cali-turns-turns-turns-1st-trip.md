---
title: 'Norcal hills trip (central cali), turns turns turns – 1st trip!'
date: Sun, 26 Aug 2012 20:38:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/04/IMG_8620-225x300.jpg
tags: ['Uncategorized']
---

I wonder if a bunch of people here have done this trip. Redwood / Pinehurst. Would be certain many CA ZXers know to head there.

Oakland start.  
Snake to Skyline Blvd, keep going all the way back down to 580 via Grass Valley Rd and Golf Links Rd. Parallel the 580 south towards Estudillo towards Lake Chabot.  
Wrap around Lake Chabot, and we drove thru Castro Valley into Hayward and stopped for some eats at Buffalo Bills.  
Back on bike, backtracked into Castro Valley. Seven Hills Road to Redwood and then right onto Pinehurst on insane turns over and over over the pass down into Moraga / Canyon area, at T straight on thru Canyon following Pinehurst thru Redwood forest to some crazy curves back up to Skyline Blvd.  
Gorgeous views.

[![IMG_8620](https://rjfilms.com/greenmagic/wp-content/uploads/2013/04/IMG_8620-225x300.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8620.jpg) ![Route](https://rjfilms.com/greenmagic/wp-content/uploads/2013/04/Route-281x300.jpg "route - check out those corners if you zoomed in. ")![IMG_8630](https://rjfilms.com/greenmagic/wp-content/uploads/2013/04/IMG_8630-225x300.jpg)[![IMG_8642](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8642-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/04/IMG_8642.jpg) ![IMG_8636](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8636-300x225.jpg)[![IMG_8641](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8641-225x300.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8641.jpg) [![IMG_8637](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8637-300x225.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8637.jpg)[![IMG_8634](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8634-300x225.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/04/IMG_8634.jpg)