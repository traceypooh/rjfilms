---
title: 'Damn, man, tired of tires.'
date: Sat, 06 Feb 2016 07:25:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3443-427x320.jpg
---

Soo… two sets of Dunlop Q3 tires for Green Magic.  
Love the grip. Love it. Blows away the Battleaxe BT016 or 023 or whatever came on the bike when I bought it.  
In fact, I hit a turn I do at 50mph all the time at 35mph on a cold day and for no reason lost the front end. Bike was almost flat horizontal in the turn, I was going down and crashing, and SOMEHOW the front tire held it’s grip and I made it out crash free. In fact, I haven’t related this story anywhere yet, and I’m going to post up what the tire looked like in the slide. It was crazy. (I examined the turn at great length. No ice, no water, no oil, no reason to have a slow barely leaning bike wash out on the front end. Only thing was there were a few lined up ridges in the corner – perhaps it induced a mini speed wobble – although the handle bars didn’t shake, the bike just nearly went down to pavement suddenly.) Can’t explain but it was crazy.

![IMG_3443](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3443-427x320.jpg)

![IMG_3444](https://rjfilms.com/greenmagic/wp-content/uploads/2016/02/IMG_3444-1000x750.jpg)

What’s also crazy or driving me crazy is the service length of these tires. Both sets are completely shot at 8000 miles. I’m talking steel showing on the rear tire shot.  
I only got 8-9 months on this last set. This is not my scene. I can’t live like this – at least not changing them myself with my little ghetto tool set. ( I have to go to a lending tool library to get huge wooden C Clamps to break the beads, and my wheels are a complete bitch to set the bead on – due to age / wear I guess. They always hold back setting until 60-70 psi or more which creeps me out).