---
title: 'Oil change day w Tracey'
date: Sat, 16 Feb 2013 22:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-includes/images/smilies/simple-smile.png
---

First oil change for Green Magic. Drove up in am. Whole day there. Parts run for both our bikes. I change my mind and scramble to get Wix filter and the hell with the Fram I had. Finished both bikes though! C came up and made us all a great burger dinner ![:)](https://rjfilms.com/greenmagic/wp-includes/images/smilies/simple-smile.png)  

[![changeoilgarage](https://rjfilms.com/greenmagic/wp-content/uploads/2013/12/changeoilgarage-150x150.jpg)](https://rjfilms.com//blog/greenmagic/oil-change-day-w-tracey/changeoilgarage/)

[![changeoil2](https://rjfilms.com/greenmagic/wp-content/uploads/2013/12/changeoil2-150x150.jpg)](https://rjfilms.com//blog/greenmagic/oil-change-day-w-tracey/changeoil2/)

[![changeoilphoto](https://rjfilms.com/greenmagic/wp-content/uploads/2013/12/changeoilphoto-150x150.jpg)](https://rjfilms.com//blog/greenmagic/oil-change-day-w-tracey/changeoilphoto/)