---
title: 'GoPro mount fabricated from DIY luggage rack'
date: Sun, 27 Jul 2014 19:45:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GMM-goPro-mount-DIY-luggage-rack-cu.jpg
---

Finally got my GoPro working. Rigged up a quick little mount off my hand built rear luggage rack. I put a longer bolt in the rear mount, and flipped it over, used one nut as a base, and a 2nd nut to clamp down a small 90 degree corner bracket. Works really well, a lot better than I thought. The viewing angle is pretty cool overall, and the rigid mount keeps the video from being annoying bouncy and no stupid rolling jello shutter errors like I saw before with a crappy suction mount.

Also of note! I finally got a new(er) laptop that can actually playback all my HD 720 60P and 1080P GoPro and new Nikon D5200 video footage. I am psyched![![GMM goPro mount DIY luggage rack cu](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GMM-goPro-mount-DIY-luggage-rack-cu.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GMM-goPro-mount-DIY-luggage-rack-cu.jpg)

[![GMM goPro mount DIY luggage rack](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GMM-goPro-mount-DIY-luggage-rack.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GMM-goPro-mount-DIY-luggage-rack.jpg)