---
title: 'Got GM running again after huge problems starting'
date: Thu, 18 May 2017 18:48:00 +0000
author: rj
draft: false
tags: ['repair']
---

My friend came by with with his truck to tow Green Magic north to his home workspace, confident we’d get it running whereas I was not.  
Turns out, he figured out no fuel was going thru the system at all. Well, I did know and show him fuel would easily gush out of the petcock at ‘on’ or ‘reserve’ setting.  
But the fuel would not enter the system. Some sort of like reverse vacuum.  
Taking off the tank, I find the fuel pump dry, the lines to the pump empty and the fuel line to the carbs empty.  
Had just before drained the carbs of ‘bad fuel’ or ‘water in the fuel’ idea, but there was hardly any fuel to drain in the carbs – not normal!

We manually filled the hose to the fuel pump with fuel. Then hit the starter, my friend put his thumb over the end of the fuel line to the pump and could (now) feel suction.  
Re- hooked it back up and GM fired up near instantly!

So we know the issue was, and how to fix it. But the cause… that’s unknown for sure right now.  
I know there are some minor fuel leaks, where hose clamps are missing and around the fuel pump was wet with fuel.  
Maybe given enough time sitting, 3+ weeks will do, 5 months is so much worse, the fuel gradually gravity drains down to the pump area where it very very slowly leaks / drips out. and mostly evaporates.  
Then the system has trouble priming with fuel again when empty. Clearly this last part is true.  
Why? Maybe the pump is weak / malfunctioning?  
I need to look all these possibilities up (about the pump function and failures).