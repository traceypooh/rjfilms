---
title: 'Yo! We got that B Roll!, Or how to get paid $800 to ride your E'
date: Fri, 14 Feb 2014 13:57:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image2-300x221.jpg
---

It was the best of times it was (almost) the worst of times…. Read on reader.

As the title suggests I did in fact get paid handsomely to ride my E 100 miles yesterday in lovely 62 weather. Weather so warm and with my activities I was sweating most of the day after taking the liner out of jacket, and down to T shirt, nylon synth Long sleeve and Frank Thomas unlined jacket. So amazing right? Couldn’t be better?

Does playing chicken with a BMW in his lane sound like a good idea?

So what is this B Roll you ask? If you don’t know that phrase allow me to introduce you to this guy:  
(I did not shoot this video in the link, I just love the video).

Link:  
http://m.youtube.com/watch?v=SItFvB0Upb8

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image2-300x221.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image2.jpg)

——————–  
G.M. Riding again!

So for start of trip I had to watch weather closely for a week to plan trip into SF. It had to be sunny or mostly sunny to match footage we shot earlier for a TV episode. I took my chance yesterday with first 50/50 cloud sun mix predicted in over a week.  
I also had to stuff a camera that costs 7X what i paid for Green Magic in a used set of TourMaster saddlebags that I had never figured out to mount right. And a bunch of other crap like extra batteries, charger, new gopro that refused to work later for fun ride, water, layers etc in other bag and full tripod. Ad my phone toasted itself a week ago so no phone. I’m used to riding always with one now just in case.

I zipped out of Oakland in fog and clouds over super foggy bridge and into sunshine and the Golden Gate park!  
Then on to Ocean Beach to get, you guessed it, B Roll shots of beach and views up by the Cliff House.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image3-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image3.jpg)

A closer shot of bags set up. Apparently I mount them backwards, but they fit better that way. If I try to mount under seat they will almost certainly hit the pipes. And melt?? So I put em backwards and slide them forward to rest on the passenger footpegs. I have camera squashed into this bag.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image4-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image4.jpg)

So the good:  
Beautiful weather – escaping east coast winter  
Paid riding time, bike working  
Pretty areas.  
Shooting solo, self directed, no silly know nothing producer with me telling me what to do. (Which can happen from time to time on shoots).  
Route 1 afterwards for fun to Stinson beach, currently my favorite twisties anywhere. They are sick.  
Got all the shots requested and they came out great.

Not so great:  
Stressful with that much valuable gear with me in new untested gear. Hwy speeds, heavy cornering etc  
Had to constantly get off bike, pull cam and strapped tripod set up shoot and put it all back together and repeat over and over. Not really a joyride. Shooting often with helmet on so didn’t have to pop off seat and jam helmet behind bag to lock it. I’m sure I looked sensible on the beach wearing a helmet getting B Roll of surfing.  
$300 goes to camera and gear.  
The worst fog I ever hit riding Golden Gate Bridge at rush hour. You could see nothing. Water and fogged up glasses and shield. Nearly riding blind in stop and go traffic over bridge.  
Rush hour Bay Area return. It’s f ing awful. At least it’s legal to split lanes if not stressful.  
Slow time of year. Only booked 2 days in last 3 weeks.

And about chicken…

I did route 1 to the Pelican Inn and back 2 full loops. Returning to go home after cranking turns I hit a corner going at quick but not fast fast pace. Said corner had water and dirt all over it. I immediately locked up both wheels and went into skid. I quickly got off both both brakes, stood the bike up straight, crossed the double line completely into oncoming lane for maybe 150 feet. While I had right foot down to hold bike up and try to fully recover before I dared ease the bike back into my lane. I could see a car had been approaching and was about to come around huge corner.  
The BMW had to slow down after his blind corner and seeing me completely in his lane. Needless to say he look surprised. I more or less had to force him to brake (fortunately he was going slow anyway) and out of his lane or both. Thanks guy, a cager who wasn’t a dick.  
It was a pretty scary event. If I had kept on brakes or tried to force bike into my lane I definitely would have downed bike and either slid into car or off the road and crossed in front of him into hill banking. I do know I really shouldn’t have been on brakes at all into a turn. Kind of lazy probably stupid end of long day not 100% focused riding in slower easier turn area. Dumb.

Below: Pic of turns and fog bank that chased me out of Stinson and back up the ridge on route 1. The whole fog bank kept me out of final approach into Stinson beach with horrible visibility and followed me back up the mtn ridge off the turns a little earlier than I wanted.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image1-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/03/image1.jpg)