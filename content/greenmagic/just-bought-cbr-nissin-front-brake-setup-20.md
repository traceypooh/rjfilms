---
title: 'Just bought CBR Nissin front brake setup! $20!!'
date: Wed, 22 Apr 2015 19:30:00 +0000
author: rj
draft: false
tags: ['DIY', 'Uncategorized']
---

After work today rode north to the city and the sunset district. I rode up and down the great highway at ocean beach a few times, bought the parts and stashed them in my tank bag, and then rode up to the cliff house and the park overlook, and then back down to the small beach south of Noriega and rode through the fine sand all over the road there to get to beach parking with my completely shot tires. That was interesting. It was really nice to cruise around the beach and oceanside after work.

Found a really superb deal on a 2004 CBR F4i Nissin complete front brake setup.  
I got the master cylinder, lines, calipers with pads with lots of life left, and the brake lever too! Guy wanted $30 for the set and then charged me $20 when I got there as I just had twenties on me, and he said he had been trying to get rid of them for a long time. (He bought that off another guy just for the steel lines and he threw all this in the deal).  
Stoked! I keep reading especially on zzr-international.co.uk site and a little on the zx6e.net site about how amazing of an upgrade this is to the stock Kwawk.  
I have to adapt from the Nissan’s 8mm mount holes to the Kwawk’s 10mm bolts. I’m planning to tap out the calipers to an M10 pitch 1.25 using a 11/32 drill bit first and the M10 tap after. I think this is the better solution.  
My best friend is flying in from the east monday and bringing with him his M10 tap. I will probably borrow a drill press locally from the local tool lending library here.

Have a busy number of days ahead with Green Magic:  
– New Tires priority #1!  
– New brakes while wheels off and front brakes unbolted for tire swap.  
– Going to remove stock rear shock and finally at least size up the Fox Twin Clicker. I need to machine a number of parts, but this will help me gauge what I need compared to the stock dimensions on the upper bushings etc…  
– Water Pump looks like it’s failing or failed. This past week I noticed spewing coolant under the pump on Tuesday, and Wednesday, and Thursday i noticed oil coming out the weep hole I think on the pump.  
– some fairing white sticker work to cover previous owner low side damage.  
– re-painting green the bar ends after sanding off the new surface rust  
these jobs will also be my first brake fluid change on the front end since buying the bike, and also be my second coolant replacement, and that’s due anyway.