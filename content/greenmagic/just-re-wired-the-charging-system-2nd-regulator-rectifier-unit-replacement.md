---
title: 'Just re-wired the charging system. 2nd Regulator / rectifier unit replacement'
date: Mon, 09 May 2016 20:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5027-1000x750.jpg
tags: ['Kawasaki', 'Ninja', 'regulator', 'repair', 'Uncategorized', 'ZX-6', 'Zx6e', 'zzr', 'zzr600']
---

I’ve had charging issues with Green Magic. Recall that was the first major problem of this bike, it would eat batteries.  
I’ve cut out crispy wires and trimmed back charred wires that hook into the regulator / rectifier unit now twice. Both times I got some good life out of the charging unit – the first time I swapped in a low mileage ZR7S R/R unit.  
The second repair job last year I cut back the wires again, but I knew it was only a matter of time…

The final day I was out west again, the bike was not charging riding and about to die soon I could easily tell. I throttled the bike at high rpms back home. And parked it and drove the van that day and flew east the next day.

6 weeks later I got back to the bike and took of the rear left panel, expecting to see fried wires at the R/R again. Which I did find. BUT, this time I also found a melted stator to regulator wires connector. That’s new and bad.

So I rounded up some new wire connectors and cut up some brand new THHN stranded 10 gauge wire and ran them from the stator connection all the way to the R/R and put new connectors into the plastic 6 pin clip there as well.  
I cut back the 3 yellow stator wires a little bit and put on new connectors there and connected to the new 10 gauge wires.

I also installed a low miles ZR7S R/R unit, the same Shindegen Shunt unit from a different Kawasaki. I found it on fleabay for $13 shipped and couldn’t say no to that!

Result? Green Magic fired up (Although grumpily after sitting for 6 weeks with a weak battery. I had to push it up the hill to the crest and then bump start it down the hill after 3 sessions of trying to battery start.)  
It ran rough for several minutes, but when it warmed up it ran strong and great!

A few days later I took it on a 80 mile run and it was great. I hope this solves the charging issues for a long time!  
[![IMG_5027](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5027-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5027.jpg)

[![IMG_5031](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5031-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5031.jpg)