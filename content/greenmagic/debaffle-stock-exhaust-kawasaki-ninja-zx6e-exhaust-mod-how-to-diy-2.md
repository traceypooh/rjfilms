---
title: 'Debaffle stock exhaust Kawasaki Ninja ZX6e. Exhaust Mod. How to. DIY'
date: Sun, 08 Nov 2015 17:35:30 +0000
author: rj
feature_image: https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-drill-rivets-e1448322987611-400x272.png
tags: ['baffle', 'chop', 'debaffle', 'debaffle cans', 'debaffled', 'DIY', 'exhaust', 'exhaust mod', 'Kawasaki', 'muffler', 'Ninja', 'repair', 'stock exhaust', 'Zx6e']
---

This is a modification to the stock dual can muffler exhaust on the Ninja ZX6E 1993 – 2004 also known as the ZZR00 in 2004 and in Europe.  
This was all inspired by “mechanicalmadness” member of the old zx6e.com forum. He did this exhaust mod and made a great write up although it’s gone from the internet now.

First thing, you have to drill out the exhaust cap rivets, there are three per side.[![stock Exhaust Mod zx6e debaffle drill rivets](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-drill-rivets-e1448322987611-400x272.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-drill-rivets-e1448322987611.png)

Next, you need to spin the exhaust caps out. I used a strap wrench. There is some sealant in there that does not let the cap release easily. Mine were easy to loosen though, perhaps due to their age.  
[![stock Exhaust Mod zx6e debaffle strap wrench2](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-strap-wrench2-e1448323033395-400x288.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-strap-wrench2-e1448323033395.png)

[![stock Exhaust Mod zx6e debaffle strap wrench](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-strap-wrench-e1448323013380-400x292.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-strap-wrench-e1448323013380.png)  
Removing the end caps reveals this:  
[![stock Exhaust Mod zx6e debaffle tailpipe left](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-tailpipe-left-e1448322967323-439x320.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-tailpipe-left-e1448322967323.png)  
This is the final tail pipe that comes from 1 of 4 pipes inside as you are about to see.

Then, I used a 1 + 3/4″ (44mm) hole saw for metal to make it fairly easy.  
[![IMG_3731](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3731-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3731.jpg)  
To get the holesaw to bite into the base of the end of the exhaust I cut the tail pipe off with a hacksaw first – maybe 1.5″ off or so.  
[![IMG_3729](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3729-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3729.jpg)

[![IMG_3728](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3728-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3728.jpg)

[![IMG_3725](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3725-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3725.jpg)  
Then the hole saw will easily reach the metal to cut.  
[![IMG_3733](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3733-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3733.jpg)

[![IMG_3735](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3735-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3735.jpg)

[![IMG_3734](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3734-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3734.jpg)  
Once that’s done, you now need to break the inside weld spots on this tailpipe / baffle. I jammed a wooden dowel in there, and used lockjaw pliers to put a lot of force on it, mostly rotating. You might find this step easier with a larger hole saw hole, but I didn’t want to go too big here.  
[![stock Exhaust Mod zx6e debaffle breaking weld](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-breaking-weld-400x225.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/stock-Exhaust-Mod-zx6e-debaffle-breaking-weld.png)  
It took some time but eventually you fatigue the metal and snap it off and out of there.

You can now see the insides of the muffler. You are looking at the plate of the middle chamber.

[![IMG_3752](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37521-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37521.jpg)

[![IMG_3753](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37531-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37531.jpg)

[![IMG_3744](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3744-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3744.jpg)

[![IMG_3754](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37541-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_37541.jpg)

[![IMG_0507](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0507-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0507.jpg)

[![IMG_3750](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3750-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3750.jpg)

[![IMG_0509](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0509-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0509.jpg)

[![IMG_0501](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0501-400x300.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0501.jpg)

These exhausts have 3 separated chambers. What this mod does is effectively eliminate the middle chamber AND cuts out a large back and forth pattern the exhaust gases used to have to follow backwards into the middle chamber and then out that small final tail pipe / baffle. With it gone, the gases enter the first chamber and then exit out the larger diameter pipe, bypass the middle chamber altogether now, and then exit into the rear chamber and have free outflow out the back now.  
Before:  
[![IMG_0513](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0513-e1448322906595-500x306.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0513-e1448322906595.jpg)  
After:  
[![IMG_0514](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0514-e1448322940468-500x284.jpg)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0514-e1448322940468.jpg)  
Here is a photo of an ‘exploded’ stock muffler:  
Yo[![zx6e exhaust ripped apart](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/zx6e-exhaust-ripped-apart-475x320.png)](https://www.rjfilms.com/greenmagic/wp-content/uploads/2015/11/zx6e-exhaust-ripped-apart.png)

Result?:  
It sounds SO much better than stock. Idle is amazing with a nice throaty burble. Cutting this all out rather than drill holes around the end tail pipe makes it deeper / bass-y sounding, and not raspy at all.  
Depending on load and throttle position etc… it can get a little loud and drone starting as early as 3500 and through 5000 rpms. Again, depends on several factors. By 6000 + the exhaust sounds sweet and great!  
My bike idle smoother at lower rpms with this as well. It burbles nicely, it really sounds fantastic.  
This little mod has totally changed the feel of the bike. It was such a quiet bland exhaust before. Some people have done this and found it too loud for them.  
I have worn earplugs riding for the past 2-3 years to protect against hearing loss (from wind noise), and I LOVE this exhaust with my ear plugs in. I am also noticed more by cars driving around as they have more of an audio warning I am nearby.  
I will posting a proper comparison of the before and after video with same video gear with the same manual settings so you can hear the difference.

It’s a free exhaust tone modification and I did it in just a few hours on the weekend. Enjoy!