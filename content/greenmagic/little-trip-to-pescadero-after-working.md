---
title: 'Little trip to Pescadero after working'
date: Fri, 27 Feb 2015 18:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1634-400x300.jpg
tags: ['Uncategorized']
---

Friday, I get to zip out the door at 2:20pm. So off to the coast I go.  
I had a blast on Hwy 84. Oddly, my favorite part was a little down from Alice’s / 4 corners towards the bay / San Mateo side believe it or not. Downhill or uphill I was just RAILING some of those turns. I probably got pretty close to grinding my knee on some of the extreme left handers downhill there.

I rode the typical 92 to Half Moon Bay, I would love to live life there.  
I cruised out towards Mavericks break ‘cuz I almost thought I could see some breakers coming up Rt 1 and saw this locked up in a industrial area which I thought was quite cool (I am a surfer). Gotta be Jeff Clark’s old rig and old advertising vehicle for his Mavericks Surf Shop.

[![IMG_1634](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1634-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1634.jpg)

The coast was very peaceful to my soul as it always is. It was quite windy today, especially when I looped back north from Pescadero back to 84. I would not want to ride without ear plugs it was so extreme today.  
[![IMG_1637](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1637-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1637.jpg)

This was a gorgeous view off Rt 1 on “Stage Road” short cutoff to the San Gregorio general store on 84.  
[![IMG_1642](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1642-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1642.jpg)

I love this tall wall of greenery on the right, felt like I was back in Ireland!  
[![IMG_1639](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1639-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1639.jpg)

As I was heading up 84 back towards home base, it started to rain a little bit. I was kind of pissed b/c wunderground rated it as a 0% chance of rain until late late night. Grr… but i rode fast and rode ahead of the rain. Phew. It did slow me a bit in some of the corners heading up to Skyline and 4 corners, but that road is such a joy. And after a hard 4 day week it was definitely the right call to get out to the coast and feel the surrounding ocean wash over my being. Very balancing for me, I’m a water sign Scorpio in need!

[![IMG_1650](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1650-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1650.jpg)  
The very pretty lookout at Pescadero. Farthest point I rode south today, my usual stop.

[![IMG_1651](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1651-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1651.jpg)  
San Gregario beach just next to the 84 and Route 1 intersection looking back at Rt 1.

[![IMG_1652](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1652-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_1652.jpg)  
Riding back up 84 towards Alice’s, there’s very cute little ranches here. It makes me dream…