---
title: 'Grapes! Upper Outer Napa, Berryessa after Davis.'
date: Thu, 22 Oct 2015 20:15:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3261-1000x750.jpg
tags: ['Touring']
---

Got released at 3pm so decided to take the very long and very scenic way back. I did this before on the 2 day trip to Sac, Hwy 88, Tahoe, and back, so I remembered this route with fondess.  
It was also hot again today but upper 80s rather than 100s. As long as I did not stop for long at all I was fine moving.

Lake Berryessa was worth riding around for the roads alone just south of it! Those roads are fantastic and very pretty too, loaded with fast sweepers (and sometimes dirt / rocks in road this trip).

Took the Pope Valley pass road out of the upper west of Berryessa  
Approaching Pope Valley, I came over a slight rise in the road, and it just opened up ahead of me… No one in line of sight at least 1-2 miles out, nothing! Was in a cruising gear around 4k but I just pinned it. Up to 110 there… heh. Ah, I have to calm down a little. Then the Pope Valley Cross Rd, (where 110 happened again).

I saw some really lovely grape leaves at vineyards around Pope Valley so I decided I needed to keep going past my turn off for a really solid photo op. Almost got it near the tiny P.V. Elementary school, but rode on.

Jackpot!! One of the most beautiful spots – I could hardly imagine it was just waiting up the road from me to come and find on my joyride. It was breathtaking, just spectacular. Only had my iPhone but I probably took 70 pic anyway. Pics do no justice, but in person it will take the breath from you. Reminded me of the gorgeous landscapes in “Jean de Fleurette” and “Manon des Sources” french films of Provence.

[![IMG_3261](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3261-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3261.jpg)

[![zx6e GMM sport touring IMG_3262](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/zx6e-GMM-sport-touring-IMG_3262-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/zx6e-GMM-sport-touring-IMG_3262.jpg)

[![GM sport touring ZX6E IMG_3277](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/GM-sport-touring-ZX6E-IMG_3277-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/GM-sport-touring-ZX6E-IMG_3277.jpg)

[![IMG_3295](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3295-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3295.jpg)

[![IMG_3296](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3296-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3296.jpg)

[![IMG_3298](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3298-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3298.jpg)

I went a little farther actually, and turned around right at this cool old barn:

[![IMG_3305](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3305-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3305.jpg)

Doubled back, passed the very cute looking Pope Valley vineyard (I WILL return and tour this when open!), and took Howell Mtn Road (a very steep pass with some crazy turns going up!) back towards the south and Angwin.  
Deer Park Road back down the pass with lots of turns and passed Viader vineyards on my right without knowing it – a spot I wine tasted a little more than a year ago… Feb 23 2014 to be precise.

[![IMG_3311](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3311-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3311.jpg)

On to Silverado Trail – the small side highway, and the best way to cruise through Napa, and passed some very cute real estate and lovely vineyards.  
[![IMG_3322](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_33221-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_33221.jpg)  
The little turn off road to Shafer Vineyards and Quixote Winery.

[![IMG_3321 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3321-copy-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3321-copy.jpg)

Stopped at Stag’s Leap for some pics of the pretty mountains behind it including Chimney Rock.

[![IMG_3330](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3330-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3330.jpg)

**I CAN’T DRIVE 55!**  
I had been riding on reserve… all the way since I throttled to 110 the 2nd time at Pope Valley. You know where this part is going right? I made it precisely to 50 miles on reserve and into American Canyon before running completely flat out of gas. No stations in the past 5 miles at least… My iPh map GPS position kept trying to say I was on top of a gas station. Uh, yeah, thanks, I would know if I was at a gas station.  
I figured I was 1 mile away from the next one south. And there was an incline on a very fast highway here… great. Wonderful. A work truck finally came by to the intersection I pulled over at, and I asked them if they knew exactly where the closest station was. They said south about a mile so. I walked back to bike, and they yelled over to me “How much gas ya need?” (ooh!)  
“Just a trickle to get me there is all see…” So they produce a gas can and dump a half tank into GM right then and there! They said they’d rather I take it all so they don’t have to worry about it tipping and leaking in the back of truck. And I told them I should pay for the gas and pulled out cash and they said Nope, you’re good! Really nice guys to bail me out. That guy had a shovel head HD bike he said along with “lucky we ride too”. I was.

Was getting pretty tired out from the long day and riding. I left the house in the morning in the pitch dark to ride up to work in Davis after all.  
I was pretty zoned on the highway south after the Carquinez bridge and decided to bail out for my own safety and take San Pablo Dam Road the whole way back which was a very good decision. I perked up being on that road rather than the awful super slab. Had a couple cold ones in the fridge waiting for me when back. It was a pretty spectacular day!  
[![Screen Shot 2015-10-25 at 11.55.24 AM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/Screen-Shot-2015-10-25-at-11.55.24-AM-1024x631.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/Screen-Shot-2015-10-25-at-11.55.24-AM.png)