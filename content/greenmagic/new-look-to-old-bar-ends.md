---
title: 'New look to old bar ends!'
date: Mon, 29 Sep 2014 14:09:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7850-426x320.jpg
---

My bar ends were looking a little standard, boring, but mostly sad on the left grip due to that p.o. low side.  
I actually learned this last night from a “Delboy’s Garage” youtube video. He said, more or less, “Why spend so and so quid on new bar ends when you can polish down to stainless steel your own ends”

Before: yuck  
[![IMG_7850](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7850-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7850.jpg)

Here’s how to do it:  
– phillips screwdriver  
– drill  
– vise  
– sandpaper. I used 100 grit and 400 grit. Worked nice.

Easy steps:  
remove bar ends with captain Phillips.  
Pass a bolt through it and a nut or two to make a ‘jig’ for your drill.  
Mount drill upside down in your vise.  
Mount bolt nut into drill.  
Then use gloves and wrap 100 grit sand paper around the spinning bar end and grind off the old paint, rust, and keep going to grind out the little divots too!  
Then finish with 400 grit (I used it dry) and reinstall.

[![IMG_7853](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7853-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7853.jpg)[![IMG_7855](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7855-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7855.jpg)

AFTER:  
[![IMG_7866](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7866.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7866.jpg)

[![IMG_7861](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7861-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7861.jpg)