---
title: 'Ok, it was a misunderstanding. We’re friends again'
date: Thu, 30 Jan 2014 23:33:00 +0000
author: rj
draft: false
---

I got my bike running again today. Just needed a little throttle with the choke as it’s sat awhile. Ran fairly easily. Drove up to Montclair and back.