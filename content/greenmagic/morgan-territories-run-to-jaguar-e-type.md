---
title: 'Morgan Territories run to Jaguar E Type!!'
date: Sat, 14 May 2016 12:04:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5174-427x320.jpg
tags: ['Uncategorized']
---

Rode the long way to work via the Morgan Territories loop. Which is always gorgeous.  
Got some fuel and then stopped in Clayton’s cute downtown and got a cup of coffee to start the day. It was a beautiful day out and nice to sip the coffee seated outside. Needed a coffee after several margaritas late last night.

And then I got to drive my favorite car again – a 1964 Jaguar E Type convertible (XK-E). Wow is that car sweet!  
Amazing. Gorgeous. Sexy, brutally fast. (60s metric)

[![IMG_5174](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5174-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5174.jpg)

[![IMG_5182](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5182-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5182.jpg)

[![IMG_5162](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5162-500x112.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/IMG_5162.jpg)

![](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2019/07/GreenMagicMan-driving-EType-back-300x170.png) ![](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2019/07/GreenMagicMan-driving-EType-1024x460.png)