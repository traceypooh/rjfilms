---
title: 'Mines Road run Mt Hamilton whole circuit!'
date: Sun, 28 Aug 2016 21:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6359-1000x750.jpg
tags: ['Mt. Hamilton', 'Touring']
---

Just did the fantastic Mines Road run today. What a treasure this road is, amazing!  
[![IMG_6359](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6359-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6359.jpg)  
[![IMG_6337](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6337-1024x221.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6337.jpg)  
[![IMG_6346](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6346-1024x221.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6346.jpg)

[![IMG_6348](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6348-1024x221.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6348.jpg)  
It felt like another world out there.  
Parts of it felt like the greater LA mountains, like the drive up to Big Bear and Snow Summit and around that area.  
[![IMG_6328](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6328-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6328.jpg)  
One small rear tire slip a inch or two, not a big deal today.  
Drove fairly conservative, just enjoying the scenery.  
It was hot on the Mines Road side of the mountain I will say that.  
[![IMG_6332](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6332-e1472445091270-563x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_6332-e1472445091270.jpg)  
I thought “The Junction” bar grill looked pretty cool to check out another time.  
About 160 miles altogether.  
Just after the Junction area was some pretty straight roadway. I scoped it out and then did 3 blast down it. First Pass, 95 mph, which felt very fast on this small road. 2nd pass, 105 mph. 3rd pass 120.  
It was pretty crazy feeling.  
Even the pavement was odd colored in spots, just after the Santa Clara line. A blueish grey sparkly color. Very little houses or ranches along the whole road.  
Was a gorgeous ride, I will definitely do this one again (and again).