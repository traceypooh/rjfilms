---
title: 'Just fabricated DIY rear luggage rack from a crutch!'
date: Tue, 18 Feb 2014 23:38:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image1-300x205.jpg
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image1-300x205.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image1.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image2-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image2.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image3-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image3.jpg)Had this zany idea to try to make a rear rack for my kawasaki zx6e out of crutch parts before, after seeing several in swap shops on the east coast…  
So what do I walk by the other day? A discarded free aluminum crutch on the curb.  
Started it a few days ago, and then got inspired to change my design tonight and use angle brackets to get the long side tubes closer to the bike and fairings to reduce torque stress.  
Planning to add a third rail on the rear very soon. If I had made precise cuts of the crutch beforehand I would have had enough lengths for 3 crossbars but I was cutting conservatively, making several cuts and refining lengths as I was not sure how long the final version should be. Pretty fun project. It bolts into stock bungee mount holes using the stock bungee bolts – that came about as an intentional change for the original design idea with spacers.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image4-300x273.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image4.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image5-300x257.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image5.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image6-300x148.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image6.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image7-300x242.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image7.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image8-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image8.jpg)

Raising the mount points higher up makes the clearance on the fairing more confidence inspiring. If you simply go straight out with a really long custom M6 1.0 pitch bolt  
a ) they are hard to find that long  
b ) there is a lot of extra torque farther out on the bolts and the subframe mounts  
c ) most worrisome is the long bars are a lot lower, almost sitting on the fairing, and I just thought of this setup under load and with bumps and flexing how the fairings could easily get damaged and scratched.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image10-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image10.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image11-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image11.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image12-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image12.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image13-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image13.jpg)

Front mount. You really want to max height to better meet the naturally higher rear mount points in the subframe but not mash into the fairing. I popped the crutch plastic plugs back in for a little less sharp forward edges.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image14-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image14.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image16-300x253.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/02/image16.jpg)