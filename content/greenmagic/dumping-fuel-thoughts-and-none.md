---
title: 'Dumping fuel thoughts. And none.'
date: Thu, 30 Jun 2016 09:42:00 +0000
author: rj
draft: false
tags: ['repair']
---

Well. About 6 times or so, maybe more, GM has dumped fuel pouring out the carbs on startup. Tends to near always happen after sitting awhile like a month or longer. Start up is hard, multiple attempts and then fuel pours out and pools on the ground. Two days ago I tried to re-start GM and three separate cycles it poured out fuel. Fuel tap can be on, key off and no leaks. But with fuel pumping after startup it pours. After third attempt I cut the petcock supply to “off” and ran motor until leaks stopped. I let it sit in the shade then for several hours. Came back and on fourth attempt it ran Great with no leaks! Great yes, but vexing why. I don’t understand if the gaskets dry out, or the float needles get sticky and stick open or if the float bowls themselves get stuck. I think not.