---
title: 'Stator Cover rebuild, JB Weld crack'
date: Mon, 29 Sep 2014 14:24:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image-300x225.jpg
tags: ['repair']
---

Just started (finally) the stator cover project. Bike had been downed, lowsided on the left from previous owner when I bought the bike 2 years ago. The light oil leak has been just annoying. No noticeable drop in oil levels between changes, but a mess maker.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image1-e1400384465732-300x224.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image1-e1400384465732.jpg)

Here’s the original shape. I thought there was only a tiny tiny crack in the bottom, but when I finally pulled the cover I saw from the inside there was a long 2.5″ crack where the two planes meet at the bottom. Much larger than I thought. Cue the JB Weld. I used the original steel reinforced resin formula, not the quick dry stuff.

I first sanded a little inside and outside to rough up the surface and then cleaned / degreased with brake cleaner.  

[![IMG_7815](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7815-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7815/)

[![IMG_7817](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7817-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7817/)

[![IMG_7823](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7823-400x300.jpg)](https://rjfilms.com//blog/greenmagic/stator-cover-rebuild-jb-weld-crack/img_7823/)

  

[![IMG_7828](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7828-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7828/)

[![IMG_7834](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7834-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7834/)

[![IMG_7837](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7837-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7837/)

  

[![IMG_7838](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7838-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7838/)

[![IMG_7832](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7832-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7832/)

[![IMG_7831](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7831-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7831/)

  

[![IMG_7829](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7829-400x300.jpg)](https://rjfilms.com//blog/greenmagic/img_7829/)

  

  
JB welded inside cover and let it sit overnight, and then JB Welded the outside of the cover next. I globbed a lot on, really the leftovers from the mixing process to fill in some of the ugly surface scratches. Yeah you could bondo it, but this is high temp and oil safe, had some left, and figured why not? It’s sandable right?  
[![IMG_7813](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7813-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7813.jpg)  
Sanded down the JB Weld. Cleaned again with brake cleaner.  
Sprayed about 5 light layers of Dodge grabber green rattle can paint over it.

It’s a HUGE improvement for the looks of the bike, it was so ugly before and befouling the middle and rear of the bike with oil and then that attracted and locked in dirt and grime. Gross. And sprayed on my left boot too, and that was annoying. Hopefully I cleaned oil off my boots for the last time!

[![IMG_7823](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7823.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7823.jpg)

Painted dodge grabber green:  
[![stator cover painted green zx6e](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/stator-cover-painted-green-zx6e-e1412284759577-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/stator-cover-painted-green-zx6e.jpg)

Installed!  
[![IMG_7955](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7955-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7955.jpg)

[![IMG_7953](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7953-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_7953.jpg)