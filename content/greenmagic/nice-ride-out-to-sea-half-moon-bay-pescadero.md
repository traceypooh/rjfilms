---
title: 'Nice ride out to Sea, Half Moon Bay, Pescadero'
date: Fri, 04 Dec 2015 15:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3628-copy-1024x598.jpg
---

Got out of work early – riding by 11am!  
On the other side of the bridge and remembered a delicious burrito place in Half Moon Bay so I was off!  
“Happy Taco” has the most amazing burrito – maybe I’ve ever had it’s so tasty!  
Grilled chicken w black beans and everything super burrito spinach tortilla.  
I don’t know how they make it but it makes me weak in the knees. So. Good.

A cruise south out of HMB. I stopped at Covell Ranch Beach and walked out to the sea there and relaxed with a carbonated drink. It’s a bit of a walk out there in full moto gear 1/3 mile each way to the viewpoint just south of the gorgeous golf course at the Ritz Carlton.  
I didn’t quite realize it – but I had stopped here before last year on a HMB mountain bike cruise with the x.  
Probably stopped for 45 minutes altogether, which becomes a critical timeframe next:

Bird of prey on walk to beach. I got quite close to it before it decided to move  
[![IMG_3628 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3628-copy-1024x598.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3628-copy.jpg)

[![IMG_3640](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3640-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3640.jpg)

Viewpoint at Cowell Ranch beach. Quite serene.

Walk back to bike and…

BIKE DEAD. makes no noise to start at all!  
I get all the gear off – I know this is going to be trouble. Removed helmet and earplugs and then and only then can I hear the faintest clicking noise when I push the start button. Of course, clutch in and out, in and out of gear, side stand up / down no difference.  
Also rocking bike back and forth in gear in case the starter teeth are stuck gains nothing.

There are exactly 3 cars in the lot, one empty.  
I ask the first guy if he has a voltmeter by any chance – he does!! Wow. Only it doesnt really work, displays gibberish screen like it’s broken. oh well. No jumpers.

2nd car DOES have jumpers! Yay!  
Then we try to get it started for awhile without luck.  
Finally, I rock bike back and forth hard in first gear and have pos – pos leads and neg – neg leads on the posts (not neg to ground). AND minivan motor running. After the rocking, with car on, it finally starts. Phew!  
I ride it about 20 miles straight south to Pescadero at revs over 4000 the whole way and once there, make short stops the usual seaside viewpoints leaving the bike running:

[![IMG_3649](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3649-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/12/IMG_3649.jpg)

I only test the ability to re-start about 35 miles of constant motor on with a big hill in front of me. I come back to bike after a minute and it starts – somewhat a little weakly but starts!