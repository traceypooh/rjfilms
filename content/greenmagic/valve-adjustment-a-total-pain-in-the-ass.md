---
title: 'Valve adjustment a total pain in the ass'
date: Sat, 14 Nov 2015 21:01:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0450-427x320.jpg
---

Mileage: 60,400  
I cannot say I am enjoying this particular job.  
Firstly I wasted several hours lining the timing marks up wrong. My digital Haynes copy timing mark photo is completely illegible. And all I could find online was ZX6R and other Kwawk instructions so I thought I was supposed to line up  
1/4 and 2/3 lines with the split in the cases – horizontally. Not the way.

This (apparently) is the way that I had to sort of figure out myself:  
[![IMG_0450](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0450-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0450.jpg)

[![IMG_0454](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0454-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0454.jpg)

[![IMG_0457](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0457-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0457.jpg)

[![IMG_0448](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0448-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0448.jpg)

[![IMG_0426](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0426-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0426.jpg)

My clearances are really bad. 3 valves are at minimum on the gauges – I can’t even get the thinnest one at .0015 ” through. And I’m irritated that the engineering seems to assume you have dwarf lady Japanese tiny hands. I can hardly move my mitts around in there. Pain in the…  
I cannot see how the hell you are supposed to get any gauge to go through the exhaust lobes of #1 and #4, especially the farthest outside ones. It’s ridiculous. I guess I might as well make up some numbers.

My exhaust clearances are in inches:  
Exhaust:  
.0015, <.0015, .005, .003, .006, .004, .005, and zero or <.0015 Intake: .005, .005, .006, .005, .0055, .007, .003, .004 So it looks like I have exactly ONE valve in spec @ .007 and it's on the tight end of the spectrum. Not good, I guess, very not good. Been on this all day, and haven't even gotten to pulling the camshafts out yet to measure what shims are in there. ---- And now (Sunday) I got all the old shims out, and calculated the new ones which took hours to be sure. I had to drive to town twice to get them as I made one error. I put the cams back in, and the timing chain looks all slacked wrong, so I am worried about this: [![IMG_0482](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0482-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_0482.jpg)

—-  
Wednesday WOOHOO!

Fired up bike and it sounded great!  
I then finishes installing the air box, hoses, tank and fairings and hit the starter again several hours later. It started so fast it kind of shocked me. It’s never started so instantly. Also I can go off the choke now very quickly and it idles nice and smooth.

I also installed the replacement ignition coil set that kjfire4life sent me from North Carolina several months back. New NGK CR9E spark plugs also installed. The original set I put was back at 43,400 miles so they’ve lasted a good while.

Very very psyched today to have this done! And I did not have to pull out the camshafts again and try to rotate them or get rid of the slack. The CCT tensioner and the crank turning and pulling the chain straightened it all out instantly.