---
title: 'Labor Day Marin trip 136 miles beautiful'
date: Mon, 01 Sep 2014 19:34:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9070-1024x768.jpg
tags: ['Touring', 'Uncategorized']
---

[![road](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9070-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9070.jpg)

road

Late start, buddies to blame, and also when I got to their house I checked my chain and then the chain on the ’78 Honda CB400A and I couldn’t believe it! It was so ridiculously and dangerously loose, it was slapping up and down with 2-3″ of slack!!! My buddy had been complaining about noise and here we find it! I showed her how to adjust the chain tension and we did that and made short work of it. Lubed up our chains, adjusted tire pressure on my ZX6E, and the CB and buddy’s ‘Busa and finally off we go at noon.  
Drove over Bay Bridge and through Presidio a little and over Golden Gate Bridge. Spectacular albeit too hot weather. 80 degrees and full sun.  
Fueled up in Mill Valley and loaded and started all our cameras. Route 1 to Stinson is heavenly, although today too many tourists and cars. Damn!  
Also Stinson Beach was so backed up on the pass we had to turn around.  
Decided on new route back and left turn to Mt. Tamalpais.  
We rode past a beautiful eat spot in the mountains with an incredible view. We had to stop there, and we were starving anyway it was 3pm!  

[![Fuel stop Mill Valley & start cams](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9059ps-300x185.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9059ps/)

Fuel stop Mill Valley & start cams

[![Lunch stop in the mtns.](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9065-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9065/)

Lunch stop in the mtns.

[![Lunch spot!](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/GOPR0066-300x225.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/dcim100gopro/)

Lunch spot!

  

[![from Mt Tam](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9068-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9068/)

from Mt Tam

[![on the road](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9069-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9069/)

on the road

[![road](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9070-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9070/)

road

  

[![IMG_9073](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9073-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9073/)

[![Alpine Lake dam](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9077-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9077/)

Alpine Lake dam

[![Alpine Lake](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9085-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9085/)

Alpine Lake

  

[![Lucas Valley Road mini summit](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9096-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9096/)

Lucas Valley Road mini summit

[![IMG_9098](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9098-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9098/)

[![IMG_9094ps](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/IMG_9094ps-300x224.jpg)](https://rjfilms.com//blog/greenmagic/labor-day-marin-trip-136-miles-beautiful/img_9094ps/)

  

After lunch over Mt Tam summit roads and down to pretty Alpine Lake and rode over the dam. Into Fairfax and then reversed direction out of town to Sir Francis Drake westbound to Nicasio Valley Road and then right turn to Lucas Valley Road back to 101.

It was a great trip!