---
title: '53884 miles 4th oil change'
date: Mon, 16 Mar 2015 23:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-includes/images/smilies/frownie.png
---

Sticking with last change elements: Shell Rotella T6 synthetic 5w40 oil and the Purolator Pure One PL14610 filter.
Honestly was a royal pain the ass removing the other Purolator. Major pain. It was like it was 10 year rusted on.
Anyway, very relieved I finally swapped out the oil. Went a little more than 4400 miles, hadn’t wanted to exceed much of 3000 miles on this first synthetic oil.
Am hoping for glass smooth shifts again tomorrow and maybe this stalling problem alleviated, but that’s probably too much to hope for.
Also, after the past 1800 mile trip finally sprayed the chain with silicon lube, had none with me on the trip ![:(](https://rjfilms.com/greenmagic/wp-includes/images/smilies/frownie.png)