---
title: 'Re- balanced my front wheel, fixed brakes'
date: Sat, 20 Jun 2015 14:42:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2399-e1435523260407-240x320.jpg
tags: ['repair']
---

Mileage: 56,284  
Finally got my front wheel off – had to borrow a 14mm allen wrench was I kept breaking bolts with the triple nut snugged up method.  
I hand balanced my wheel again using some cat litter boxes that I leveled side to side and front to back. I find out two days later my re-balance worked Great!! Waay better than the $5000.00 balancer machine ‘that woman’ ‘balanced’ my tires with. Look at all the wheel weights I had to remove! it was way off. Made the Sonora Yosemite trip at speed not fun.

Also with the wheel off, I hand cleaned all the brake gunk off the right side rotor (and what little was left on the left rotor) and shot it all up with brake cleaner.  
Wow! Do these Nissin caliper brakes finally brake!

[![IMG_2399](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2399-e1435523260407-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2399-e1435523260407.jpg)

[![IMG_2400](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2400-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2400.jpg)

[![IMG_2402](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2402-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2402.jpg)

[![IMG_2403](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2403-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2403.jpg)

[![IMG_2404](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2404-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2404.jpg)

[![IMG_2407](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2407-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_2407.jpg)