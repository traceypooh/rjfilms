---
title: 'Mini Rt 1 tour, Pescadero lunch after work, Pacifica'
date: Fri, 06 Nov 2015 19:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3344-427x320.jpg
tags: ['Touring']
---

I was done with the main part of work at 11am and riding from work at noon on Friday!  
Decided after four day work with a double up job on a special day it was time to celebrate a little with a joyride to the coast and along it in the sunny weather.  
Just before La Honda small center I hit up some sweeping turns and looped it there about a dozen or more times. Was a rush, trying to get the knee down on these huge turns. Didn’t but it was fun!

I stopped at Applejacks hoping to get lunch, but it was close to empty and they have no kitchen apparently. So continued on out of La Honda.  
I took a new road for me, up past the Jones Gulch YMCA and eventually got on the windy mtn pass road direct to Pescadero.  
[![IMG_3344](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3344-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3344.jpg)  
This was my third day using my brand new Shoei helmet and I’m really loving it. Very solid feeling, close to air tight and quiet.  
[![IMG_3354](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3354-e1448386281924-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3354-e1448386281924.jpg)  
[![IMG_3352](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3352-e1448386244364-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3352-e1448386244364.jpg)

Very pretty riding through redwoods and nice twisty roads.  
[![IMG_3360](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3360-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3360.jpg)

[![IMG_3369](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3369-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3369.jpg)

[![IMG_3377](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3377-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3377.jpg)

[![IMG_3386 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3386-copy-302x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3386-copy.jpg)

[![IMG_3381](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3381-e1448386303234-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3381-e1448386303234.jpg)

Stopped there for a (late) nice sandwich at the General Store and had a little conversation with a cute girl there from out of town.  
Out to the ocean and Pescadero State Beach. I really love the Pescadero area. I dream of living on a ranch there, apparently everyone else does, I saw a house for sale for $1 Million.  
[![IMG_3392](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3392-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3392.jpg)

I always like to stop at this one overlook at take a picture:  
[![IMG_3389](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3389-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3389.jpg)

Along the 1 up through Half Moon Bay and Moss Beach. I stopped at the Moss Beach Distillery here just to see the beach in the daytime there (only been there once at night).  
It was a beautiful beach view up some small cliffs. I walked out to the peak of one cliff, and found out later it was all restricted. Was kind of funny because there were tons of people around and Distillery staff and no one said a thing to me. Took a few ‘illegal’ pictures there I guess you might say.  
There were humpback whales spouting out in the ocean.  
[![IMG_3394](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3394-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3394.jpg)

[![IMG_3397](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3397-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3397.jpg)

I tried to find that beach overlook and wound up on California street and down to a little beach park there that was quite cute and I plan to go back to on a warm day and enjoy the beach there.

Onward toward Pacifica, and I stopped at a beautiful overlook on a turnout on Rt 1 in the curves before Pacifica:  
[![IMG_3400](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3400-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3400.jpg)  
[![IMG_3427](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3427-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3427.jpg)

[![IMG_3407](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3407-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3407.jpg)

[![IMG_3411](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3411-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3411.jpg)

[![IMG_3421](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3421-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3421.jpg)

It was a beautiful day out there, and I was pretty thrilled to get some ocean riding in. Very peaceful out on the road. The water always makes me feel calm.