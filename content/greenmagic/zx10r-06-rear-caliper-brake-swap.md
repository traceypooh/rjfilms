---
title: 'ZX10R ’06 rear caliper brake swap!'
date: Sat, 24 May 2014 23:56:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image5.jpg
tags: ['caliper', 'DIY', 'rear brake', 'upgrade', 'ZX10R', 'Zx6e', 'zzr600']
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image5.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image5.jpg)  
Mileage: 47,292

Yesterday I just changed out my ’95 stock caliper to a 2006 ZX10R. It’s a direct swap. No mods needed. (Other years of similar era would work too, just check).

Although same lackluster performing caliper they are easier to find, cheap, newer, as quite a few 10 owners upgrade and have them laying around. Had to change pads anyway, and this was almost priced as same price as pads alone.

I bought the entire unit for $40 with about 6K and almost no use (off ex-race champ who never brakes with rear except trail braking). It had been upgraded to steel Galfer line, EBC sintered metallic pads. Came with brake piston cylinder, lines, reservoir, brake lever, clip on, and fluid all still attached with piston freely moving.

Some interesting things happened. Was going to try to swap everything, but found even though the brake cylinder looked identical with same number on front, it has a different part number stamped on the back. And it’s length to the pivot on the foot brake lever is shorter by about 1/2″ or more. I was going to try to use it anyway, figuring it was newer, but I also realized I’d have to mess with the brake light sensor unit and change its length on the collar if I could etc etc…. And at 9pm at night wasnt wanting to make it a longer project. Basically if you just bolted it in as is, the foot brake lever would drop lower, and have less travel to brake, and the brake light would probably be on the whole time unless you adjuster that plastic sensors length (it clearly is adjustable but how far and which direction I did not experiment with after deciding to keep my stock cylinder, upper hose,mand stock upper reservoir.

In fact, the caliper is listed as a different part number but fits no problem. The pads are identical to both ZX6E and ZX10R 2006 era and other years.  
The Galfer steel brake line was quite long, and I had to flip over both banjo bolt ends  
As they seemed to be angled wrong.  
The upper reservoir looked the same, but looking closer the feeder at bottom was in reverse position, and then I noticed it had upper and lower markings on both sides! So although its different Kawi probably had it doing double duty in multiple bikes. The brake hoses are wider gauge on the E stock unit all around compared to the ZX10R.

So in summary, easy caliper and pad swap out, no problem at all. Everything interchanges and mixes and matches perfectly. Brake works great now too! Lots of bite in back now.  
Lines and banjo bolts all match and bolt threads etc…  
The ZX10 brake cylinder could I feel be used – it has identical bolt in position and pattern to footpeg brackets. But you’d probably have to lengthen the pivot bolt by loosening the nuts and extending the pivot down (I did that in fact but stopped the install due to other reasons.)  
The upper reservoir could be used with zx10R hoses to cylinder if all off same bike, but you would probably either have to mount it backwards up top, and have the angle of the outlet at bottom pointing slightly the wrong way or better yet just make a metal extension piece to keep the reservoir facing the way the E wants it, it would be like a 3 or 4 inch piece with a mount spot.

ZX10R complete setup as purchased in pic below.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image6.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image6.jpg)

Original stock unit before replacement. Pads totally shot.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image7-e1401047966496.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image7.jpg)