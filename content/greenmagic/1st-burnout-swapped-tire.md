---
title: '1st Burnout! & swapped tire'
date: Sat, 25 Apr 2015 23:23:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/Screen-Shot-2015-04-25-at-9.21.28-PM-1024x544.png
---

[![Screen Shot 2015-04-25 at 9.21.28 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/Screen-Shot-2015-04-25-at-9.21.28-PM-1024x544.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/Screen-Shot-2015-04-25-at-9.21.28-PM.png)  
[![Screen Shot 2015-04-25 at 9.22.48 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/Screen-Shot-2015-04-25-at-9.22.48-PM-1024x556.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/Screen-Shot-2015-04-25-at-9.22.48-PM.png)  
Took 5 tries, but I finally got a good smoking burnout on my 1st set of Dunlop Q3 tires I had installed April 2014. These tires have about 8200 miles on them and are shot. Last year I wanted to try a burnout but instead did a chickenout. The burnout was scary fun! The best kind of fun. My heart was beating pretty fast there. It was many kinds of awesome. I’ve always wanted to do one, but been afraid…

I started the tire swap at 5:45pm. And was done by about 8:15pm. Now that ALSO included removing the new tire and re-mounting it the correct direction – I forgot to check the direction again this year, and with a 50-50 chance of being right, of course, both last year and this year I was wrong. I was irate. It was all going so well too…

Anyway, the new tire is on. Now tomorrow just have to go air it up and set the new bead – something I dread a bit.  
But hey, at least this time breaking the beads was no sweat thanks to the huge wood clamps I used on loan from the Oakland Public library. Hooray!

[![IMG_0001](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0001-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0001.jpg)

[![IMG_0002](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0002.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0002.jpg)

No duct tape on hand this year, so I resorted to packing tape which was not ideal. A p.i.t.a. which instigated much swearage.  
[![IMG_0005](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0005-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/IMG_0005.jpg)

Here’s the tools I used to break the bead which was a piece of cake this year thanks to these burly clamps.  
[![zx6e breakin beadsScreen Shot 2015-04-27 at 10.37.49 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.37.49-PM-386x320.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.37.49-PM.png)  
[![zx6e breakin beadsScreen Shot 2015-04-27 at 10.39.10 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.39.10-PM-400x223.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.39.10-PM.png)

You can exploit the weakness of the bead with insane angles with the two adjusters and keep the angle force tons of pressure on the bead and it’s blam! done!  
[![zx6e breakin beadsScreen Shot 2015-04-27 at 10.38.40 PM](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.38.40-PM-500x280.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/04/zx6e-breakin-beadsScreen-Shot-2015-04-27-at-10.38.40-PM.png)