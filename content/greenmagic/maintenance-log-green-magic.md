---
title: 'Maintenance log Green Magic'
date: Sun, 23 Aug 2015 11:49:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0075-copy-400x268.jpg
tags: ['DIY', 'repair']
---

[![DSC_0075 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0075-copy-400x268.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0075-copy.jpg)

43,174 8/15/12 Purchased!

43,295 9/16/12 Carbs cleaned (2 pilot air jets clogged), New fuel filter, clean & oiled Uni air filter, cleaned plugs, re-routed 2 hoses properly, blew oil out rear canister hose & rerouted, mixture screws reset from 3.4 turns to 2 turns, re-set idle adjuster.  
9/20/12 throttle cable adjustment fixed, R/R wiring chopped and fixed.

10/08/12 New AGM battery  
11/28/12 cleaned carbs again (were pretty clean).  
11/30/12 fuel pump tested ok, checked for spark ok  
2/8/13 43,305 Holy shit! Green Magic is back from the dead! NEW spark plug wires installed.  
2/16/13 Oil change #1  
2/26/13 installed painted green windscreen, fuel line clamps installed  
4/8/13 blue carbon fiber seat cover installed  
4/24/13 white mirrors installed  
43,697 5/2/13 replacement regulator / rectifier installed  
43,950 6/16/13 coolant replaced and flushed  
9/30/13 chain tension checked and ok less than 1.3” slack  
9/29/13 Double Bubble windscreen installed  
45,381 2/1/14 Green LED gauges installed! installed ram air guards  
46,439 5/4/14 Tire swap #1 to Dunlop Q3  
46,931 5/17/14 Oil change #2, epoxy try on stator cover (x)  
47,292 5/24/14 ZX10R rear caliper installed w steel line  
47,696 balanced rear wheel  
49,469 10/13/14 New Clutch, stator cover leak fixed + painted. replaced clutch rod  
Oil change #3 – Rotella T6 and Purolator from now on.  
51,600 2/23/15 cleaned carbs, cleaned out starter jets, last 2 new float bowl gaskets installed  
53,884 3/16/15 Oil change #4  
54,860 4/26/15 Tire swap #2  
54,860 Nissin brake calipers mod installed ’05 F4i brakes, front and rear brake fluid swapped.  
2nd re-crimp of R/R wiring. Shock pulled and Fox TC lined up.  
55,318 6/10/15 water pump repair started  
55,435 6/13/15 “Hot idle” problem fixed – loose coil spark plug wire deterioration issue.  
56,284 6/20/15 re-balanced front wheel, brake cleaned front brakes

57,320 8/9/15 sanded off gas tank p.o. damaged rust spot and painted white, JB Welded cracked clutch cover  
57,998 8/22/15 installed replacement Kawi front brake lever  
58,241 8/27/15 installed replacement thermostatic fan switch  
8/30/15 installed replacement clutch cover, green painted clutch lever,  
bar end for drop damage, clutch knob  
57,998 8/33/15 Adjusted chain slack, reconnected speedo cable that had fallen out.  
58,321 9/6/15 Oil change #5  
58,250 9/3/15 fixed replaced clutch cover leak, verified fan comes on. re-bent clutch lever back.  
10/9/15 clutch lever stripped to bare metal aluminum at D.C.  
59,646 11/02/15 rebuilt fuel petcock (didn’t need it).  
59,982 11/08/15 Exhaust mod. Chopped out baffles. Debaffle. Installed new 5/16″ Parts Unlimited fuel lines  
60,401 11/18/15 **Valve adjustment** job. Ignition coils replaced (both). New spark plugs, adjusted throttle cables.  
62,470 2/12/16 Gold X Ring chain and sprockets back to factory 16/48 teeth. 3rd set of tires: Michelin Power Pilot 3  
63,134 2/23/16 Oil Change #6 (Pleasanton street after shoot #97)  
63,325 5/9/16 stator to regulator / rectifier re-wire upgrade to 10 gauge THHN wire (2 white and 1 black runs)  
63,465 6/28/16 carbs dumping fuel. 3x, 4th after running bike with petcock ‘off’ and sitting 2 hours and no leaks  
63,567 7/1/16 headlights lo and hi beams repaired. Hand grip wired back to stock. New lead back of R/R white stator to junction box old yellow connection (new blue wire).