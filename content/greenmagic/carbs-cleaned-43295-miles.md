---
title: 'Carbs Cleaned!! 43,295 miles'
date: Sun, 16 Sep 2012 14:26:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/4-carbs-zx6_8791-150x150.jpg
---

Just finished carb clean job! 2 of 4 air jets to pilot jets were clogged shut. Gently poked them out with twisty tie. Evinrude “Engine Tuner” could not dissolve clog. Finished carb reassmbly last night.

[![4 carbs zx6_8791](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/4-carbs-zx6_8791-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/4-carbs-zx6_8791/)

[![4carbs zx6_8790](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/4carbs-zx6_8790-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/4carbs-zx6_8790/)

[![IMG_8788](https://rjfilms.com/greenmagic/wp-content/uploads/2013/08/IMG_8788-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/img_8788/)

  

[![zx6emechanicals_8766](https://rjfilms.com/greenmagic/wp-content/uploads/2013/08/zx6emechanicals_8766-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/zx6emechanicals_8766/)

[![zx6emechanicals_8798](https://rjfilms.com/greenmagic/wp-content/uploads/2013/08/zx6emechanicals_8798-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/zx6emechanicals_8798/)

[![zx6emechanicals_8794](https://rjfilms.com/greenmagic/wp-content/uploads/2013/08/zx6emechanicals_8794-150x150.jpg)](https://rjfilms.com//blog/greenmagic/carbs-cleaned-43295-miles/zx6emechanicals_8794/)

  

New fuel filter

Cleaned and oiled Uni Air filter

Inspected, cleaned, and checked gaps on all 4 plugs

re-routed 2 hose connections properly, blew out oil filled rear evap line and re-routed it correct. Fix potential ram air leak.

re-set pilot screws from 3.4 turn to 2 turns  
re-set idle screw adjuster now to where it supposed to be.

So status?

Started her up for first time in 2 weeks. Hard to get fuel going and start, but first time ever she started up cold with push button! rough at first, and I have to fix the throttle cable adjustments (tried to match to my eye the turns on the adjuster but the throttle return lags quite a bit.)  
It now can actually idle down to 1300 rpm without dying which it never did before!  
It’s all about cleaning out thoroughly those pilot jets and pilot housings!!  
Main jets: 140 Pilot jets: 35 Keihin. Needles: N1VC  
Set pilot mixture screws to 2 turns out.