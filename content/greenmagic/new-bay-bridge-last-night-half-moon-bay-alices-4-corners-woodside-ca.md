---
title: 'New Bay Bridge! last night, Half Moon Bay, Alices, 4 corners Woodside CA'
date: Mon, 02 Sep 2013 10:04:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image13-150x150.jpg
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image13-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-14/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image12-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-13/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image11-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-12/)

  

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image10-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-11/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image9-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-10/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image8-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-9/)

  

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image7-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-8/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image6-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-7/)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2013/09/image5-150x150.jpg)](https://rjfilms.com//blog/greenmagic/232/image-6/)

  

Nice ride been wanting to do for awhile. Oakland via favorite moto riding roads for me – Redwood to Hayward over San Mateo bridge, 92 to Skyline, south to 4 Corners / Woodside / Alices restaurant, windy small road Tunitas Creek Canyon I think, to Half Moon Bay! Route 1 north to AF and then took brand new Bay Bridge on first night it was opened after 10pm! It was pretty exciting.Here’s link to the video of the bridge crossing at night. More pics to follow.[https://archive.org/details/GreenMagicNewBayBridge3min](https://archive.org/details/GreenMagicNewBayBridge3min)

So this was the trip excuse: wild rumors swirled for days the brand new Bay Bridge \* might \* open up the night before – maybe late, maybe not – before the official 5am next day opening after labor day weekend.  
Lots of bikers cut the cage line like us to get a better position, and then… wait for 45 minutes at least, before some bikers took it upon themselves to barge up the ramp!

The New San Francisco to Oakland bridge is quite something.

————