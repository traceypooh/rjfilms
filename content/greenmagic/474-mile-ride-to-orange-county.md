---
title: '474 mile Ride to Orange County!'
date: Wed, 04 Mar 2015 21:45:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image3-400x300.jpg
tags: ['Touring']
---

Ok, a bit crazy. I get a call at 10:32am asking about work in Boston. Then, as I mention I’m still out in CA after work last week, this guys asks, oh, if I am in CA can I work there locally coming up? (Ok, sure, where?) LA, are you LA? (No) Can you get there (hmmm… yes). Ah, it’s a bit south of LA, Dana Point? (Ok) Um, by tomorrow? For a 6 day job?  
Ah…. You can imagine what happens next. I start packing while on the phone. Call my great friend Bob next who lives 30 miles from the work location. He’s around and wants me to come visit! Confirm job formally.  
Pack for a week, shower, eat lunch, prep bike, pack, add 3/4 qt. synthetic oil (was a bit below low line), check tire pressure. Wow. Have to get out of here as fast as possible. This is gonna be a long day…

Start miles: 52,130.  
So off I go, leaving at 12:45pm. It’s 74 degrees in Oakland. Kind of hot, didn’t even expect that.  
Spontaneously I decide not to go fastest way on the horrible 5 up north, but wait to pass 152 at Gilroy. And nearing that, I decide its so pretty out and I Love the 101 route so I will ride further south on it and figure out the next or second… Or 3rd pass over to the dumb 5.  
I ride 121 miles straight to well south of Gilroy, have to pee and getting low ish on fuel. I finally stop at the entrance to Constellation Wines in Gonzales, CA, and, well, I pee on their tree. Was Getting a little foggy riding too. [![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image3-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image3.jpg)  
Short walk break there. And back on Green Magic I go.  
Shorter leg to first fuel up stop in Kings City, just south of that town at Wild Horse Road and the cheapie station there. Boy have I acquired a lot of bugs on my helmet. Yuck. Wash shield off in bathroom. Much better, but it won’t last… Saddle up and move out, have a long way to go and I keep cracking the whip on myself.  
Riding along the 101 south, thinking maybe I’ll take the pass out of Paso Robles… and then… I decide I’d love to stop in Pismo Beach, and ride the pass to Santa Barbara, a favorite drive in a cage for years… And I’ve never ridden it. California is so green everywhere this time of year it’s just lovely. I want more.  
[![IMG_8171 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8171-copy-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8171-copy.jpg)  
My first real big stop is in Pismo Beach. Too early for dinner, I just stroll about the boardwalk and eat a packed apple for about 30 minutes and snap some pics. And water is good now too.  
[![IMG_8188](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8188-e1427786920408-225x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8188-e1427786920408.jpg)[![IMG_8187](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8187-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8187.jpg)

[![IMG_8177](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8177-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8177.jpg)

Off to Santa Barbara.  
[![IMG_8190](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8190-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8190.jpg)  
I make a short detour here for pics and wind up going through Los Alamos. This becomes very helpful later on the ride north.  
[![IMG_8194](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8194-e1427787208245-366x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8194-e1427787208245.jpg)  
Of course I turn off the 101 and take the beautiful 154 pass shortcut. It’s such a nice segment of this trip, I’ve been looking forward to it the whole ride. The sun is setting and huge moon has risen right when I take this exit.  
[![IMG_8197](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8197-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8197.jpg)  
[![IMG_8202](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8202-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8202.jpg)  
[![IMG_8199](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8199-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8199.jpg)  
[![IMG_8201](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8201-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8201.jpg)It’s dusky, and getting darker but oh so pretty anyway. Thrilled I decided to take the 101 after all. Coming down the peak, the last vestige of setting sun and some red are on the far horizon.  
[![IMG_8203](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8203-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/IMG_8203.jpg)I make it to Santa Barbara – a town I love – and gas up in Santa Barbara at 6:44pm, was really low on fuel and took 3.7 gallons. Check my texts and a contact there can’t meet for another 45 min for dinner, and I’d be waiting around… Maybe another time we will meet up. I head off to southern border of S.B., the Santa Claus lane exit, and there’s a cool little burger place there called Padaro Grill I’ve been to once before, that time on a sunny day piloting a classic Buick GS convertible about 1972 vintage, with a huge 440 motor with my (ex) girl at my side… Ah, Memories…  
Order at 7pm. I get a CA burger with a big slab of Avocado. I sit in a large couch outside under a heat lamp, it’s getting cold. Ah… A great 30 minute pit stop. I bundle up with my last minute added wind breaker jacket under my vented leather AlpineStars jacket which proves to be a critically good decision. It’s getting dark and cold out there. And I still have prob 80-100 miles to go.

I arrive at 9:450pm both bleary eyed and a little foggy tired and a little hyper too. Really, this trip wasn’t anywhere near as hard as I thought it would be. I mostly rode at indicated 75-80 mph on the 101.  
Very few stops, I clipped off the miles fast. From when I left, at one point I made 300 miles in 5 hours!! I’ve never paced like that on a bike before.  
Only longer day in my life was the earliest post here on a different bike, the CX500 Honda where I went 580 miles in one day in France. That was brutal. This was a lot better!

A few days later next to my work location, Dana Point overlook:  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/03/image.jpg)