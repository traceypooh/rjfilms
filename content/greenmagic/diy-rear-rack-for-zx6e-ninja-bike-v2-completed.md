---
title: 'DIY rear rack for ZX6e Ninja bike V2 completed!'
date: Wed, 23 Apr 2014 19:43:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-6-1024x764.jpg
tags: ['DIY']
---

Version 2 just finished today!  
I used 1/2″ steel EMT tube. A 10′ piece costs about $2.50 at Home Despot. (in electrical aisle). It’s that metal conduit for garage / basement / outdoor wire running. It has some sort of coating on it too to make it ‘waterproof’ and not rust – although the cut and drill points remain to be seen how they hold up.  
This is a pretty cool solution if you’re like me, and like cheap.  
It’s stronger and better looking than V1. (steel vs aluminum)  
[![ZX6e DIY rack Green Magic 6](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-6-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-6.jpg)

I also only drilled through the lower part of EMT at the front mounts by the seat for this version. Cleaner and stronger. I used spare battery bolts and nuts up there – c’mon you been riding more than 2 years you know you have extras of those kicking around!

[![ZX6e DIY rack Green Magic 4](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-4-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-4.jpg)

[![ZX6e DIY rack Green Magic 5](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-5-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-5.jpg)

[![ZX6e DIY rack Green Magic 2](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-2-764x1024.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-2.jpg)

[![ZX6e DIY rack V2 Green Magic 1](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-V2-Green-Magic-1-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-V2-Green-Magic-1.jpg)

[![ZX6e DIY rack Green Magic 3](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-3-1024x764.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-3.jpg)

This version looks a LOT nicer. The crossbars in the back I integrated more into the design. Cleaner, lower profile for bags on top, and less nuts to shake around and loosen up riding.  
This bike already does a good enough job shaking my own nuts around. [![nutkick](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/nutkick.gif)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/nutkick.gif)

I drilled into the inside portions only this time for the ‘crossbars’ at the rear. It was donor metal I scrounged from a metal rack I found abandoned on the street (that’s how we roll in the city here, or really how people and hipsters etc… do when they move here and move on). I measured and cut them to size with a hacksaw.  
Oh and I should have mentioned it, I more or less followed the crutch double bends. I bent it a little more custom to raise it a little higher and dip the angle back lower a little.  
But what I did with the EMT is bend the back two bends first with an EMT pipe bender. I have a tiny kink on the inside edge of one, but it’s not a big deal.

Here’s some updated measurements I made in 2015 of the same rack:

[![DIY luggage rack zx6e V2 brackets dimensions](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/DIY-luggage-rack-zx6e-V2-brackets-dimensions-396x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/DIY-luggage-rack-zx6e-V2-brackets-dimensions.jpg)

[![ZX6E rack dimensions IMG_2743 measured write on](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6E-rack-dimensions-IMG_2743-measured-write-on-282x320.jpeg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6E-rack-dimensions-IMG_2743-measured-write-on.jpeg)

[![ZX6E rack dimensions IMG_2744](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6E-rack-dimensions-IMG_2744-500x168.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6E-rack-dimensions-IMG_2744.jpg)

[![ZX6e-DIY-rack-Green-Magic-2-600x](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-2-600x.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-2-600x.jpg)[![ZX6e-DIY-rack-Green-Magic-4-600x](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-4-600x.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/04/ZX6e-DIY-rack-Green-Magic-4-600x.jpg)