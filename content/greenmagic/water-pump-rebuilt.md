---
title: 'Water pump rebuilt.'
date: Fri, 12 Jun 2015 20:33:26 +0000
author: rj
draft: false
tags: ['DIY', 'repair']
---

I can indeed confirm this Honda oil seal part fits the ZX6E / ZZR600 model: Honda part #: 91201-MF2-003 description: OIL SEAL (12X28X7) price: US Dollars $4.99 I had to special order the part and installed it today. Very nice fit. This is the oil seal that goes around the impeller shaft at the very end (not the wider O Ring that goes around the water pump impeller housing.