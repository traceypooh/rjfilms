---
title: 'Just debaffled my exhaust. ZX6E exhaust mod.'
date: Sun, 08 Nov 2015 16:46:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3752-428x320.jpg
---

Mileage 59982  
Custom modified my stock exhaust. It took a little over 2 hours. I shot pics and video of the process and will upload a YouTube video of it soon.  
I also installed a new 5/16″ Parts Unlimited polyurethane fuel line. The main line to the fuel filter and filter to pump runs.  
I installed a new WIX 33011 fuel filter while in there. The old one was in there for 3 years and maybe 16,000 miles so time for a fresh one.

On startup… Wow this thing is loud. Kind of annoying me loud. It also made a huge after fire POP noise on startup. But it idled immediately at 1500 rpm cold with the choke – it’s never done that.  
When I got it warmed up, it sounded pretty good at 1500 idle. And shutting it off and re-starting it warm, it sounds pretty cool.  
But I will say at 5000 rpm it’s loud. 6000 rpm is the old 9000 rpm noise level I’d say.  
I’ve been very curious to try this out for a long time. I commute and lane split more than I’d like and I have been thinking this could be better to alert cages I’m coming up the lane.

I used a few bits to drill out the 3 exhaust cap rivets. Then I used a strap wrench to spin free the caps – although they were close to a bit loose and did one by hand. I took a hacksaw to the skinny outlet pipe there for the next step: Then I used a 2″ metal hole saw to cut a 2″ circle out of back of exhaust around that small pipe. Then I wiggled and man handled and broke free the rear baffle tube so it bypasses the whole last chamber and three tubes it no longer needs. That part was the most tedious, although not terribly bad.  
Kind of interesting overall.  
Have no idea what it will be like at speed or the highway… But I will find out tomorrow on my commute!  
I wear ear plugs all the time anyway, so I thought this might not impact me as much as others.  
[![IMG_3752](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3752-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3752.jpg)

[![IMG_3753](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3753-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3753.jpg)

[![IMG_3754](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3754-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/11/IMG_3754.jpg)