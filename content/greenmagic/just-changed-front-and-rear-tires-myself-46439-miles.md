---
title: 'Just changed front and rear tires myself!! 46,439 miles.'
date: Sat, 03 May 2014 20:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image2-e1400385002893.jpg
---

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image2-e1400385002893.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image2-e1400385002893.jpg)I just changed the front and rear tires on stock whees to a Dunlop SportMax Q3. Stock size 160/60/ZR17 and 120/60/ZR17.  
Those tires, at least, are very compliant. I can join two beads together with duct tape with a finger and thumb. Another nod to their great “handling”  
The bead braking part was the hardest part, used 2 clamps. After that change in and out was easy and fast. Very surprising. This is my first tire change ever.  
Details coming. Essentially it’s a modified “zip tie” trick. Only I didn’t have zip ties just duct tape laying around.  
Did rear first and for front wheel I’d read it’s harder. It turned out to be about the same.

I was pretty excited about it and stunned it worked so I wanted to share. (This is been on my ‘mechanical’ bucket list for years and years and I always wussed out).  
The actual mounting of new tire was easy and only took 2 minutes!