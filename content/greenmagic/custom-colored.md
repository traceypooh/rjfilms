---
title: 'Custom colored'
date: Tue, 26 Feb 2013 13:21:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/IMG_8615-225x300.jpg
---

Just painted windscreen Dodge “Grabber Green” yesterday and installed it today. 5 light coats of green, 2 coats of clear matte over it all in ten minute intervals.

![IMG_8615](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/IMG_8615-225x300.jpg)[![greenwidnscreen1](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/greenwidnscreen1-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/greenwidnscreen1.jpg) [![green-windscreen3](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/green-windscreen3-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/02/green-windscreen3.jpg)