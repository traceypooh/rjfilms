---
title: 'Replacing clutch, stator cover crack fix, synthetic oil'
date: Fri, 26 Sep 2014 17:35:00 +0000
author: rj
draft: false
tags: ['repair']
---

My clutch slipped when I got the bike at 43K. I adjusted the cable all the way OUT, and that finally made the clutch slipping over 6K revs stop.  
However, if you checked out my Yosemite report, you know the clutch release rod fell out close to home on my return after 530 miles on the road. This I verified today was from the clutch being worn beyond service limits. The friction plates must be about 3mm thick and my last inner plate was down to 2mm. With 7 friction plates and 6 steel plates, a little here and there makes a huge difference.  
In fact, clutching has always been, well, a drag on this bike. And got worse. It takes lever finesse to launch the bike and especially at higher revs, it’s very easy to botch a shift. Takes a lot of focus.  
I am in great hopes the new clutch solves this issue, I know it will fix the rod slipping out.

Had originally planned to just do the clutch and tip the bike hard on the left, but eventually decided to deal with stator too, and the oil and filter were changed not 2000 but 2500 miles ago and I drove through 8 miles of fine dusty dirt so might as well drain it all and switch over to synthetic Shell Rotella T6 5w-40 diesel oil now with a better filter than I’ve been using, a Purolator Pure One PL14610. Have to go get those parts too. This will make doing the clutch neater, since the oil won’t gush out of the cover too.

Getting a number of my to do things done! Especially the cracked stator cover. Was cracked ever so slightly as purchased, and has leaked a little bit of oil continuously for two years. Very aggravating, especially on my new Sidi boots! Makes a mess on trips over 30 miles. Also helps the rear end of my bike look awful, oils it up and grabs all the dirt it can on the road.

Unfortunately, I have to wait for parts from cross country, so in the meantime I decided to dig into my stator crack issue, and my CLUNK on startup creepy issue. The CLUNK no start has become more frequent. And some screws in the washing machine noise behind the stator cover too a couple times while trying to start.  
I expected to find broken starter gear teeth under my cover, but the large started gear, the idle gear, the starter small spline gear, and the other gear all look fine. Don’t look excessively worn, and no broken teeth to be seen.  
It’s often the case the starter one way clutch is bad, but it seems ok, in that the starter will only spin one way and lock up right away from spinning back as it should.  
So am scratching my head here as to what the super CLUNK cause is.

The hardest part of the clutch job and the stator cover removal is simply removing the old gasket material with a razor blade. Quite time consuming. Just the clutch replacement on its own is a simple breeze of a job, could be done quite quickly in a couple of hours.