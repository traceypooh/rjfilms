---
title: 'New Clutch in! Stator cover fixed, new paint 49,469 miles'
date: Mon, 13 Oct 2014 11:53:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/10/IMG_7877-426x320.jpg
tags: ['New clutch', 'repair', 'repair', 'Stator cover']
---

Finished the bike re-build yesterday in a few hours! Was pretty easy, everything went smooth.  
I had to wait until now, a day later, to test fire it.  
A few spots of Permatex RTV at critical gasket areas, and it need 24 hrs for full cure. This time I want no mistakes… no leaks.  
So… on to that test… was quite nervous I might have botched something somewhere or clutch wouldn’t work.  
But I got it running and it sounds quieter at all revs, noticed it right away when it fired at 3500 rpms. It idles smoother. I know I must be crazy but I know Green Magic pretty well at this point too, so it seems very true.  
Might be due to switching to 5W-40 Shell Rotella T6 synthetic oil. Only been running dino before. New better filer installed too, a Purolator Pure One 14610 unit that is better filtering than most of what’s out there.

Clutch IS working. And I have full cable adjustability back. In fact, near too much the other way, lol. Love it.  
Warmed up bike on center stand for 10 minutes only so far to ‘set’ that RTV sealant, and check for any leaks and clutch operation.  
Even though it wasn’t a huge repair, I was still nervous hitting that starter button. Going for ride later today after a little fairing clean up and reinstall of left mid and lowers.  
[![IMG_7877](https://rjfilms.com/greenmagic/wp-content/uploads/2014/10/IMG_7877-426x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/10/IMG_7877.jpg)

Before:  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image.jpg)

After:  
[![IMG_7934](https://rjfilms.com/greenmagic/wp-content/uploads/2014/10/IMG_7934.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/10/IMG_7934.jpg)