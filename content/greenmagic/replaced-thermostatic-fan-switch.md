---
title: 'Replaced thermostatic fan switch'
date: Thu, 27 Aug 2015 13:49:00 +0000
author: rj
draft: false
tags: ['repair']
---

mileage: 58241  
My radiator fan has not come on in last 2 weeks or so that I know of. Even in hot weather stuck at light after light – I have to avoid those. My temps can go past the halfway which is far too hot. The bike starts to run a little stumbly and the clutch operates worse. Everything feels a bit ‘rubbery’ – I can tell when the heat is getting too much. It probably doesn’t help that my left ignition coil is not in good shape. I got the clutch cover, switch and the 2 coils with wires and boots from the old z6xe.com forum member from North Carolina for a nice price.

I have to go a constant 35 mph to maintain just below half on the gauge, it’s been pretty stressful commuting 10 days to work with stopped traffic.

Hope this solves the issue. I had pulled the wires about a week ago, and shorted them and the fan came on right away, so this is the test to see if the switch has failed or it’s another electrical issue in the circuit.