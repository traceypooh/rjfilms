---
title: 'How much stuff can you fit onto a ZX6E motorcycle? “Anti-Tour” baggage lugged'
date: Fri, 13 Jun 2014 17:53:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_1-e1404568871573.jpg
---

About to start loading up bike to its most laden down ever test. It’s not a test actually, I have to get all this stuff there!  
The “anti tour” I thought of calling it because I’m riding the bike and will be gone 4 days and 4 night, however, I’m going only about 100 miles of most of it bland riding to one location and staying there whole time.  
I will be mounting back up my experimental V2 rear DIY rack for this one, my tank bag, saddlebags, rear seat loaded with gear too.  
Have to carry:  
Tent, sleeping bag, therma rest, tarp, pillows.  
Clothes  
Spare hiking boots, running shoes, flips  
Tripod yes a damn tripod  
Rental large camera  
Wireless mic set  
17″ full size heavy old laptop for media transfers  
Etc etc…

Well try to post a shot of bike loaded before I leave today. Is there some “easy” way to mount saddle bags I’m missing? Sheesh.  
I think I’m going to need a rudder to steer this barge…

[![AntiTour Green Magic ZX6e_1](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_1-e1404568871573.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_1-e1404568871573.jpg)

And loaded almost and about to leave.

Yup it ridiculous!

[![AntiTour Green Magic ZX6e_2](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_2-e1404568911417.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_2-e1404568911417.jpg)

The trip was great, I got be in the mayhem of crazy custom trucks racing through the mountains and obstacles for four days.  
I also thought it might be a good idea to add even more cargo for the trip home:  
18 beers (now in black duffle bag)  
3 event t shirts  
Large sunblock, two bags of coffee, extra food, 3 waters, fruit and Brie cheese.  
[![AntiTour Green Magic ZX6e_3](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_3.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_3.jpg)

Returning, Friday with rush hour looming and getting worse at 3pm, I took a longer fantastic back way route through Niles Canyon, Palomares Canyon, and Pinehurst – all regular riding spots for me. About 80+ turns. Can’t say I “hammered” turns with all this stuff on it, more like sloppy joed my way through the turns.  
I bottomed the shock out twice, once hard, so I guess I am getting a feel for what being well over two hundred pounds on the stock shock must be like for others – crap. This trip it really wallowed in corners etc…

But the E was great other than that.

On the way there I rode 80 miles straight with the new bar risers and didn’t have any real wrist pain. Tried to go quickly to escape rush hour traffic and drove out of area a little to escape the awful 880 but went inland over a pass and hit 100 degrees in Pleasanton area. Yuck. I could only sort of try to not roast over 50-55 mph or more. I opened up my visor there to cool and at speed felt like a hair drier in my face set on medium. Was actually cooler to close visor.  
One stop and then the rest of the 35 miles or so to get to camp area before dark and hoped to make dinner.  
Free beer every night after work, campfires and worked with a buddy from a few years before, so fun trip.

Have a lot more confidence to solo camp now since i can cut my gear probably in half and know i can easily haul it with the setup I have.

[![AntiTour Green Magic ZX6e_4](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_4.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_4.jpg)

Campsite was very dusty. Tried to keep fine dust out of ram tubes with tarp parked for 4 days. Large area but shared with 12 truck teams, their mechanics, entourage and welding at night. One team broke their transmission and had a new one in by next morning. Both country and western seemed to dominate the airwaves.  
Many trucks flipped some 2x in same spot, a few engine fires, hydro locked motors in window high water holes, drive shaft fell off one truck in pits, broken parts, etc etc…

[![AntiTour Green Magic ZX6e_6](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_6.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_6.jpg)

That black duffle kept creeping into my back on way down, very annoying. Cramming into tank isn’t usually fun but when 80 lb lump is doing it even worse.  
[![AntiTour Green Magic ZX6e_5](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_5.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/AntiTour-Green-Magic-ZX6e_5.jpg)