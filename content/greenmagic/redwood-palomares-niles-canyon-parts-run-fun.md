---
title: 'Redwood > Palomares > Niles Canyon parts run fun.'
date: Wed, 10 Jul 2013 15:37:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/07/GreenMagicM-on-Niles-Canyon-RR-e1373582740989-150x150.jpg
---

[![GreenMagicM on Niles Canyon RR](https://rjfilms.com/greenmagic/wp-content/uploads/2013/07/GreenMagicM-on-Niles-Canyon-RR-e1373582740989-150x150.jpg)](https://rjfilms.com//blog/greenmagic/redwood-palomares-niles-canyon-parts-run-fun/greenmagicm-on-niles-canyon-rr/)

[![GM funny rr model](https://rjfilms.com/greenmagic/wp-content/uploads/2013/07/GM-funny-rr-model-150x150.jpg)](https://rjfilms.com//blog/greenmagic/redwood-palomares-niles-canyon-parts-run-fun/gm-funny-rr-model/)

[![SunolGM](https://rjfilms.com/greenmagic/wp-content/uploads/2013/07/SunolGM-150x150.jpg)](https://rjfilms.com//blog/greenmagic/redwood-palomares-niles-canyon-parts-run-fun/sunolgm/)

  

off a parted ’97 with 25K picked up:

\-‘W’ lower front fairing

\-front left blinker (p.o. lowsided on top of it)

\-Charcoal canister (emissions – mine’s toast)

\-Full belly pan fairing

– ram air guards

all for 54

Beautiful, hot day. Great ride on those roads to back way to Fremont. Joaquin Miller to Redwood to Palomares Canyon Rd to Nile Canyon. I went back through cute little Sunol. End 44,292 mileage.