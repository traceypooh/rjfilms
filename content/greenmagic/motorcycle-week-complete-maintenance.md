---
title: '“motorcycle week” complete. Maintenance'
date: Sat, 02 May 2015 20:00:00 +0000
author: rj
draft: false
tags: ['repair']
---

mileage 54,860  
I swapped out both tires to a second new set of Dunlop Q3 tires. I really liked these tires last year. When they are fresh there is nothing like them.  
I also upgraded my front brake system. I did the full CBR Nissin caliper swap.  
It’s well documented on the http://www.zzr-international.co.uk site.  
I opted to drill out and tap the Nissin calipers themselves, so I jumped up the bolt in size from 8mm stock Honda size to the stock Kawi size: 10mm 1.25 pitch bolts.  
This enables me to use the stock Kawi bolts and easily switch back and forth if there is any trouble.  
I used the included used pads – for now. I did a full brake fluid change, flush and bleed with DOT 4 synthetic fluid. I also did my 2nd change and flush of the ZX10R rear caliper while doing the front.  
I happened to have the left tail fairing off, and for some reason decided to pull the regulator / rectifier connector. The wiring was frying out again there, 2 of 3 stator yellow wires were crispy and cracking, as was the white wire. I chopped out the bad bits and re crimped on connectors and plugged it back in.  
I also pulled the rear stock shock to size up the feasibility of trying to adapt my ’84 Honda Interceptor VF750F based Fox Twin clicker.  
Stock shock back in, brakes fabricated, tires on, wheels back on, and fairings back on.

Result: I had to cram in all the final brake assembly, bleeding and fairing reassembly as the place I was house sitting and working the owner came back a day early. As a result, I spilled quite a bit of brake fluid around, and when I drove off that night at 11pm the front brakes were not functioning wonderfully. Inspecting it later the next day after driving 50 miles, I noticed some slimy stuff on the rotors – looks like brake fluid and dirt shoved to the edges of the rotors.  
The brake lever feel was much improved, but again, with old used pads (with a lot of meat on them), and the gunk on the rotors, so far the braking is so-so. Maybe a slight down grade in performance.  
But verifying again a few days later, I don’t it’s the rotor’s fault at all. In fact, when I pulled the old pads to check everything all the caliper pistons looked nice and neatly pressed in from where I had forced them back with my hands while installing. Everything looks good with the calipers. I just cleaned the gunk off, made sure to dry off all traces of brake fluid, but now I need to wait to test them again as I am pretty sure my water pump is toast.