---
title: 'Still wrenching… Shock thoughts'
date: Mon, 27 Apr 2015 10:49:00 +0000
author: rj
draft: false
---

Can’t get the rear tire bead to seat. what a shock. Was a major p.i.t.a. last year. Just a little bit left on one side won’t set at 55+ psi, in the sun for hours, in a hot car, and bouncing the sh&t out of it.  
Also, mt regulator / rectifier wiring hook ups I happened to check yesterday… and 2 of 3 yellow stator wires were fried again. Smoked, charred, blistered insulation, and oxidized green gunk all over the connector. ugh, again, after two years – I’ve seen this movie before. Also this time, the thicker white wire was crisped too and cracked insulation.  
I chopped the 3 yellow wires back another 1.5 inches or so and put on new connectors – I’m at the very limit of the wiring loom length now, so next time this happens I’m going to have to install 3 brand new (heavier – yay!) gauge wires directly to the stator outputs and run them all the way to the R/R and wrap them and tie them off. A future project I can probably put off for a little while…

My Fox Twin Clicker shock is a no go. I knew it wouldn’t go in this time. I pulled the stock shock out for the first time, and boy were those lower dogbone bolts near impossible to break free. It was an ‘event’ when I cracked them with a long cheater bar on my ratchet on the nut and my huge torque wrench on the bolt head.  
Anyway, pulled the stock shock for the first time yesterday – in fact my first time pulling any shock out of a vehicle. So had I a spare shock this would be like replacing one with a new one.  
But I wanted to just size up the Fox and see what bits I need to buy / mod. All the internets research in the world won’t answer these questions and I knew it.  
So, I was completely surprised to see the clevis lower end will in no way shape or form ever fit this bike. That was a ‘shock’. Hadn’t though of that as an issue. And that’s going to make this so much more complicated to make work, i can pretty much forget about finding a 25-30 year old clevis end for this Fox – they stopped supporting their racing shocks ten years ago… and they are so rare now.  
So I don’t know if I have to chop off the stock one and have someone weld it to a thread so I can screw it into the Fox?? I don’t know if this would even be strong enough! that spot takes all the weight and bumps.  
Also, the remote reservoir must have a 90 degree offset on those hose installed – it cannot fit the way it is now. Which means pulling off the hose. Which means releasing the 200 psi nitrogen charge. Which means releasing the oil. Which means – at this point the shock has sat since 1986 – a full re-build of the shock is demanded. So a full rebuild kit, building a specialty tool to pull one thingeee out etc etc… and learning how to do that procedure from all my accumulated internets data.  
At least a new proper clevis will solve clearance issues there, solve the 10mm Fox clevis hole vs. 12mm ZX6e hole problem there, and the clevis was too wide and was going to use spacers, new clevis will solve that too.  
The spring on the Fox is noticeably wider, so I may have to file down part of the swing arm ‘shelf’ cutout thingee to allow clearance. It will fit up there now but I don’t know if it can move or compress and not hit. I also have a free ZX6R spring of about the same length that is slightly narrower – although still a little bit wider than stock spring.  
So I will research the stock ZX6R spring and ideal rider weight setup for this stock spring, and maybe when I rebuild the Fox I will install this spring for a little more clearance.  
Now I also have to solve the top mount issue. The Fox (Honda VFR purposed) shock used a 10mm bolt there, and my bike has a 12mm mount hole, and the widths of the bearing / bushings are different. So I probably need to press out the old heim bearing and install a wider opening bearing and then get the correct spacer / bushing width parts too.

So it’s going to be a lot of effort to adapt this shock to fit, and more money too… Not sure about this all. We will have to see. I know the stock shock is junk and bounces me all over and can’t handle imperfections on turns.