---
title: 'Accessory 12V / cig lighter / USB connection sorted GPS'
date: Wed, 03 Sep 2014 18:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image6.jpg
---

For first time verified my 12V spare leads work, and rigged up wiring disconnect extension with bullet connectors on bike side and spade type connectors for the cigarette lighter access that came mounted in battery tray. Had to remove metal screws as they were oversized and dangerously close to battery negative post.  
Also tossed in a spare cig lighter to USB converter I hand on hand.  
Immediately lit up LED even without key in ignition! I guess it will pull 12V without key so that’s handy but have to keep eye on that to not drain battery when bike off.  
Mocked up the cable routing and GPS mount spot, settled right on the speedo! As my TomTom unit has a built in accurate speedo anyway. Should be an excellent addition for longer road trips and fun on shorter ones too.  
I know a new Nikon kit will have cig battery charger cable which is excellent, and I can borrow the gf iphone cig charger cable as the std. USB plug in to cig lighter USB port won’t work on my phone. Argh, annoying Apple!!  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image6.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image6.jpg)