---
title: '1995 trip Switzerland'
date: Thu, 20 Jul 1995 23:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image-e1443980277797-1024x723.jpg
---

July 1995.  
I rode the ’78 CX500 Honda again from Utrecht to Verbier, Switzerland. It’s about 607 miles one way.  
I rode two days each way and stayed in Verbier for about 5 days of shooting.  
I used about 25 gallons for 1220 miles altogether at about $100-$125 USD. Which got me about 49 mpg for this trip.

Left Holland about noon. Drove through Maastricht, NL; Fameck, France; Frouard, Epinal and stopped in Bensancon, France for the night.  
I had dinner in Epinal France at 8:40pm. Ok onto the funny story for the night.  
My French is, how you say, not very good.  
I try to find this universite hostel in Bensancon. I cannot. I go back a few times to a massive posted map of the town. An old man watches me and watched me return looking lost. He approaches and more or less (as far as I can understand), the hostel is closed for the night and there is nothing at all, no place to stay in town. Why don’t I come along with him and stay at his place?  
He’s probably 4 feet tall and 75 – 80 years old and very nice to help a tall stranger in a strange land on a motorcycle of dubious tags and wearing a zippered, multi pocketed badass stereotypical moto leather jacket.  
So off I go. I gather when I get to his house his wife has passed away and that he’s all alone.  
Very very kind of him to share his home with me for the night.

On to Verbier.  
I arrive pretty road weary and tired and take what might be my worst photo ever for the ski pass for the week, and I love it! I look pretty much like the walking dead with super helmet head and wide de focused eyes. It’s fantastic, I scan my pass and keep copies and send out copies here and there over time.

Verbier and their summer glacier. This is my first trip to it, and I’m kind of excited to see it in person after hearing so much about the snowboard summer camp and seeing all the antics in the videos.

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image-e1443980277797-1024x723.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image-e1443980277797.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image1-e1443980216136-467x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image1-e1443980216136.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image2-e1443980302692-459x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/1995/07/image2-e1443980302692.jpg)

It’s a great week, I share a lodging with a summer cool Aussie Photographer Scott Needham, and Victoria Jealouse is also in our little abode. In case you don’t know who she is, she’s a gorgeous snowboarder from Canada.  
And one other Scandinavian rounds out our little comped lodging, Ami Voutailanen. These guys are basically here to shoot stills with Scott, but they’re fine to shoot video with me too.

I left after 5 days of shooting on the glacier at 6pm on July 19th.  
Gas up at 8:40 pm Agip, Yverdon Les Bains, Switzerland.  
Dinner back at Pontalier, France at 9:30pm.  
Midnight I get into my University room in Bensancon, France.

Next day I gas up in Bayon, France, Woippy, France, and final gas stop at Rosmalen, Netherlands.  
It was a really great trip.