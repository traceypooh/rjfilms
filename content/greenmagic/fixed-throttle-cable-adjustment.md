---
title: 'Fixed throttle cable adjustment.'
date: Thu, 20 Sep 2012 21:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image1-427x320.jpg
---

Less than 1 hr for whole job. Test and cut connectors at R/R.  
I popped two of the pins out of the connector and chopped out parts of two crispy fried wires to the regulator unit.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image1-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image1.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image2-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image2.jpg)

[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image3-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2012/09/image3.jpg)  
Drive to Arrow Glass, Emeryville. Stranded in Emeryville – dead batt again ![:(](https://rjfilms.com/greenmagic/wp-includes/images/smilies/frownie.png) A guy on a R6 stopped and gave me a ride the whole way to glass co. so I could buy glass for TelePrompTer just before they closed.  
Stashed in backpack and walked back the entire way from Albany through entire Emeryville back to Green Magic. Frantic calls go nowhere. I bump start bike on a whim and am able to just barely wheeze it home before it’s totally dead again.  
Damn battery problem isn’t fixed.