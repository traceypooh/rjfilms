---
title: 'Planning a sneaky rear shock upgrade…'
date: Wed, 13 Aug 2014 11:14:00 +0000
author: rj
draft: false
tags: ['Uncategorized']
---

Locating parts, and researching a shock upgrade. If I can get the part(s) and make it work, I’ll have to do some light fabrication to make it fit.  
I’m looking at a needed 12mm bolt in eyelet on the shock to match stock size, but this one may have an eyelet that’s only 10mm, or it may have a ball joint! I am trying to get clarification.  
Kinda biting my nails over here waiting for word back from a remote seller. I really hope I can get my greedy little hands on this part! They are uber rare.  
The clevis side of the shock is wider than stock, but that shouldn’t be a significant problem as I am assuming I can install spacers to even it out. A lot better than being too narrow. I am planning that I’ll likely need to do a full rebuild as well, with new O rings, new oil, and new nitrogen charge.

The killer thing is this shock is actually the correct length which if you know these motorcycles is extremely rare! I’m going back in time on this one…  
Will share my secret weapon if I can get my hands on it. Stay tuned…