---
title: 'Fuel pump tests ok. Checked for spark. Lean.'
date: Fri, 30 Nov 2012 22:00:00 +0000
author: rj
draft: false
---

Worked on bike 2pm – 10pm. Checked fuel pump w C helping. Used wall as barrier. Had to hook up external 12VDC source to removed pump and try pumping fuel from one jar to another. Yikes! Anyway it works just fine – so not the issue. (Start suspecting fuel relay?)  
All 4 plugs show spark (I assume that checks out good – little, oh so little, do I know… See freaking Feb 2013). Checked each plug – they all look lean. My first time ever checking anything for spark correctly. C fired off the starter push button each plug for me too!