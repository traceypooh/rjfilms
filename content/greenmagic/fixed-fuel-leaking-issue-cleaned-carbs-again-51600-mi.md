---
title: 'Fixed fuel leaking issue, cleaned carbs again 51600 mi'
date: Mon, 23 Feb 2015 19:10:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8156-427x320.jpg
---

51600 miles.  
Spent today rebuilding the carbs again. 12:30pm – 7:20pm which included full clean up and re-assembly of donor carb banks too. Not bad, and the fix worked!  
I got a 4 pack of float bowl gaskets after I had cleaned out the carbs two and half years ago. I got them because I developed fuel leaks on two carbs after re-installing the old ‘flatted’ gaskets.  
Unfortunately I could only access 2 of the 4 bowls with the carbs mounted on the bike and only did those. After FINALLY getting Green Magic to both start and run well I did not want to go through pulling off the tank, airbox, and carbs again if there was any way at all to avoid it.

So in those 2.5 years what happened? Well the other two un-replaced older gaskets would dry out and in shrink a bit in cold weather and four times I was greeted with horrible fuel leaks about 4 times at least on a morning start up if the bike sat 2-4 weeks.  
Had a job on Sunday to get to, but the volume of fuel pouring out grounded me and the bike. A fix was a requirement at this point.  
I knew this mechanical harbinger of death was stalking my motor because of this very issue (so to speak).

I had also thought I had a sticking float needle or float needle seat issue as well, and leaking fuel into the motor and the oil. However, the oil has always looked fine, doesnt smell of gas, my fuel level doesnt really go down and my oil level doesnt ever change. Still I had been worried, because I think on occasion Green Magic suffers from hydro locking and doesn’t want to start.

[![IMG_8156](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8156-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8156.jpg)

[![IMG_8155](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8155-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8155.jpg)

[![IMG_8160 annotated choke carb ports](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8160-annotated-choke-carb-ports-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8160-annotated-choke-carb-ports.jpg)

[![IMG_8157](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8157-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8157.jpg)

[![IMG_8167](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8167-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8167.jpg)  
Other mechanical observations:

I took out spark plug #2 and #1. Their appearance after nearly 10,000 miles was excellent. Just what it should be and not wet with fuel etc…  
I also hit the starter button briefly with each plug out to see if fuel would shoot out of the open cylinder hole. None did. Phew!! Here’s spark plug #1 and #2 below as noted with fingers, and their condition after 8000ish miles.  
[![IMG_8147](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8147-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8147.jpg)

[![IMG_8153](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8153-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/02/IMG_8153.jpg)  
I taped up a few nicks in the upper airbox hose with duct tape.

The pilot jets were clear and clean. The main jets were clear as well, as were the emulsion tubes and the float needles and seats. The float bowls were quite clean as well, I was very surprised about that. I was very happy with the condition of the carbs after this amount of time. Also, none of the float needles were stuck or sticking in the seats, they all moved easily and freely.  
I soaked the main jets, the pilot, the emulsion tubes anyway for 45 + minutes in the Evinrude marine tuner product (hardcore carb cleaner).  
Sprayed them all out with brake cleaner too.  
I sprayed out the pilot air jets passageways several times with cleaner and brake cleaner (which weren’t clogged this time around), the main jet air jet holes, and the mixture screw passageways too.

I did notice that what seems to be the air tubes for the choke – the smallest brass holes on the pilot air jet side were all gummed / plugged up. EDIT: Upon reflection and some research these are in fact the ‘starter jet’ or really the choke ‘enricher’ circuit tube. This tube sips up extra fuel and adds it to the mixture for cold starts when the choke slides are pulled and more fuel can pass through the end of these into the Venturis. Their output is the small hole off center just below the butterflies.  
I inserted carefully a twistie tie into these ‘starter jet’ holes and wriggled them around to clean them and shot the carb cleaner and brake cleaner in those holes too. I had not done that before I don’t think as I wasn’t aware it was an narrow opened tube. Hoping that this will make starting easier! GM has been getting a little fussier starting gradually over the past 2.5 years.