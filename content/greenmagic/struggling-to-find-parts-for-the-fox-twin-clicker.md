---
title: 'Struggling to find parts for the Fox Twin Clicker'
date: Thu, 04 Sep 2014 12:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image.jpg
---

Many hours of research sunk into this.

The clevis – lower mount – has 10mm bolt hole openings. Stock E shock bolt to pivot is 12mm so I either need to sleeve E pivot hole down to 10mm or enlarge the Fox clevis end to 12mm

Here’s some more pics. The clevis has a lot of material around it. I’d be willing to bet width was made all the same at factory and holes were bored out depending on final application. I doubt a 12mm hole factory clevis has more width than this. But a guess without confirmation. Shallowest clevis depth as seen is 9mm or 3/8″.  
The measurement of clevis material north – south looking in this pic (ruler measuring other way) way is 1.25″ or 32mm.

Wondering if more sensible to just enlarge hole 1mm all around, so the shallowest end would go to 8mm of material.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image.jpg)  
The fox twin clicker eyelet seems to be 1″ size or 25.4mm. But it could also be 25mm size. It’s hard to know with micrometer gauge on hand.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image1.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/09/image1.jpg)