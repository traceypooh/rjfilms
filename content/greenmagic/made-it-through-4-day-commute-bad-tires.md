---
title: 'Made it through 4 day commute bad tires'
date: Thu, 23 Apr 2015 18:00:00 +0000
author: rj
draft: false
---

Got in a little bonus ride: After work up to Pacifica and then down to Half Moon Bay, a favorite spot of mine.  
I also made my first confirmed kill, as in road kill today. I ran right over the back of a poor squirrel today on Redwood coming back. I did not jam the brakes or swerve and risk a crash on my horrible tires.

That said, I got behind 3 other motos turning up Skyline off 92 to go pee, and I could not resist the temptation. I let them go, and then got on the gas and started hanging off to catch the KTM sport bike with the under tail exhaust. Was an easy job catching this guy, and after about 6-7 turns I had to say to myself “what are you doing, you have bald tires and metal showing on the rear?” “You slid the front the other day up Redwood, what are you doing?” “Stop!” I had to pull over and stop or I’d just keep going. That coulda ended in tears.

Traffic wasn’t horrible as I got out of the job at 2:41pm.  
Planning my first burnout on the last remnants of my rear Q3 tomorrow!