---
title: 'Oil change #2 46,931 miles. Epoxy stator cover.'
date: Sat, 17 May 2014 20:37:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image.jpg
tags: ['JB Weld', 'repair', 'Stator cover']
---

Oil change today and while there finally trying to stop the one drop of oil a day coming out of stator cover on teenie hole from p.o. lowside. Also for some reason riding w newer thinner boots its spraying a bit on them on the hwy which is making a mess and annoying. Didn’t on older lg work boots.

Planned to use JB weld but couldn’t find it. I found some Loctite quickset Epoxy. Two part plunger style kit. Says waterproof tight bond for metals, wood etc…  
Not sure if that can take high temps around motor.

Drained oil and changed filter and will leave empty for a day. Used brake cleaner to clean outer cover and then epoxied hole.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image.jpg)  
Shot of leak area when I got the bike 2 years ago. You can actually see leak in above pic in the oil pool you can see hairline crack on right side coming out of pool.  
[![image](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image1-e1400384465732.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/05/image1-e1400384465732.jpg)  
For those interested, this pic is area cleaned and the crack as it is two years later and 4K. I don’t think its spread like I’ve read it can. It’s center frame on cover where two edges meet and you can see sun lighting up the tip of crack. Man, tiny but what a mess maker over time and miles. Sheesh. Bike is a dirty birdie all cause of this and you can hardly see it with the naked eye. Only under bike incredibly close staring at it.

Hope it bonds tight. Otherwise JB Weld next and then new cover.