---
title: 'Damn I dropped it and cracked it up. Clutch cover +'
date: Mon, 03 Aug 2015 06:10:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2740-1000x750.jpg
---

Aw, dammit I dropped the bike Monday morning before heading to work.  
I fly X Country for work last night with a cold and my left ear pressure went ballistic and it still wouldn’t pop (it’s now a week). Feels like I have a fuzzy basketball in there.

Anyway, no balance, took the bike of the center stand after it would not start (been 5 weeks), and I could tell had no chance of starting with the normal electric start.

Was planning to run it uphill to run it down and bump start it.

The second I took it off the center stand down she goes hard on the concrete parking slab. What I notice at first is the shattered front brake lever. I am now late and panicking as this is the only way to get 38 miles to work for hours and hours. I decide I am going to run it anyway in lane splitting traffic choked traffic out here the whole way with no front brakes.

Everything else looks ok besides a cosmetic scuff on the generator cover.

WHAT I DON’T NOTICE IS THIS:

[![clutch cover cracked zx6e IMG_2740](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2740-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2740.jpg)

I push the bike up the short hill here. Won’t start. not even close. I push it all the way out of the road here to the next bike road with a huge endless hill, and finally get it to bump start on that pretty quickly after panting and sweating up the hill.

Riding without the front brake lever is stressful and weird. At one point I decided to gently scrub some speed at 70 mph and reach for the phantom lever and it’s shocking to not have it there.  
I remember the rest of the way though.

I get to work, side stand it and start to step off to the right and my right boot feel super slippery, it also was on the peg I recall. Damn, that feels like oil. Where did I pick up oil on my boot from?  
Well, it was my bike. I look down and there are pools of oil forming, the whole middle to back right side of Green Magic is covered in oil. The side of the rear tire is too. Not good. (And I am 20 minutes late right now too).

[![clutch cover cracked zx6e IMG_2737](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2737-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2737.jpg)

[![clutch cover cracked zx6e IMG_2739](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2739-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2739.jpg)  
[![clutch cover cracked zx6e IMG_2734](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2734-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/clutch-cover-cracked-zx6e-IMG_2734.jpg)  
I look around quickly and then see the shattered clutch cover that I had not seen before. Shit.  
After work, I somehow decide to gingerly ride it home. I get out early ahead of the crazy traffic and use huge safety margins with cars in front of me and keep the speed fairly slow.  
When I get back the whole rear end is soaked in oil, the tire is completely oil covered, and the rear brake is soaked in oil and basically not braking anymore either. Sheesh. I got lucky there. No more riding until this is fixed.  
[![IMG_3630 GM oil splattered cracked cover](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3630-GM-oil-splattered-cracked-cover-344x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3630-GM-oil-splattered-cracked-cover.jpg)

[![IMG_3634 GM oil splattered cracked cover tire](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3634-GM-oil-splattered-cracked-cover-tire-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3634-GM-oil-splattered-cracked-cover-tire.jpg)

[![IMG_3635 GM oil splattered cracked cover](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3635-GM-oil-splattered-cracked-cover-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3635-GM-oil-splattered-cracked-cover.jpg)

I wipe all the oil off and then remove the rear caliper and pads and dry it all and use brake cleaner on all the brake parts to hopefully remove the oil residues.  
A bad start to a work week, but could have been a lot worse.

——  
Follow up:

I just removed those shattered bits with my fingernail, and used a small hammer to pound / flatten back the circular opening a little bit. it had been deformed a little – there’s a small oil seal there and I could see it would not be able to do it’s job. Just set the JB Weld on it and we’ll see in 24 hours if it hold oil or not.  
[![IMG_3645 picked pieces out clutch cover lores](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3645-picked-pieces-out-clutch-cover-lores-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3645-picked-pieces-out-clutch-cover-lores.jpg)  
[![IMG_3647 JB weld clutch cover lores](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3647-JB-weld-clutch-cover-lores-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3647-JB-weld-clutch-cover-lores.jpg)  
Re-assembled the rear brake and after pumping the lever several times it’s back to braking hard. Good.