---
title: 'Started the water pump repair finally'
date: Wed, 10 Jun 2015 21:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0080-400x300.jpg
tags: ['repair']
---

mileage: 55,318  
After finally getting all the flipping parts – all special order except the bearings – I got down to business.  
I drained all the coolant and pulled the pump out, which all went easily enough. I was cleaning the housing for about an hour when I noticed to my horror there were multiple cracks in the back cover housing!  
So I realized this must be where all my oil leaks had been coming from – not the actual oil seals in the pump.  
I started desperately searching CL and fleabay for any water pump, seized or not.  
I posted on BARF looking for a Kawi pump, and got a quick answer. He pointed out that whole area only contains water and with no residue and water leaking it could not be leaking. He called them “casting errors” which I think is quite generous to Kawasaki but… I realized the guy was right, there was no leak through those cracks and couldn’t be oil coming from there anyway.  
Anyway, after thinking about it all a bunch I elected to simply JB Weld the outside of the case cover and let it sit overnight tonight.  
Tomorrow I will replace both oil seals anyway, and then reinstall it all and test it out.

Originally I was going to do the full rebuild, replacing the two bearings and another seal, but when I pulled the water pump I noticed the rear shaft is very tight and there is zero lateral and back and forth play. It feels rock solid, so at this point there’s no need to get in there to replace the bearing, and perhaps risk not having the right loctite type sealant on hand for the mechanical seal cup.  
[![IMG_0080](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0080-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0080.jpg)

[![IMG_0078](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0078-400x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0078.jpg)

I’ve been riding the bike about 4x very reluctantly to work over the past 8 days. Originally, I was worried my pump was shot and coolant was getting in the oil and my motor could seize. But the oil looks totally normal in the engine, and no coolant has ever come out the weep hole, so I think I’m ok there. I don’t think the oil and coolant ever mixed.  
Although I will say my coolant looked like shit. There was dark small sediment in it, wasn’t a pretty sight. I changed this all out about 2 years ago and it’s gone south quick.

Here’s the nasty coolant I pulled out of the machine – the worst bit of it. I swapped the coolant and did a flush about 2 years ago, and it already had all this sediment in it. Yuck.

[![IMG_0056](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0056-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0056.jpg)

[![IMG_0061](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0061-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/IMG_0061.jpg)