---
title: 'White pain tank ding, JB Weld clutch cover'
date: Sat, 08 Aug 2015 18:40:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2758-427x320.jpg
tags: ['repair']
---

Dropped the damn bike on Monday. Finally had time to look at it and decided it was worth trying to JB Weld it and see if it could hold up.

ALSO!: Finally, ground down the original p.o. gas tank damage where the handlebars bent and slapped into the tank. Then sprayed it white, several thin coats, maybe 6 or so.

Yesterday I managed to score a white spray paint can, moto chain lube, and rustoleum clear spray all for free at the Alameda County Hazardous waste free cycle hut! Was pretty stoked! The tank looks SO much better now!  
[![White paint gas tank ding IMG_2758](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2758-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2758.jpg)

[![White paint gas tank ding IMG_2757](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2757-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2757.jpg)  
[![White paint gas tank ding IMG_2761](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2761-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2761.jpg)

[![White paint gas tank ding IMG_2760](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2760-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2760.jpg)

[![White paint gas tank ding IMG_2759](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2759-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/White-paint-gas-tank-ding-IMG_2759.jpg)

[![Clutch cover crack JB Weld IMG_2755](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/Clutch-cover-crack-JB-Weld-IMG_2755-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/Clutch-cover-crack-JB-Weld-IMG_2755.jpg)

[![IMG_3645 picked pieces out clutch cover lores](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3645-picked-pieces-out-clutch-cover-lores-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3645-picked-pieces-out-clutch-cover-lores.jpg)

[![IMG_3647 JB weld clutch cover lores](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3647-JB-weld-clutch-cover-lores-428x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/IMG_3647-JB-weld-clutch-cover-lores.jpg)