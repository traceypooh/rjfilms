---
title: 'Bought 4 into1 titaninium Kawasaki exhaust last night'
date: Thu, 17 Oct 2013 10:55:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/10/00i0i_eXs0mvDCJK_600x450-300x224.jpg
tags: ['Touring']
---

Worked in Napa and went to check out a used ‘ZX6’ stock Kawasaki titanium exhaust system. Bought the whole system for $55 which is a steal, however, this is not a ZX6 exhaust as far as I know. It looks to be a ZX7R exhaust and don’t know if I can make it fit. But stupid cheap price compelled me. My stock duals are heavy, duals, and quietly boring. Ends are rashed too from previous owners. So I’ve been interested to get rid of the stockers…  
The header on pipe says “KHI M 048” which means Kawasaki Heavy Industries. The can says  
“Model specific code: Kaw3700900”  
“Serial number: 7 J 1”  
[![00i0i_eXs0mvDCJK_600x450](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/10/00i0i_eXs0mvDCJK_600x450-300x224.jpg)](https://rjfilms.com/blog/greenmagic/wp-content/uploads/2013/10/00i0i_eXs0mvDCJK_600x450.jpg)