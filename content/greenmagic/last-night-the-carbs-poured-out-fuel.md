---
title: 'Last night the carbs poured out fuel'
date: Sun, 19 Apr 2015 15:54:00 +0000
author: rj
draft: false
---

Last night the carbs poured out fuel when I started bike. It took awhile to start too. This is the 2nd time this has happened after I “fixed” the leak problem by replacing the last 2 old worn float bowl gaskets.  
Obviously the O rings in the float needle seats are shot, or the float needles are sticking but I say, no, they are not sticking and have not been the problem.  
I also have my floats set at 13mm which might be quite high, so next time in there, will lower that height to 10-11mm range.  
I have to change the tires pronto too, (ugh), since they are very worn at 8100 and bald on the rear with some metal in a spot showing.