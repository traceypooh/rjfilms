---
title: 'Ride after work – checking property and joyride'
date: Wed, 09 Nov 2016 19:01:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2017/06/gmm-Nov-8-2016-ride-skyline-IMG_7035-1024x663.jpg
---

Riding after work. Looped out towards Half Moon Bay and 92 up to Skyline south.  
Looked at a few pieces of land for sale. Looked at an interesting spot in La Honda.  
I really like that out of the way location.  
[![gmm Nov 8 2016 ride skyline IMG_7035](https://rjfilms.com/greenmagic/wp-content/uploads/2017/06/gmm-Nov-8-2016-ride-skyline-IMG_7035-1024x663.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2017/06/gmm-Nov-8-2016-ride-skyline-IMG_7035.jpg)