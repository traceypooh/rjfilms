---
title: '5th oil change done 58,321'
date: Sun, 06 Sep 2015 22:03:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/oil-change5-IMG_3078-427x320.jpg
---

3rd round with the Purolator Pure One PL14610 filter and 5w40 Shell Rotella T6 synthetic diesel oil.  
I went past due on this change. Was relieved to finally get it done.  
Note the CB400A still being worked on in background by me.  
[![oil change5 IMG_3078](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/oil-change5-IMG_3078-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/10/oil-change5-IMG_3078.jpg)