---
title: 'Midweek commute thoughts…'
date: Wed, 09 Dec 2015 08:43:00 +0000
author: rj
draft: false
---

Yesterday riding in I was thinking about how thrilled I am riding. I just love riding the motorcycle.  
I am also very psyched about the upcoming planned moto trip to Vietnam! – After doing more research (at work) about the route in Vietnam to ride Mon and Tues.  
This morning I moved into the decent flowing slow lane on the bridge to enjoy the serene water. The bay just kind of melted into the cloudy land in the distance. I saw a little duck take off and fly next to me right by the bridge in the calm from any cross breezes. I outpaced him at 45 mph but not all that quickly.  
Another duck took off and flew not far from me and the bridge in our line of travel and outpaced me easily at 35mph! Was pretty cool.

Monday night I spent about an hour and a half tending to my chain. It’s not in good shape. I got a few hard kinks out of it with silicon spray lube, but one remains. And I found a very stretched link – not good. I spun the wheel a lot adjusting the chain tension for multiple points. It’s too slack in a spot and almost all the way tight in a spot and this is after riding and the chain is warm too!  
Tuesday I could hear the chain pulsing on the wheel revolutions, not reassuring. The tight spot makes the opposite side too slack I think and then it its too loose around the rear sprocket teeth. That parts clacks. And the tight spot throbs when riding. It’s unsettling, and has me quite worried.

I cannot get parts and have time to replace them this week, so it will have to be a 2016 job… I just need the chain to hold on for the week and maybe a little longer. My rear tire is quite worn down too, well past the wear bars and it’s only been about 8 months since I installed them new!