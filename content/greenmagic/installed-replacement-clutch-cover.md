---
title: 'Installed replacement clutch cover'
date: Sun, 30 Aug 2015 13:34:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0277-337x300.jpg
---

Last night I cleaned, sanded and rubbing alcoholed the used clutch cover that I got in last week.  
It was a poorly painted black that was orange peeled and chipped paint. A mess and filthy inside the cover as well. But it looks solid.  
The clutch cover that I fixed with JB Weld has a greater leak out the clutch release rod hole than I thought.  
After removing the lowers for the fan switch job last Thursday it was easier to see much more oil than I had thought was coming up. Still nothing major, and the JB Weld job cut down 99% of the leak and got me thru two weeks of heavy use so I’m grateful.

I also removed the clutch lever and bent it back to it’s original shape (prior owner left low side damage I never repaired), and I painted it green, and sanded down the pitted and rusty lever adjuster and painted that green as well. Also, the bar end I scraped down to bare metal I sanded lightly and sprayed that again too.

Installed everything leisurely before lunch in a couple hours. I will test it either today or by Wednesday at latest for leaks and to see if the rad fan will come on.  
[![IMG_0277](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0277-337x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_0277.jpg)

[![IMG_3039](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3039-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3039.jpg)

[![IMG_3040](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3040-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/08/IMG_3040.jpg)