---
title: 'My first ZX6e!! Bought last night 9pm! Pics, Fell in love on the ride home.'
date: Fri, 24 Aug 2012 12:19:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8612-300x225.jpg
tags: ['Uncategorized']
---

Just bought this bike for $900.00 cash last night. Runs a little rough, perhaps a weary clutch too. 43,174 miles on it. Some damage from PO low siding it in a turn.  
So psyched to get this bike. You might guess I like Kawasaki Green.  
PO fueled with 87 octane. And fuel present in tank was 8 month old 87 octane. Hard to start, sort of rough idle (big surprise). Bad. I put in 3.5 gal of 91 Octane down the street from purchase and that already made it ride better. Starts not great, had to bump start it three times to get it to run after sitting 5 days by previous owner for my test ride. Electric started once after a stall. Then had to bump start it again at gas station.  
Might try the SeaFoam stuff. Does that work well?  
This bike destroys my Katana 600 I owned forever. I love it.

![IMG_8612](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8612-300x225.jpg)[![IMG_8615](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_86151-224x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_86151.jpg)[![IMG_8609](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8609-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8609.jpg)[![IMG_8616](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8616-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8616.jpg) ![IMG_8617](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8617-300x225.jpg)[![IMG_8607](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8607-224x300.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8607.jpg)![IMG_8610](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8610-229x300.jpg)![IMG_8614](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8614-300x225.jpg)[![IMG_8608](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8608-300x225.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8608.jpg)![IMG_8618](https://rjfilms.com/greenmagic/wp-content/uploads/2013/02/IMG_8618-300x225.jpg)