---
title: '221 mile ride along Rt 1. Bodega Bay. Stinson. Bought 1″ bar risers!'
date: Sat, 31 May 2014 21:30:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8948-1024x768.jpg
tags: ['Touring']
---

[![GM Bodega 221 miles_8948](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8948-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8948.jpg)  
Left about 11:30 am (late!). Back by 9:30pm. Long ride.  
Ultimate goal was to get to Santa Rosa but via scenic beautiful windy coastal roads. Spotted a set of custom machined 1″ bar risers online and the sellers were riding from Fort Bragg to SR for parts buying ride. So I felt I had to go get these parts!  
Rode into SF and over Golden Gate. Was going to stop at overlook on Marin but crammed with tourists so kept going. First stop on beautiful Rt 1 to Stinson Beach on beautiful overlook of ocean on cliffs. Brought a slice of pizza I made night before chicken jalapeño sausage, crimini mushroom, cheddar cheese, red onion and little chipotle special sauce C made. Yum! Had. Nice chat with German bicyclist there.

On to Stinson, sunny pretty overview into Stinson.  
[![GM Bodega 221 miles_8944](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8944-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8944.jpg)[![GM Bodega 221 miles_8949](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8949-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8949.jpg)  
Such a beautiful peaceful ride along the salt water lagoon / inlet out of Stinson.  
Another quick stop stretch and p stop at Tomales Bay. And onward![![GM Bodega 221 miles_8969](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8969-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8969.jpg)  
Drove up the whole side of Tomales Bay onto Ford road and took a lot of turns to get to Santa Rosa including a bad bumpy narrow farmer road that was beating the crap out of my back as the bike was bucking all over. In fact, the suspension doesn’t feel the same after that stretch. Worse and bumpier. Sebastopol was a really cute downtown and very old and rustic. Scared away a bunch of turkey vultures on a deer carcass near town. Park outside of cute Sebastopol:[![GM Bodega 221 miles_8983](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_89831-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_89831.jpg)  
Met [LilRedRidingLiz](http://lilredridingliz.com/ "LilRedRidingLiz"), Liz and her riding partner Tony at cycle shop and swapped some stories and bought the risers! He’s got a crazy modded 89 ZX10 with Ducati tail etc… Very nice people.  
Tony and I went to cycle salvage biz to look for parts and I got some wheel weights to balance my rear tire finally!  
We then had some grub and I got a coffee at Denny’s Santa Rosa before we all started the long way back to our homes.  
I went back hwy 12 straight to Bodega and Bodega Bay. The little town of Bodega was so cute! Historic and very compact and charming. Short stop there. No gas. [![GM Bodega 221 miles_8984](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8984-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8984.jpg)  
On to Bodega Bay and gassed up finally, was getting close as last fuel was far side of Sebastapol on this leg.  
Drove down to the docks and hit some nice corners in town. Very quaint. [![GM Bodega 221 miles_8991](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8991-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8991.jpg)  
And then surprise gem discovery of Doran Park. What a park!! I can’t wait to go back there and camp for a night. So pretty and peaceful. Drove to very end with a big fishing boat leaving the channel for open ocean.[![GM Bodega 221 miles_9007](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9007-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9007.jpg) Starting to get cold and losing light fast. Knew I was in for a tough ride back but I’ve never been here and so pretty!

[![GM Bodega 221 miles_9005](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9005-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9005.jpg)  
Weary and freezing cold last several hours along coast.  
The ride back route 1 to Nicasio Valley road around the beautiful lake and then around to Lucas Valley Road was bone chilling cold. [![GM Bodega 221 miles_9040](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9040-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9040.jpg)

[![GM Bodega 221 miles_9044](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9044-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_9044.jpg)Stupidly I only wore my AlpineStars summer vented leather jacket and T shirt, no long sleeve, no extra layers. Nose running constantly, shivering, eventually so tired and dazed I was was becoming a spaced rider. My brain could not focus anymore. My back had been in pain since that damn farmer road and wrists in a lot of pain as they usually are from that much saddle time.  
Anyway, finally and happily made it back and warmed up and got some thai takeout.  
A great road trip!

Even the gas station had a view in Bodega Bay!  
[![GM Bodega 221 miles_8987](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8987-1024x768.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2014/07/GM-Bodega-221-miles_8987.jpg)