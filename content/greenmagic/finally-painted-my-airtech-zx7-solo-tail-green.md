---
title: 'Finally painted my AirTech ZX7 solo tail green!'
date: Fri, 27 Nov 2015 18:07:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3529-563x750.jpg
---

It’s been a long while, and I don’t have my own place really to do it, nothing ideal… but I finally got an opening to do it in a friend’s garage.

I also got a chance to finally rivet in the exhaust end caps. I had removed them to gut / debaffle the exhaust. Which is still the best custom thing I’ve done to the bike so far I think.

![airtech race tail paint IMG_3529](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3529-563x750.jpg)  
![airtech race tail paint IMG_3535](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3535-563x750.jpg)  
![airtech race tail paint IMG_3536](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3536-563x750.jpg)  
![airtech race tail paint IMG_3553](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3553-563x750.jpg)  
![airtech race tail paint IMG_3554](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/airtech-race-tail-paint-IMG_3554-563x750.jpg)

Here’s some pics of Kidsaki’s amazing looking ZX6E he customized years back…

Basically the look I’m going for:

![kidsaki Airtech tail paint job top GOOD](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/kidsaki-Airtech-tail-paint-job-top-GOOD-480x320.jpg)

![kidsaki airtech zx7 tail on E](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/kidsaki-airtech-zx7-tail-on-E.jpg)

Here’s some pics of the tail assembled before it was painted. Notice the custom chop into the stock seat plastic spine and the custom metal brackets. I am almost 100% certain that the legendary Bobl from zx6e.net fabricated this seat and used it for awhile before selling it to Dave1 who then sold it to me.  
![ZX7-Tail-swap-d1_9315](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/ZX7-Tail-swap-d1_9315-1024x671.jpg)

![ZX7 Tail swap d1_9285](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/ZX7-Tail-swap-d1_9285-427x320.jpg)

![ZX7 Tail swap d1_9283](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/ZX7-Tail-swap-d1_9283-427x320.jpg)

![ZX7 Tail swap d1_9282](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/ZX7-Tail-swap-d1_9282-427x320.jpg)

![ZX7 Tail swap d1_9281](https://rjfilms.com/greenmagic/wp-content/uploads/2016/05/ZX7-Tail-swap-d1_9281-427x320.jpg)