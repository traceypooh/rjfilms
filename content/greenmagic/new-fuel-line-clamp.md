---
title: 'New fuel line clamp'
date: Tue, 26 Feb 2013 17:21:00 +0000
author: rj
draft: false
---

Installed fuel line clamp at the carburetors. That’s a relief. Had been driving without one.