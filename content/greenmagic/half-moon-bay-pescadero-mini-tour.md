---
title: 'Half Moon Bay Pescadero mini tour'
date: Mon, 22 Feb 2016 20:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4312-1000x750.jpg
tags: ['repair', 'Uncategorized']
---

Had a short day at work and got to blaze out the door with Green Magic!  
It was a beautiful day.  
[![IMG_4312](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4312-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4312.jpg)

[![IMG_4313](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4313-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4313.jpg)  
Creepy abandoned or semi abandoned cabins found on creepy old dirt road.

[![IMG_4304](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4304-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4304.jpg)

[![IMG_4309](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4309-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4309.jpg)

[![IMG_4308](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4308-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4308.jpg)

[![IMG_4289](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4289-1000x750.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4289.jpg)

[![IMG_4281](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4281-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4281.jpg)

[![IMG_4303](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4303-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4303.jpg)

[![IMG_4299](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4299-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4299.jpg)

[![IMG_4298](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4298-e1472250045418-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4298-e1472250045418.jpg)

[![IMG_4294](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4294-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4294.jpg)

[![IMG_4292](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4292-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4292.jpg)

[![IMG_4288](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4288-e1472250034500-240x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4288-e1472250034500.jpg)

[![IMG_4286](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4286-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4286.jpg)

[![IMG_4284](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4284-427x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/08/IMG_4284.jpg)