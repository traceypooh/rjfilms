---
title: 'Carson Pass, Lake Tahoe, Hwy. 50 Sac overnight, Berryessa, Napa'
date: Tue, 30 Jun 2015 23:00:00 +0000
author: rj
feature_image: https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0601-copy-479x320.jpg
tags: ['Touring']
---

[![DSC_0601 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0601-copy-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0601-copy.jpg)  
[![gmm overhead Sierras Tahoe 80 50](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/gmm-overhead-Sierras-Tahoe-80-50-1024x576.png)](https://rjfilms.com/greenmagic/wp-content/uploads/2016/07/gmm-overhead-Sierras-Tahoe-80-50.png)  
Had a client ask me to work in Sacramento and got me an overnight hotel so I thought I would squeeze in some good riding in the Sierras and the gold mine towns.  
Day 1: 400 miles. (399.5 to be exact). Oak 4 to Sac, to 49 Jackson, 88 to Carson Pass, on to Lake Tahoe. 50 back.  
Day 2: Sacramento to Davis and then small highway to Lake Berryessa, and on to Napa Valley. Up and Down Bar in Point Richmond for a beer at the end and back to Oakland.

First Day:  
Route to get to Sac via 4 and the 5. I checked in and dumped all my clothes and work gear and headed out. The heat was scorching and not fun. Was still pretty roasting miserable even to Jackson, CA an old gold rush town. Jackson had some very cool large mining structures just outside of downtown on the famous 49.  
[![DSC_0574](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0574-1024x684.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0574.jpg)  
Tried to find a nice place for a very late lunch around 5pm, but most of the restaurants were closed on Monday. Had to settle for a truck burrito which was decent.  
On the way up the 88 Carson Pass I was still boiling and craving me some cheap ice tea sugary drink, so I gave in to the bad urge at this little stop and met a few friendly locals:

This was highway 88 towards Carson Pass and Kirkwood.  
[![DSC_0577](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0577-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0577.jpg)  
A nice stop on the 88:  
[![DSC_0597](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0597-1024x684.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0597.jpg)  
By now the extreme heat had finally started to break and let up. But only just barely. I turned into a sweaty mess at the Kirkwood resort road taking pictures in my leathers. That was a bad idea. A paving delay I had to stop for there and was so hot I had to strip off the helmet and leathers to almost breath.  
[![DSC_0670](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0670-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0670.jpg)  
I had never done the next section in nicer weather and it was much more beautiful than I remembered during my snowboard video years.  
[![DSC_0646](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0646-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0646.jpg)  
[![DSC_0651 copy](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0651-copy-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0651-copy.jpg)  
I hit the 3 way intersection and headed towards Hwy 50 and Lake Tahoe.  
[![DSC_0681](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0681-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0681.jpg)

[![DSC_0685](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0685-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0685.jpg)  
Some heavy rains had just pounded through here and everything was a startling green and some very wild misty vapor rising off the road backlit with the sun was pretty otherworldly.  
I pushed on to old stomping grounds: South Lake Tahoe. I really knew I should be heading back towards Sacramento with the coming darkness but I couldn’t resist blowing right by Hwy 50 and cruising the soaking roads into Lake Tahoe itself. I raced down a private Marina road to try to see the Lake quickest, and all I got to see was this sorry bit of water. Cue the letdown.

Oh yeah, and there was this rainbow thingee rising out of the Lake too:  
[![DSC_0698](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0698-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0698.jpg)

Ok, so you are let down too and want to see the actual Lake? Ok ok, I turned around and took the other T branch and went for it until the end to a very private property area but snapped some picks anyway after parking illegally there:  
[![DSC_0706](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0706-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/06/DSC_0706.jpg)

I back tracked south back to the Hwy 50 turn and headed up the 50. It was much more beautiful than I remembered, and I used to drive that road a fair amount back then too:  
[![DSC_0757](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0757-480x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0757.jpg)

[![DSC_0776](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0776-480x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0776.jpg)

[![DSC_0745](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0745-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0745.jpg)

[![DSC_0740](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0740-479x320.jpg)](https://rjfilms.com/greenmagic/wp-content/uploads/2015/07/DSC_0740.jpg)

Made it back to hotel quite late. Hit up In N Out burger for a late dinner. I was pretty fried by that point (so I guess fries and burger made sense).  
Spent a nice quiet night at the hotel and left in scorching heat late morning back towards the Bay.  
I rode out towards Davis and then headed off the super slab to take in some pretty (hot) farmland and some vineyards eventually on my way to Lake Berryessa. I’ve never seen this Lake, just read about it. It was pretty cool! Some great fast roads on the southern tip and rode to the northern tip.  
I sat in the shade at the top of the Lake for awhile to cool off and stripped off all my clothes above the waist. Could almost manage to be cool there in the breeze. Just before I had finally taken off my jacket and had it wedged between my lap and the tank it was so hot. We’re talking 103 degrees and full sun here.

Then off towards some back roads to Napa. I came down Napa from a really nice eastern road, finally stopped at Soda Springs cute general store for a very late lunch and cool down in the shade.

It was a great trip.