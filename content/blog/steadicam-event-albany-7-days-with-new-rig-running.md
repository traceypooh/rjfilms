---
title: 'Steadicam event Albany 7 days with new rig running.'
date: Sun, 14 Apr 2019 19:04:27 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0133-1024x768.jpg
tags: ['Uncategorized']
---

Just got my new Steadicam rig running for a massive eSports event at the Albany NY convention center. I got two friends hired to work with me as well so it was a really fun shoot. Flying RF Sony cameras at least 20 lbs of camera and transmitter weight. I got a new Atomos Samurai Blade HD monitor recorder running as my main monitor. I really like the heavy duty build of my rig. I custom fabricated a much better balance bracket out of a custom Honda lower control arm that I found while picking up a rear caliper for my Honda car. It's a beautiful anodized gold super strong plate all custom made with 5/8" metal bolts and studs that I scavenged from metal piles and a broken off telephone pole top I found on a bike ride :) My hand fabricated custom balance plate. This is much better than the one that comes with this Steadicam unit. More level, stronger, and looks custom. I did some nice upgrades to this for the next eSports event: Los Angles. ![](https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0133-1024x768.jpg)