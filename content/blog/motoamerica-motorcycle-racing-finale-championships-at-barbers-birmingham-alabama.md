---
title: 'MotoAmerica motorcycle racing finale Championships at Barbers, Birmingham, Alabama'
date: Tue, 09 Jul 2019 19:40:20 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2018/09/IMG_8928-1024x768.jpg
tags: ['camera', 'Camera op', 'championship', 'motoamerica', 'motorcycle', 'racing', 'Uncategorized']
---

My second time back to the lovely Barber's racetrack outside of Birmingham, Alabama. We were graced with much better weather this year for the finals! Flew into Atlanta and rented a car and drove out there with my camera buddy Chris S. Had to race back to Atlanta to make the last flight out Sunday night after shooting all day. I had a 5 day Video Engineering job back in Boston I had to make for Monday morning early. ![RJ Films MotoAmerica camera motorsports](https://rjfilms.com/blog/wp-content/uploads/2018/09/IMG_8928-1024x768.jpg)