---
title: 'US Open Tennis 2019 USTA. Camera for ESPN live broadcast'
date: Sat, 31 Aug 2019 21:19:23 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/08/IMG_0428-RJ-shooting-US-OPEN-2019-1024x768.jpg
tags: ['Boston Camera', 'Boston cameraman', 'Boston cameraman', 'Boston Director of Photography', 'Boston Director of Photography', 'camera', 'Russ Jaquith camera']
---

![](https://rjfilms.com/blog/wp-content/uploads/2019/08/IMG_0428-RJ-shooting-US-OPEN-2019-1024x768.jpg) Just shot 7 straight days at the US Open in Flushing, Queens, NY again. Great event. Was sick the first two days of shooting but the last final days were really fun. Much better weather this year, and Hyeung Chung finally completed a fantastic comeback at Court 10 Thursday night late in front of a completely packed screaming house of fans. The lines to get in were endless, there were no spots to view the match left. Take a look at the upper left corner. The girl serving is completely in the air - that cracks me up about this photo. ![](https://rjfilms.com/blog/wp-content/uploads/2019/08/IMG_0426-RJ-shooting-US-OPEN-2019-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/08/IMG_0429-RJ-shooting-US-OPEN-2019-1024x768.jpg)