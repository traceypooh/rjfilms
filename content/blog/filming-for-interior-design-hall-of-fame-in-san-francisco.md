---
title: 'Filming for Interior Design Hall of Fame in San Francisco'
date: Thu, 01 Oct 2015 12:46:00 +0000
author: rj
feature_image: https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Interior-Design-SF-slider-IMG_3183-467x350.jpg
tags: ['Uncategorized']
---

Finally got a chance to use my O’Connor 50D tripod on an important shoot and it delivered big time!  
I got asked to work with a friend’s Sony F55 camera on a slider platform.  
I rented a slider from the neighbors next door – to later find out it was a lightweight DSLR rig and not meant for heavy weight. The O’Connor 50D fluid head completely bailed me out of that situation.

I was able to have extremely precise movements with the F55 camera with a heavy lens, Canon 15.5 mm – 47mm PL mount lens on it too. I was thrilled with the performance of this O’Connor.

[![Interior Design SF slider IMG_3183](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Interior-Design-SF-slider-IMG_3183-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Interior-Design-SF-slider-IMG_3183.jpg)

My O’Connor head:  
[![Interior Design SF slider IMG_3185](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Interior-Design-SF-slider-IMG_3185-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Interior-Design-SF-slider-IMG_3185.jpg)