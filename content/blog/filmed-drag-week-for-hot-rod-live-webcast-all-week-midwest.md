---
title: 'Filmed “Drag Week” for Hot Rod live webcast all week midwest.'
date: Sun, 20 Sep 2015 11:21:00 +0000
author: rj
feature_image: https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day1-Gateway-Madison-IL-camera-setup-IMG_3100-467x350.jpg
tags: ['cars', 'Drag racing', 'motorsports', 'Uncategorized']
---

Flew out to St. Louis, Missouri for the start of Drag Week.

Day 1: Gatweway, Madison, IL:  
camera setup down the strip elevated in scissor lift:  
[![Drag Week 2015 day1 Gateway Madison IL camera setup IMG_3100](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day1-Gateway-Madison-IL-camera-setup-IMG_3100-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day1-Gateway-Madison-IL-camera-setup-IMG_3100.jpg)

![](https://rjfilms.com/blog/wp-content/uploads/2015/09/Drag-Week-2015-day2-Indianapolis-IMG_3103-e1602024075265-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2015/09/Drag-Week-2015-day3-RJ-Shooting-grab-red-car-Great-Lakes-Dragaway-Union-Grove-Wis-1024x576.png)

Day 3: Great Lakes Dragaway, Union Grove, Wisconsin:  
\[caption id="attachment\_699" align="alignnone" width="640"\]![racing camera Russ Jaquith ](https://rjfilms.com/blog/wp-content/uploads/2020/10/Drag-Week-2015-day3-RJ-Shooting-grab-Great-Lakes-Dragaway-Union-Grove-Wis-1024x578.png) racing camera Russ Jaquith \[/caption\]

Apparently an overhead drone shot of the strip. I’m shooting at the upper left area in a scissor lift type of rig:  
![](https://rjfilms.com/blog/wp-content/uploads/2015/09/record-crowds-great-lakes-dragaway-drag-week-2015-8.jpg) >

Day 4: Cordova International Raceway, Cordova, IL:  
6:48am:  
[![Drag Week 2015 day4 daybreak Cordova IL IMG_3108](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day4-daybreak-Cordova-IL-IMG_3108-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day4-daybreak-Cordova-IL-IMG_3108.jpg)

[![Drag Week 2015 day4 RJ Shooting Drag Week - grab two cars blurry Cordova IL](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day4-RJ-Shooting-Drag-Week-grab-two-cars-blurry-Cordova-IL-500x274.png)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day4-RJ-Shooting-Drag-Week-grab-two-cars-blurry-Cordova-IL.png)

Day 5: Gateway, Madison, IL:  
daybreak 6am:  
[![Drag Week 2015 day5 6am dawn Gateway IMG_3099](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day5-6am-dawn-Gateway-IMG_3099-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day5-6am-dawn-Gateway-IMG_3099.jpg)

Afternoon filming at the burnout box:  
[![Drag Week 2015 day5 Gateway burnout box perch IMG_3117](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day5-Gateway-burnout-box-perch-IMG_3117-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day5-Gateway-burnout-box-perch-IMG_3117.jpg)

Day 6: Gateway, Madison, IL:  
My scissor lift home:  
[![Drag Week 2015 day6 Gateway Madison IL scissor lift IMG_3124](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day6-Gateway-Madison-IL-scissor-lift-IMG_3124-467x350.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2015/10/Drag-Week-2015-day6-Gateway-Madison-IL-scissor-lift-IMG_3124.jpg)