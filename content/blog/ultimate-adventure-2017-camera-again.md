---
title: 'Ultimate Adventure 2017 - Camera - Arizona, Utah'
date: Tue, 27 Jun 2017 15:25:59 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8548-e1563118313908-1024x768.jpg
tags: ['camera', 'motorsports']
---

2017 was the year of my broken ankle and the year we wheeled around Arizona. 8 day trip all around the state and various passes. Grand Canyon. We ended the trip with a final shoot in Utah and the incredibly beautiful dunes area of Sand Hollow in the south of the state. Flew back out of Las Vegas. Highlight video of my shots I posted here: [2017 Ultimate Adventure RJ Shots](https://rjfilms.com/video/ultimateadventure2017.mp4) The first day at the bottom of a canyon I felt exactly like someone was hacksawing into my ankle bone. It was pretty much almost a 10 on pain. I had a lot of swelling and pain every single day but I hobbled all over the place with my ankle. I had only just had 7 screws and a plate put in 30 days ago and I could pretty much not walk on my aircast without support. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8548-e1563118313908-1024x768.jpg) Shooting at the Grand Canyon. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8406-1024x768.jpg) ![Ultimate Adventure 2017 start](https://rjfilms.com/blog/wp-content/uploads/2017/06/rj-w-mike-parking-lot-UA2017-1024x501.jpg)