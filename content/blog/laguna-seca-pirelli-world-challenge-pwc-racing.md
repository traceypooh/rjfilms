---
title: 'Laguna Seca - Pirelli World Challenge PWC racing'
date: Sun, 15 Oct 2017 15:47:29 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9156-1024x768.jpg
tags: ['camera', 'motorsports', 'RJ Films', 'Russ Jaquith', 'Russ Jaquith camera']
---

Very hot week at Laguna Seca raceway in Monterrey, California. Super fast beautiful cars. The best part of the event. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9156-1024x768.jpg) I honed in live on the developing and massive crash of Bryan Ortiz in the Miata class. It was one of the heaviest wrecks in years at Laguna Seca. I posted the live play by play shot I made during the race as well as one of many slow mo replays of this flipping multiple times wreck right in my corner. There were no other cameras around to capture this wreck - or the faraway ones missed it. [![](images/dialogcomm.gif)![](https://rjfilms.com/blog/wp-content/uploads/2017/10/david-ortiz-screen-grab-wreck-Laguna-Seca-2017-RJ-Films-demo-reel-sample.png)](https://rjfilms.com/video/motorsportsrj.mp4) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9144-1024x768.jpg)