---
title: 'Filming "Taste for Adventure" for Fine Living Network 2007. Season Two TV show.'
date: Fri, 06 Jul 2007 22:45:17 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-monument-valley-Kara-camIMG_1399-e1602024474261-768x1024.jpg
tags: ['camera', 'Russ Jaquith', 'Russ Jaquith camera', 'TV series']
---

I filmed as second camera and sometimes helped with sound for Season two of the network show: "Taste for Adventure". I worked all spring and summer 2007 on this series. We did a '4 corners' episode pictured here at Monument Valley, Arizona. A place I had filmed just the year before for my documentary "8 Days and 6 Hours". ![](https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-monument-valley-Kara-camIMG_1399-e1602024474261-768x1024.jpg)