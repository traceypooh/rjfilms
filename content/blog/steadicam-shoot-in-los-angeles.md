---
title: 'Steadicam shoot in Los Angeles.'
date: Tue, 09 Jul 2019 18:33:40 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/rj-shooting-Fortnite-grab-from-twitch-1024x574.png
tags: ['Boston', 'Boston based', 'camera', 'operator', 'owner', 'Steadicam', 'Uncategorized']
---

Just back from excellent week in Los Angeles shooting at the L.A. Forum. I used my new heavy duty Steadicam rig. We were flying the new sony P1 cameras with full RF kit. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/rj-shooting-Fortnite-grab-from-twitch-1024x574.png) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-steadicam-Los-Angeles-profile-576x1024.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0277-1024x768.jpg)