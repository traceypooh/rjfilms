---
title: 'Director and Director of Photography for my documentary "8 Days and 6 Hours": A coast to coast adventure race.'
date: Thu, 06 Jul 2006 22:49:24 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-monument-valley-Kara-camIMG_1399-e1602024474261-768x1024.jpg
tags: ['Boston Director of Photography', 'camera', 'Director of Photography', 'Russ Jaquith', 'Russ Jaquith camera']
---

![](https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-monument-valley-Kara-camIMG_1399-e1602024474261-768x1024.jpg) Filmed my epic documentary "8 Days and 6 Hours". This film highlights team JDRF's attempt to cross America in a non-stop race \[caption id="attachment\_753" align="alignnone" width="407"\]![8 Days and 6 Hours documentary](https://rjfilms.com/blog/wp-content/uploads/2020/10/8daysposter.jpg) 8 Days and 6 Hours documentary\[/caption\] It captures the pains, struggles, highs and lows of this grueling race The four racers endure almost no sleep, riding all day and night, lightning storms, crashes, conflict, and the enduring beauty of America, all via small highways Director of Photography Russ Jaquith Directed and Produced by Russ Jaquith Main webpage link: [Main website 8 Days 6 Hours page](http://rjfilms.com/8days.htm)