---
title: 'Shot “Celebrity Bucket List” with Matt Nathanson and Chris Isaak'
date: Mon, 03 Feb 2014 12:46:00 +0000
author: rj
draft: false
---

Filmed in San Francisco as the camera operator for Lifetime’s TV show “Celebrity Bucket List” (was on A & E season 1). This is for Season 2. Matt Nathanson got to meet up with Chris Isaak, share a cheeseburger and jam out by a fire pit in the Presidio. Was a beautiful sunny day.

I also filmed another episode for this series in Napa, CA this past fall, which featured Olympic Gold Medalist Natatie Coughlin.

San Francisco / Oakland CA local Director of Photography / cameraman / camera operator, Russ Jaquith, owner of [RJ Films](http://www.rjfilms.com)