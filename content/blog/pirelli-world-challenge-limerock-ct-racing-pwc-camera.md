---
title: 'Pirelli World Challenge - Limerock, CT racing PWC - Camera'
date: Mon, 28 May 2018 16:03:42 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9940-1024x768.jpg
tags: ['camera', 'motorsports', 'Repair', 'RJ Films', 'Russ Jaquith', 'Russ Jaquith camera']
---

My second trip to Limerock, CT raceway. Great racing action. I absolutely love the KTM car - it's a heavily race modified XBow with massive spoilers, splitter, tail and wraparound panels. Super low, super wide, cornering (if not power) monster. I want one!! ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9940-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9931-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9927-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_9926-1024x768.jpg)