---
title: 'MotoAmerica motorcycle racing finale 2017 Championships at Barbers, Birmingham, Alabama'
date: Fri, 15 Sep 2017 19:34:14 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2018/09/IMG_8928-1024x768.jpg
tags: ['Uncategorized']
---

Four days in Alabama, and my hotel room was massive and beautiful. I must have gotten the nicest room in the place. We started out a a very dumpy hotel and were moved to beautiful new place the next day. ![RJ Films MotoAmerica camera motorsports](https://rjfilms.com/blog/wp-content/uploads/2018/09/IMG_8928-1024x768.jpg) I got a rental Jeep at the Atlanta airport and drove out to Birmingham taking a scenic detour into the Cheeha Mountains park. I wound up by interesting accident on the very same roads I had shot on for Ultimate Adventure a few years earlier. These are dirt tracks into the hills and over mountain passes... really broken up and fun stuff! ![](https://rjfilms.com/blog/wp-content/uploads/2018/09/IMG_8898-1024x768.jpg) I DID get the jeep stuck... turns out the damn thing was FWD only and no 4WD option... so could not get it out of the mudhole. I walked back to the mountain road and managed to flag down a very nice army guy returning home in his Tacoma truck. He attached a chain to my rear control arm and pulled me out while I throttled it. Anyway, the shoot itself was great! Although we got dumped on with pounding rains, and there were several crashes in the ridiculous pools of water before they delayed a race finally.