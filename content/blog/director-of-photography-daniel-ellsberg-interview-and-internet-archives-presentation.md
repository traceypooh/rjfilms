---
title: 'Director of Photography – Daniel Ellsberg interview and  Internet Archive’s presentation'
date: Thu, 24 Oct 2013 13:19:00 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/rj-shooting-Daniel-Ellsberg-767x1024-767x1024.jpg
tags: ['camera']
---

Yes, this was an interview with THE Daniel Ellsberg, author of “The Pentagon Papers”. He’s 82 and still kicking ass. He was the keynote speaker and major interview subject. He is an intelligent, and fascinating person, the interview spanned 3 + hours and was riveting.

I shot this with my Panasonic HPX-170 camera and my Tram TR50 lav microphone.

The complete interview and the keynote presentation are all available at:

[Daniel Ellsberg complete interview uncut](https://archive.org/details/danielellsberginterview)

I was the Director of Photography / cameraman / camera operator for this shoot in San Francisco, California. [![rj-shooting-Daniel-Ellsberg](https://rjfilms.com/blog/wp-content/uploads/2020/10/rj-shooting-Daniel-Ellsberg-767x1024-767x1024.jpg) Daniel Ellsberg RJ filming\[/caption\]](https://www.rjfilms.com/blog/wp-content/uploads/2014/02/rj-shooting-Daniel-Ellsberg.jpg)