---
title: 'PAX East Boston, Main stage, principal camera'
date: Sun, 08 Apr 2018 20:02:55 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-Shooting-PAX-final-day-GS-2018-owner-Mike-1-1024x573.png
tags: ['Boston cameraman', 'camera', 'RJ Films', 'Russ Jaquith', 'Russ Jaquith camera']
---

The really great event PAX East, came to Boston again. 2nd or 3rd year working the event. Main stage camera operator for entire week event. 70,000+ attendees... this show is HUGE! Really great attendees and spirit. One of my favorite corporate video gigs each year. This was the final event final day: The Omegathon. \[caption id="attachment\_722" align="alignnone" width="640"\]![PAX East Boston cameraman](https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-Shooting-PAX-final-day-GS-2018-owner-Mike-1-1024x573.png) PAX East Boston cameraman\[/caption\] They had two nights of concerts with multiple bands I got to film under the main stage, and on the stage handheld as well. Really excellent bands / music.