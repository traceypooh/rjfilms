---
title: 'Projectionist for the Boston TEDx talks'
date: Fri, 03 May 2019 19:09:25 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_0146-1024x768.jpg
tags: ['2019', 'Boston', 'Opera House', 'Projectionist', 'TEDx', 'Uncategorized']
---

I was the projectionist responsible for setting up 5 separate screens in the gorgeous Boston Opera House. We had a converged ultra short throw 'snorkel' lens setup that one of my great projectionist friends says is extremely hard to converge with clean resolution. It all came out great. The clients were all very happy. I also ran some new dynamic 'living background' software for two of the projectors that shot on 3D Atomic backdrop panels. It looked really gorgeous and moving during the talks.![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_0146-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_0155-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_0156-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_0152-1024x768.jpg)