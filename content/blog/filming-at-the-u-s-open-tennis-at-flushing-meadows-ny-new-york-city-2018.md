---
title: 'Filming at the U.S. Open Tennis at Flushing Meadows NY, New York City 2018'
date: Thu, 30 Aug 2018 19:25:33 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/rjshootingusopen.jpg
tags: ['broadcast', 'camera', 'Camera op', 'NBC', 'New York City', 'Tennis', 'Uncategorized', 'US Open']
---

Got to shoot the U.S. Open of Tennis! Was fantastic shoot. I was camera #6 on court 10. And two days I was running camera 2 half the time and 6 the other half. Super fast action, shooting the close up replay cameras, this stuff is faster than racing cars! It was super hot this week, they introduced new break rules for the players as it was over 100 degrees F courtside. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/rjshootingusopen.jpg)