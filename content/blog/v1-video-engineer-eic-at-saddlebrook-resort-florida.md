---
title: 'V1 Video Engineer, EIC, at Saddlebrook Resort, Florida'
date: Mon, 05 Feb 2018 19:50:34 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/IMG_9596-RJ-V1-Florida-Saddlebrook.jpg
tags: ['Russ Jaquith', 'V1', 'Video Engineer']
---

Just worked as the lead Video Engineer V1 for a week at SaddleBrook golf resort north of Tampa Florida. I was operating the Barco E2 video switcher and 3 projection screens. We had a fun concert shoot on the final evening and a few interesting speakers.\[caption id="attachment\_698" align="alignnone" width="453"\]![](https://rjfilms.com/blog/wp-content/uploads/2020/10/IMG_9596-RJ-V1-Florida-Saddlebrook.jpg) video engineer, V1, Russ Jaquith, Boston, Florida\[/caption\]