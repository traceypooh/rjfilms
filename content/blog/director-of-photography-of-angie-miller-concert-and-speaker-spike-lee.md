---
title: 'Director of Photography of Angie Miller concert and speaker Spike Lee'
date: Fri, 01 Nov 2013 13:24:00 +0000
author: rj
feature_image: https://www.rjfilms.com/blog/wp-content/uploads/2014/02/RJ-shooting-Angie-Miller-20-600x361.jpg
tags: ['camera']
---

Was solo shooter for an Angie Miller concert in Boston, MA. Spike Lee was also the keynote speaker at this private event.

Angie Miller can sing, let me say.

Boston local Director of Photography / cameraman / camera operator, Russ Jaquith, owner of [RJ Films](https://www.rjfilms.com)  
[![RJ-shooting-Angie-Miller-20](https://www.rjfilms.com/blog/wp-content/uploads/2014/02/RJ-shooting-Angie-Miller-20-600x361.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2014/02/RJ-shooting-Angie-Miller-20.jpg)[![RJ-shooting-Spike-Lee-IMG_2](https://www.rjfilms.com/blog/wp-content/uploads/2014/02/RJ-shooting-Spike-Lee-IMG_2-535x400.jpg)](https://www.rjfilms.com/blog/wp-content/uploads/2014/02/RJ-shooting-Spike-Lee-IMG_2.jpg)