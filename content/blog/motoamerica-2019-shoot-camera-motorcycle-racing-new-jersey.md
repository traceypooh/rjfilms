---
title: 'MotoAmerica 2019 shoot camera motorcycle racing New Jersey'
date: Mon, 09 Sep 2019 20:25:08 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/09/MotoAmerica-2019-Millville-New-Jersey-IMG_0439-1024x768.jpg
tags: ['camera', 'motorsports', 'Russ Jaquith', 'Russ Jaquith camera']
---

Just got back from the NJ MotoAmerica motorcycle race series. Was a fun shoot with the fast bikes and great weather this year! ![](https://rjfilms.com/blog/wp-content/uploads/2019/09/MotoAmerica-2019-Millville-New-Jersey-IMG_0439-1024x768.jpg) Camera 6 with a fast turn to the left. Had a lot of live air time. visit Russ' main website for demo reel video material and more: [RJ Films main website](http://rjfilms.com)