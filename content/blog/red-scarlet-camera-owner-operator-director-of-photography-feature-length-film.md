---
title: 'RED Scarlet 4K camera owner / operator. Director of Photography. "Habitual" Feature length film'
date: Sat, 11 Feb 2017 19:04:33 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-shooting-Habit-RED-Scarlet-4K-raw-Boston-area-4.jpg
tags: ['4K RAW', 'Boston', 'Boston based', 'camera', 'camera', 'cameraman', 'Director of Photography', 'DP', 'Owner operator', 'prime lenses', 'RED', 'Scarlet', 'Zeiss']
---

I shot a new psychological thriller called "Habitual" over the past two years. It centers around an actual abandoned Massachusetts insane asylum. Also official known as "the Insane Hospital". It was abandoned in the 80s or 90s and fell into complete disrepair. I got us the official permission to shoot exteriors and some interiors through the owner which was the town at that point. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-shooting-Habit-RED-Scarlet-4K-raw-Boston-area-4.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-shooting-Habit-RED-Scarlet-4K-raw-Boston-area-3.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-shooting-Habit-RED-Scarlet-4K-raw-Boston-area2-1024x576.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/RJ-shooting-Habit-RED-Scarlet-4K-raw-Boston-area-1024x575.jpg)