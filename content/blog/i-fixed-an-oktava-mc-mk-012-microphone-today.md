---
title: 'I fixed an Oktava MC / MK 012 microphone today!'
date: Tue, 07 Jan 2014 18:03:00 +0000
author: rj
feature_image: https://www.rjfilms.com/blog/wp-content/uploads/2014/01/Oktava-MK012-600x238.jpg
tags: ['DIY', 'MC012', 'microphone', 'MK012', 'oktava', 'professional', 'Repair', 'repair']
---

I trawled CL country wide last year for an Oktava 012, and found a listed Oktava MC 012 microphone for sale from a Church that was closing it’s doors priced at $40.00!

I jumped on it, and had it shipped. In January. Which may have caused the issue…

The mic, for me, showed up D.O.A. and I had been hoping to rely on this as a 2nd shotgun microphone to back up my superb Sennheiser MKH416T mic, a much loved workhorse of mine since the “Stolen Good” days. In fact, my Sennheiser MKH 416 microphone did my entire film, from field recordings, foley, ambient,  and hand built ‘booth’ ADR work.

Anyway, back to this Oktava. Needless to say it was dead, did not power up, no audio, with without phantom power from multiple sources – nothing.

I had only one day before a major shoot to use it so it was a huge disappointment. Fast forward one year. I made a few inquiries about sending it in for repair. I also then thought that I might as well take apart the capsule and see if there was anything obvious wrong / broken. By the way, you screw the mini bolts IN, NOT out, to remove the outer capsule to reveal the insides. This is a real Russian built microphone, not the cheap, less better sounding Chinese copies.

![Oktava MK012](https://www.rjfilms.com/blog/wp-content/uploads/2014/01/Oktava-MK012-600x238.jpg)So, to my astonishment, there was a very obvious broken weld / solder point at the tip where the mic head screws into. I realized right away there’s no way the mic could possible work this way, so I soldered it back in. I re tried the mic, and it powered up but sounded very very bad, I could barely hear a thing. So I made a second cleaner smaller solder, I suspected the soldered joint was large enough to touch the outside barrel capsule and thought that perhaps was causing the issue. Re hooked it back up and it worked and sounded perfect! A lucky easy fix! So I will be using this mic this year as I’m booked again on the same two week assignment where I need two complete sets of gear: 2 cameras, 2 boom mics working, 2 tripods, 2 light sets etc…![Oktava MC 012 2](https://www.rjfilms.com/blog/wp-content/uploads/2014/01/Oktava-MC-012-2-600x288.jpg)

In this picture, you can see the broken solder joint at the lower left. An obvious major problem. I will post a thorough review of this mic in March, and it will in essence be a bit of a comparison with my Sennheiser MKH 416 mic. I’ll post videos made exclusively from each mic to essentially identical cameras.

Update: The Oktava performed brilliantly as a boom mic for the horror short film. We screened my film with the Oktava and my co-teacher’s film with my cherished MKH416T Sennheiser and they both sounded terrific in a huge theater / film projection room at the school!