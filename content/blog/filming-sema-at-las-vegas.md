---
title: 'Filming SEMA at Las Vegas!'
date: Wed, 05 Feb 2014 15:56:07 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-SEMA-alex-car-g.jpg
tags: ['Boston cameraman', 'camera', 'Director of Photography', 'motorsports', 'RJ Films', 'Russ Jaquith', 'Russ Jaquith camera']
---

The legendary SEMA motorsports trade show in Las Vegas. Always wanted to attend this show. We had a foodee on crew, and we ate a fantastic crab dinner out one night. Really amazing cars and builds. ![](https://rjfilms.com/blog/wp-content/uploads/2020/10/RJ-shooting-SEMA-alex-car-g.jpg)