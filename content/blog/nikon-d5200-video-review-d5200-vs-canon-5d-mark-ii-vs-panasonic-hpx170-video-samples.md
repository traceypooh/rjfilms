---
title: 'Nikon D5200 Video review! D5200 vs. Canon 5D Mark II vs. Panasonic HPX170. Video samples'
date: Wed, 01 Oct 2014 15:33:00 +0000
author: rj
draft: false
---

This year I purchased a factory refurbished Nikon D5200. I was impressed with the video write ups coming in on the camera… According to reviewers, it was trumping the high end Nikon cameras, and the renowned Canon 5D Mark II and Mark III cameras.

I shot my first video with it using a Nikon kit (cheaper) lens: the 17-55mm zoom lens.  
The video was decent. I was happy with it. I was also running my Panasonic HVX170 DVCPRO HD camera at 720P 24PN setting with a very nice color profile that I typically use.  
I thought the Nikon’s color was pretty excellent, with little to no tweaking shooting the same interview.

I was even more impressed with this camera when I mated it up to Nikon’s superb 85mm 2.0 F stop lens.  
You can get some crazy shallow depth of field shots with this setup, especially shooting wide open, which I was. I’ll have to post up some samples with the beautiful 85mm Nikkor lens at a later date.

For now, I’ll include a few video links below.

This Porsche 914 6 cylinder video is shot on the Nikon D5200 at ISO 100, 720P 60P setting, stock cheap kit zoom Nikon lens 17-55mm zoom. NO post processing was done on the Nikon footage.  
There are even a couple of ugly Canon 5D Mark II shots in here. I was not operating that camera. You can tell them from mine, the exposure is dark and colors off on the Canon, and during the engine running shots, the angle shot from the left of the car with the crushed blacks is the 5D footage.

Below is an Austin Healy car video I shot with my Panasonic HPX170 at 720P 24PN setting. Again, no post grading on this footage:  

The Nikon can shoot 1080P yes, P at 30P fps, but the 60P setting at 720P resolution suits fast motion better, and besides, I am now shooting cars with this Nikon and my GoPro Hero 3+ camera using a 720P 60P setting as well and it intercuts easier with Final Cut Pro.

The final product is released at 720P to simply YouTube, so no need for 1080P. Allegedly, according to EOSHD, the 1080P setting is much better and the Nikon D5200 video ‘sweet spot’. I will have to experiment more with that setting and post my impressions.

But so far I love shooting with the Nikon. The controls are easily laid out, the LCD monitor is easy to pull focus with and is very accurate so I am not getting unpleasant surprises in post. So far, after about 7 car videos, it’s mostly what I see shooting on the LCD is what I get. Great!  
The Panasonic viewfinder is a LOT worse in that regard.

I lock off the Nikon in Manual mode and lock the exposure. For these shoots I am mostly over exposing the footage on purpose to make the car paint sing more.  
And so far, the only small complaint I have with the results at 720P is that the black seem crushed, particularly compared to the Panasonic professional 3CCD video camera, BUT the Nikon footage appears less grainy. Both have very nice coloring and picture profiles, although the Nikon is easy to get there, and the Panasonic needs some good gamma tweaking etc…

AND the Nikon kills the Panasonic camera for low light shooting. For ‘normal’ light indoor shooting, in fact. The Panasonic makes beautiful images but demands a lot of light to look right.

All in all, I am pretty thrilled with this new Nikon acquisition. It’s exceeded my video expectations, and the price is a fraction of the Panasonic price and the Canon 5D. An amazingly adept camera and superb price value. Oh, and the still photos are amazing. The built in metering is bang on, and make amazing looking pictures very, very easy right out of the box. I was a bit dissappointed with my original DSLR, the Nikon D40X. That camera can’t focus for damn. 3 focus points and, wait, it’s hunting for the wrong focus again… A camera you could never trust for, say, a wedding shoot. The new sensor and metering of the D5200 is the DSLR that I always wanted for stills, and that it makes fantastic video to boot… heavenly.