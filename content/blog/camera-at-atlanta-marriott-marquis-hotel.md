---
title: 'Camera at Atlanta Marriott Marquis hotel'
date: Wed, 20 Feb 2019 19:29:48 +0000
author: rj
draft: false
tags: ['Boston', 'cameraman', 'corporate camera', 'event', 'Uncategorized']
---

2 days of camera inside the beautiful Marriott Marquis hotel in downtown Atlanta, GA. The architecture of this place is off the hook. Just spectacular views walking around hotel and exploring. Nice easy shoot with good crew.