---
title: 'Filming MotoAmerica in New Jersey'
date: Tue, 12 Sep 2017 19:43:01 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/rj-shooting-MotoAmerica.jpg
tags: ['broadcast', 'camera', 'cameraman', 'motoamerica', 'motorsports', 'motorsports', 'racing']
---

Just got back from filming MotoAmerica in New Jersey this time. I'm back flying out to Alabama tomorrow for the next one. Here's pics from New Jersey: ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/rj-shooting-MotoAmerica.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2017/09/rj-shooting-MotoAmerica-tires-corner.jpg) This was really scary. We all didn't know how bad this was. Caroline Olsen had a severe crash right under my tower. Motionless face down. Full speed, no scrubbing speed, no turn, and right into the wall. She missed the air wall by about 4 feet. That chopper landed in my turn and airlifted her out. ![](https://rjfilms.com/blog/wp-content/uploads/2017/09/rj-shooting-MotoAmerica-crash.jpg) I found out later she experienced complete brake failure, and rather than try to turn at full speed after the straight and take out another rider, she slightly leaned away from the turn last second... and then smashed the wall and missed all the protective airwalls. Sad. She live though :) Severe concussion, broken ribs, broken leg and collarbone. This was a tough break for her.