---
title: 'MotoAmerica 2017 1st one - Pittsburg PIRC Racetrack'
date: Sun, 27 Aug 2017 15:40:23 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8736-1024x768.jpg
tags: ['Boston cameraman', 'camera', 'motorsports', 'RJ Films']
---

My first time shooting the MotoAmerica series. It was awesome! Of course, the kid with broken ankle gets the assignment up the tower. Turn 1. ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8736-1024x768.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8763-1024x768.jpg) Tower turn one: ![](https://rjfilms.com/blog/wp-content/uploads/2019/07/IMG_8734-e1563118770140-768x1024.jpg) ![](https://rjfilms.com/blog/wp-content/uploads/2017/08/pitts-race-tower-RJ-pano-IMG_8768-344x1024.jpg)