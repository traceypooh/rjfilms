---
title: 'Fabricated balance plate / mount for my Steadicam rig.'
date: Thu, 04 Apr 2019 19:45:54 +0000
author: rj
feature_image: https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0133-1024x768.jpg
tags: ['Boston', 'Boston area', 'Boston based', 'camera', 'DIY', 'owner', 'RED 4K', 'Steadicam', 'Steadicam op']
---

Was at the massive auto junkyard pulling a rear passenger caliper for my failed Accord one, and I spotted this gorgeous heavy duty suspension part out the side of a wrecked, stacked car. I tried to remove it to get it for this purpose, but the accident / damage had made removal with normal hand tools not possible. I returned a few days prior to my Albany NY big event and was able to hacksaw through the crushed subframe part and remove it! $20 cash. I spent the better part of the next day on the Cape measuring carefully and machining off metal parts with my angle grinder. I scrounged 5/8" bolts from the local scrap metal pile here, and then scavenged a downed electric main utility pole top that had sheared off and was laying at road side. The bolts used throughout? You guessed it. 5/8" So I got this all going for next to nothing. Balancing and levels the Steadicam rig way better than the manufacture's $55 version. And looks really nice, very custom. I got some compliments on the next big shoots on it. ![](https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0133-1024x768.jpg) In use in Los Angeles, July: ![](https://rjfilms.com/blog/wp-content/uploads/2019/04/IMG_0277-1024x768.jpg)