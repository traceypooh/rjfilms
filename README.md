# unofficial site of RJ Films

## find 1st referenced image in each post
so we can edit edit post `.md` file and add in the "front matter":
```markdown
feature_image: ".."
```
using script - find the first image mentioned in each post, and add it to the top front matter:
```bash
for dir in content/blog content/greenmagic content/guitars; do
  cd $dir
  for i in $(egrep -Ho '\([^\)]*\.(png|gif|jpg|svg)\)' *.md | fgrep .md:); do
    md=$(echo "$i" |cut -f1 -d:)
    img=$(echo "$i" |cut -f2- -d: |tr -d '()')

    perl -i -pe "s^draft: false^feature_image: $img^" $md
  done
  cd -
done
```

## conversions from .xml
done with https://github.com/palaniraja/blog2md
eg:
```bash
git clone https://github.com/palaniraja/blog2md
( cd blog2md; npm i )
node blog2md/index.js w camerablogrjfilms.WordPress.2020-11-30.xml camera
```

## add RJ as "author" to all posts
`perl -i -pe 's!^(date:.*)!$1\nauthor: rj!' $(find . -name '*.md')`
